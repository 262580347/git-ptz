#ifndef _COMPILECONFIG_H_
#define _COMPILECONFIG_H_


#define RELEASE			0
#define _DEBUG_	 		1

//硬件版本配置===================================================
#define HARDWARE_VERSION	600

//嵌入式软件版本配置=============================================
#define SOFTWARE_VERSION	600

//传感器配置=====================================================
#define TH_SENSOR_EN	0
#define BMP_SENOR_EN	0
#define MAX44009_EN		1

//DAC输出通道配置================================================
#define TH_TEMP		1
#define TH_HUMI		2
#define BMP_TEMP	3
#define BMP_PRESS	4
#define MAX44009_ILLUMINANCE	5

//#define DAC_CHANNEL_CONF_1	TH_TEMP
//#define DAC_CHANNEL_CONF_2	TH_HUMI

#define DAC_CHANNEL_CONF_1	0
#define DAC_CHANNEL_CONF_2	MAX44009_ILLUMINANCE


//DAC通道编号
#define DAC_CHANNEL_1	1
#define DAC_CHANNEL_2	2

#if RELEASE
//模块调试信息输出使能===========================================
#define _DETAIL_DEBUG_MAIN		1		//主模块的调试信息输出开关
#define _DETAIL_DEBUG_CONTROL	1		//控制模块调试信息输出开关
#define _DETAIL_DEBUG_DETECT	1		//检测模块调试信息输出开关
#define _DETAIL_DEBUG_THCOMM	0		//通信模块调试信息输出开关
#define _DETAIL_DEBUG_GPRSCOMM	1
#define _DETAIL_DEBUG_SAVING	0

#else
//模块调试信息输出使能
#define _DETAIL_DEBUG_MAIN		1		//主模块的调试信息输出开关
#define _DETAIL_DEBUG_CONTROL	1		//控制模块调试信息输出开关
#define _DETAIL_DEBUG_DETECT	1		//检测模块调试信息输出开关
#define _DETAIL_DEBUG_THCOMM	1		//通信模块调试信息输出开关
#define _DETAIL_DEBUG_GPRSCOMM	1
#define _DETAIL_DEBUG_ZIGBEECOMM 1
#define _DETAIL_DEBUG_XBEECOMM	1
#define _DETAIL_DEBUG_SAVING	1
#define _DETAIL_DEBUG_SETCOMM	1		//串口通信处理
#define _DETAIL_DEBUG_SOIL_TH_DETECT	1	//土壤温湿度
#define _DETAIL_DEBUG_ILLUMINANCE		1	//光照度
#define _DETAIL_DEBUG_CARBONDIOXIDE		1	//二氧化碳浓度
#define _DETAIL_DEBUG_FLOW		1	//二氧化碳浓度
#define _DETAIL_DEBUG_ZIGBEEINIT	1		//Zigbee初始化
#define _DETAIL_DEBUG_GPRSINIT		1		//GPRS模块初始化
#define _DETAIL_DEBUG_LORACOMM		1
//#define _DETAIL_DEBUG_LORACOUNT		1			//显示LORA收发统计数据
#define _DETAIL_DEBUG_LORACOUNT	0			//不显示LORA收发统计数据


#endif


//消除编译类型不匹配警告
#pragma  diag_suppress 870,167


#endif
