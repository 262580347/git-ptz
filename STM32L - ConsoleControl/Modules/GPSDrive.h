#ifndef		_GPSDRIVE_H_
#define		_GPSDRIVE_H_

#define		GPSPRODUCTINFORMATION		0x01
#define		GPSANTTEST					0x02
#define		GPSGETLOCATION				0x03

#define		GPS_POWER_PIN		GPIO_Pin_15
#define		GPS_POWER_TYPE		GPIOA

#define		GPS_RESET_PIN		GPIO_Pin_5
#define		GPS_RESET_TYPE		GPIOB


void GPSPowerOn(void);

void GPSPowerOff(void);

void GetPosition(float *Latitude, float *Longitude);

unsigned char GetGPSDataFlag(void);

float GetAltitude(void);

unsigned char GetGPSPowerFlag(void);
	
#endif



