/**********************************
说明:日志存储

日志使用EEPROM	30~64  区块
每区块有16*2字节
每2字节作为最小存储单位

格式为 | 日（8bits）、时（8bits）、分（8bits）、秒（8bits） | 	| 电压（16bits）、错误码1（8bits）、错误码2（8bits）、错误码3（8bits）、错误码4（8bits） |
作者:关宇晟
版本:V2019.07.05
***********************************/

#include "main.h"
#include "LogStore.h"

#define		DATA_MAX		8
#define		LOGBLOCKMAX		8

#define		LOG_ROOM_MAX	432	 	//432个日志区
#define		STORE_ERR_CODE	0XEEEE
typedef __packed struct
{
	unsigned char 			Magic; 	//0x55
	unsigned short 			Length;	//存储内容长度	
	unsigned char 			Chksum;	//校验和
	unsigned short			PointerAddr;
}LOGSTORE_CFG;

LOGSTORE_CFG	LogStoteConfig;
static LOGSTORE_CFG	*pthis = NULL;

u8 s_LogData[sizeof(LOGSTORE_CFG)] ;
u16 s_LogErrCode = 0;

void SetLogErrCode(unsigned short ErrCode)
{
	if(ErrCode == 0xFF)
	{
		s_LogErrCode = 0;
	}
	else if(ErrCode < 16)
	{
		s_LogErrCode |= (1<<ErrCode);
	}
}

void ClearLogErrCode(unsigned short ErrCode)
{
	s_LogErrCode &= ~(1<<ErrCode);
}

void LogStorePointerInit(void)
{
	if (Check_Area_Valid(LOG_STORE_POINTER_ADDR))
	{
		EEPROM_ReadBytes(LOG_STORE_POINTER_ADDR, s_LogData, sizeof(LOGSTORE_CFG));
		
		pthis = (LOGSTORE_CFG *)s_LogData;
		
		LogStoteConfig = *pthis;
	}
	else	//第一次上电初始化
	{
		LogStoteConfig.Magic 		= VAILD;
		LogStoteConfig.Length 		= 0X02;
		LogStoteConfig.PointerAddr 	= 0;
		LogStoteConfig.Chksum = Calc_Checksum((unsigned char *)&LogStoteConfig.PointerAddr, 2);
		EEPROM_WriteBytes(LOG_STORE_POINTER_ADDR, (unsigned char *)&LogStoteConfig, sizeof(LOGSTORE_CFG));
		u1_printf(" LogWrite the default value for the first power on\r\n");
	}
}


unsigned short ReadPointerAddr(void)
{
	if (Check_Area_Valid(LOG_STORE_POINTER_ADDR))
	{
		EEPROM_ReadBytes(LOG_STORE_POINTER_ADDR, s_LogData, sizeof(LOGSTORE_CFG));
		
		pthis = (LOGSTORE_CFG *)s_LogData;
		LogStoteConfig = *pthis;
		
		return LogStoteConfig.PointerAddr;
	}
	else
	{
		return 999;
	}
}

void WritePointerAddr(unsigned short NewAddr)
{
	pthis->PointerAddr = NewAddr;
	pthis->Chksum = Calc_Checksum((unsigned char *)&pthis->PointerAddr, 2);
	EEPROM_WriteBytes(LOG_STORE_POINTER_ADDR, (unsigned char *)pthis, sizeof(LOGSTORE_CFG));
}

void WriteLogData(unsigned char *DataBuf)
{
	unsigned short PointerAddr;
	unsigned int AbsoluteAddress = 0;
	PointerAddr = ReadPointerAddr();
	
	if(PointerAddr == 999)
	{
		u1_printf(" Pointer Addr Err\r\n");
	}
	
	AbsoluteAddress = LOG_STORE_START_ADDR + PointerAddr*LOGBLOCKMAX;
	EEPROM_WriteBytes(AbsoluteAddress, (unsigned char *)DataBuf, DATA_MAX);
	
	u1_printf(" Write Log (%d)\r\n", PointerAddr);
	PointerAddr++;
	if(PointerAddr >= LOG_ROOM_MAX-1)
	{
		PointerAddr = 0;
	}
	WritePointerAddr(PointerAddr);
	
}
void StoreOperationalData(void)
{
	unsigned short Vol = 0;
	unsigned char StoreBuf[50], i = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
	
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	
	StoreBuf[i++] = RTC_DateStruct.RTC_Date;
	StoreBuf[i++] = RTC_TimeStructure.RTC_Hours;
	StoreBuf[i++] = RTC_TimeStructure.RTC_Minutes;
	StoreBuf[i++] = RTC_TimeStructure.RTC_Seconds;
	StoreBuf[i++] = Vol >> 8;
	StoreBuf[i++] = Vol&0xff;
	StoreBuf[i++] = s_LogErrCode >> 8;
	StoreBuf[i++] = s_LogErrCode&0xff;
	
	WriteLogData(StoreBuf);
}

void ShowLogContent(void)
{
	unsigned short i;
	unsigned char Buf[DATA_MAX];
	
	u1_printf(" Pointer Addr:%d\r\n", ReadPointerAddr());
	for(i=0; i<LOG_ROOM_MAX; i++)
	{
		if(i%30 == 0)
		{
			IWDG_ReloadCounter();
		}
		memset(Buf, 0, DATA_MAX);
		EEPROM_ReadBytes(LOG_STORE_START_ADDR + i*LOGBLOCKMAX, Buf, DATA_MAX);
		
		if(((Buf[4]) + Buf[5]))	//只是判断有无数据
		{
			u1_printf("%03d.(%d)[%02d:%02d:%02d] Vol:%d ", i, Buf[0], Buf[1], Buf[2], Buf[3], ((Buf[4]<<8) + Buf[5]));
					
			if(Buf[7] &0x01)
			{
				u1_printf(" No ME909s |");
			}
			if(Buf[7] &0x02)
			{
				u1_printf(" Weak Signal |");
			}
			if(Buf[7] &0x04)
			{
				u1_printf(" Console On |");
			}
			if(Buf[7] &0x08)
			{
				u1_printf(" Timeout |");
			}
			if(Buf[7] &0x10)
			{
				u1_printf(" Success |");
			}			
			if(Buf[7] &0x20)
			{
				u1_printf(" Post Fail |");
			}
			if(Buf[7] &0x40)
			{
				u1_printf(" No Server Ack |");
			}
			if(Buf[7] &0x80)
			{
				u1_printf(" No Card |");
			}

			if(Buf[6] &0x01)
			{
				u1_printf(" Photo Fail |");
			}
			if(Buf[6] &0x02)
			{
				u1_printf(" No Hi3519 |");
			}
			if(Buf[6] &0x04)
			{
				u1_printf(" Night |");
			}
			if(Buf[6] &0x08)
			{
				u1_printf(" SensorErr");
			}
			if(Buf[6] &0x10)
			{
				u1_printf(" GPS OK |");
			}		
			if(Buf[6] &0x20)
			{
				u1_printf(" GPS Fail |");
			}
			if(Buf[6] &0x40)
			{
				u1_printf(" HardErr |");
			}
			if(Buf[6] &0x80)
			{
				u1_printf(" UNICOM |");
			}			
			
			u1_printf("\r\n");
		}
	}
}


























