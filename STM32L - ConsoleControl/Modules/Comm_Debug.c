/**********************************
说明:慧云配置工具串口通信程序
	  
作者:关宇晟
版本:V2017.4.3
***********************************/
#include "Comm_Debug.h"
#include "main.h"

#define		MAX_BUFF	100
#define		COM_DATA_SIZE	100

static void Debug_Send_Packet(u8 *packet, u8 len)
{
	u8 send_buf[MAX_BUFF];
	u8 send_len;
	
	send_len = PackMsg(packet, len, send_buf, MAX_BUFF);			//数据包转义处理					
	USART1_DMA_Send(send_buf, send_len);	
}

static void Debug_Read_SystemInfo(u8 *data, u8 lenth)
{
	u8 send_buf[128];
	u32 send_len;
	
	SYSTEM_INFO 	*sys_info;
	
	CLOUD_HDR *hdr;
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEM_INFO));
	sys_info = Get_SystemInfo();
	memcpy(&send_buf[sizeof(CLOUD_HDR)], sys_info, sizeof(SYSTEM_INFO));
	send_len += sizeof(SYSTEM_INFO);
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
}


static void Debug_Read_SystemConfig(u8 *data, u8 lenth)
{
	u8 send_buf[128];
	u32 send_len;
	
	SYSTEMCONFIG 	*sys_cfg;
	
	CLOUD_HDR *hdr;
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG));
	sys_cfg = GetSystemConfig();
	memcpy(&send_buf[sizeof(CLOUD_HDR)], sys_cfg, sizeof(SYSTEMCONFIG));
	send_len += sizeof(SYSTEMCONFIG);
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
}

static unsigned char Debug_Set_SystemConfig(u8 *data, u8 lenth)
{
	u8 send_buf[64], ReturnVal = 0;
	u32 send_len;
	SYSTEMCONFIG 	*sys_cfg;
	
	CLOUD_HDR *hdr;
	
	sys_cfg = (SYSTEMCONFIG *)&data[sizeof(CLOUD_HDR)];
	ReturnVal = Set_System_Config(sys_cfg);
	if(ReturnVal == FALSE)
	{
		return FALSE;
	}
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = 0;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	

	return TRUE;
}





static void Debug_Enter_Test_Mode(u8 *data, u32 len)
{
	CLOUD_HDR *hdr;
	u8 send_buf[64];
	u32 send_len;
	
	if(g_ComTestFlag == FALSE)
	{
		g_ComTestFlag = TRUE;	
	}
	else
	{
		g_ComTestFlag = FALSE;
		App_Run();
	}
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(u32));
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);			
}



void OnDebug(u8 *data, u8 lenth)
{
	u8 ReturnVal = 0, Point;
	u32 Hor, Ver;
	CLOUD_HDR *hdr;

	hdr = (CLOUD_HDR *)data;
	switch(hdr->cmd)
	{
//		case CMD_RD_SYS_INFO:	//读取系统信息 --0xF3
//			Debug_Read_SystemInfo(data, lenth);
//			break;
//	
//		case CMD_RD_SYS_CFG:	//读取系统配置 --0xF5
//			Debug_Read_SystemConfig(data, lenth);
//			break;
//		case CMD_WR_SYS_CFG:	//设置系统配置 --0xF6
//			ReturnVal = Debug_Set_SystemConfig(data, lenth);
//			if(ReturnVal == FALSE)
//			{
//				u1_printf(" Config Err Data\r\n");
//				return;
//			}

//			break;

//		case CMD_TEST_MODE:
//			Debug_Enter_Test_Mode(data, lenth);		//进入低功耗设备配置模式	关闭通信，不进入停止模式
//			break;
		
		case CMD_CONTROL_STOP://停
			u1_printf(" Stop\r\n");
			MotorControl_Stop();
		break;
		
		case CMD_CONTROL_UP://上
			u1_printf(" UpUp\r\n");
			MotorControl_Up(VER_PLUSE);
		break;
		
		case CMD_CONTROL_DOWN://下
			u1_printf(" Down\r\n");
			MotorControl_Down(VER_PLUSE);
		break;
		
		case CMD_CONTROL_LEFT://左
			u1_printf(" Left\r\n");
			MotorControl_Left(HOR_PLUSE);
		break;
		
		case CMD_CONTROL_RIGHT://右
			u1_printf(" Right\r\n");
			MotorControl_Right(HOR_PLUSE);
		break;
		
		case CMD_CONTROL_GO_TO_PRESET://转至预置点
			Point = (data[sizeof(CLOUD_HDR)]);
			u1_printf(" Go to %d\r\n", Point);
		break;
		
		case CMD_CONTROL_SET_PRESET://设置预置点
			Point = (data[sizeof(CLOUD_HDR)]);
			u1_printf(" Set %d\r\n", Point);
		break;
		
		case CMD_CONTROL_CLEAR_PRESET://删除预置点
			Point = (data[sizeof(CLOUD_HDR)]);
			u1_printf(" Del %d\r\n", Point);
		break;
		
		case CMD_CONTROL_GO_TO_ANGLE:	//转至指定角度
		
			Hor = (data[sizeof(CLOUD_HDR)]) + (data[sizeof(CLOUD_HDR)+1] << 8);
			Ver = data[sizeof(CLOUD_HDR)+2] + (data[sizeof(CLOUD_HDR)+3] << 8);
		
			u1_printf(" Angle: %d %d\r\n", Hor, Ver);		
		
			Hor *= HOR_ANGLETOPLUSE;
		
			Ver *= VER_ANGLETOPLUSE;
		
			if(Hor > MAX_HOR_PLUSE)
			{
				Hor = MAX_HOR_PLUSE;
			}
			
			if(Ver > MAX_VER_PLUSE)
			{
				Ver = MAX_VER_PLUSE;
			}
			
			u1_printf(" Pluse: %d %d\r\n", Hor, Ver);	
								
			MotorControl_GotoAngle(Hor, Ver);
			
		break;
		
		case CMD_CONTROL_AUTO://自检操作
			u1_printf(" Auto\r\n");
			MotorAutoRun();
		break;
		
		case CMD_RESET:
			u1_printf("\r\n Reset Cmd...\r\n");
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			Sys_Soft_Reset();
			break;
	}
}
