#include "main.h"
#include "Arithmetic.h"

#define		AMPLITUDE_LIMITING		10

unsigned short PackMsg(u8* srcbuf,u16 srclen,u8* destbuf,u16 destsize)
{
	u16 i,j;

	j = 0;
	i = 0;
	destbuf[j++] = 0x7e;
	for (i=0;i<srclen;i++)
	{
		switch (srcbuf[i])
		{
		case 0x7d:
			destbuf[j++]=0x7d;
			destbuf[j++]=0x5d;
			break;		
		case 0x7e:
			destbuf[j++]=0x7d;		
			destbuf[j++]=0x5e;
			break;
		case 0x21:
			destbuf[j++]=0x7d;		
			destbuf[j++]=0x51;
			break;
		default:
			destbuf[j++]=srcbuf[i];
			break;
		}
		if ( j > destsize- 2) break;
	}

	destbuf[j++] = 0x21;

	return j;
}

void Int32ToArray( unsigned char *pDst, unsigned int value )
{
	pDst[0] = (value>>24)&0xff;
	pDst[1] = (value>>16)&0xff;
	pDst[2] = (value>>8)&0xff;
	pDst[3] = value&0xff;
}
unsigned char CheckHdrSum(CLOUD_HDR *pMsg, u8 *data, u8 lenth)
{
	u8 checksum = 0, i;
	
	checksum += pMsg->protocol & 0xff;
	checksum += pMsg->protocol >> 8;
	checksum += pMsg->device_id & 0xff;
	checksum += (pMsg->device_id >> 8) & 0xff;
	checksum += (pMsg->device_id >> 16) & 0xff;
	checksum += (pMsg->device_id >> 24) & 0xff;
	checksum += pMsg->dir;
	checksum += pMsg->seq_no & 0xff;
	checksum += pMsg->seq_no >> 8;
	checksum += pMsg->payload_len & 0xff;
	checksum += pMsg->payload_len >> 8;
	checksum += pMsg->cmd;
	for(i=0; i<lenth; i++)
	{
		checksum += data[i];
	}
	return checksum;
}
float encode_float(float value)
{
	unsigned char buf[4];
	float result;
	unsigned char *ptr;
	
	ptr = (unsigned char *)&value;
	
	buf[0] = ptr[1];   //次低
	buf[1] = ptr[0];   //低
	buf[2] = ptr[3];   //高
	buf[3] = ptr[2];   //次高
	
	result = *(float *)&buf[0];
	
	return result;
}

float Adc_Filter(float * filter_data, unsigned short len) 
{ 
	unsigned short i, j = 0, Count = 0; 
	float filter_sum = 0, filter_avg = 0; 
	float filter_max, filter_min; 
	float FilterBuf[ADC_BUFF_COUNT];
	
	if(len < 3)
	{
		return 0;
	}
	
	for(i=0; i<len; i++)	//求平均值
	{
		filter_sum += filter_data[i];
	}
	
	filter_avg = filter_sum/len;
	
	j = 0;
	
	for(i=0; i<len; i++)	//剔除大于限值的元素
	{
		if(filter_data[i] > filter_avg)
		{
			if(filter_data[i] - filter_avg > AMPLITUDE_LIMITING)
			{
				continue;
			}
		}
		else
		{
			if(filter_avg - filter_data[i] > AMPLITUDE_LIMITING)
			{
				continue;
			}
		}
		
		FilterBuf[j++] = filter_data[i];

	}
	
//	u1_printf("j=%d avg=%.1f ", j, filter_avg);
	Count = j;
	if(j < 3)
	{
		return filter_avg;
	}
	else
	{
		
	}
	filter_max = FilterBuf[0]; 
	filter_min = FilterBuf[0]; 
	filter_sum = FilterBuf[0]; 
	for(i = Count - 1; i > 0; i --) 
	{ 
		if(FilterBuf[i] > filter_max) 				//找出最大值
			filter_max = FilterBuf[i]; 
		else if(FilterBuf[i] < filter_min)			//找出最小值 
			filter_min = FilterBuf[i]; 
		filter_sum = filter_sum + FilterBuf[i];	//求和 
	} 
	i = Count - 2; 
	filter_sum = filter_sum - filter_max - filter_min ; 
	filter_sum = filter_sum / i; 
	
	return filter_sum; 
}

float Mid_Filter(float * filter_data, int16 len) 
{ 
	int16 i; 
	float filter_sum = 0; 
	float filter_max, filter_min; 

	filter_max = filter_data[0]; 
	filter_min = filter_data[0]; 
	filter_sum = filter_data[0]; 
	for(i = len - 1; i > 0; i --) 
	{ 
		if(filter_data[i] > filter_max) 				//找出最大值
			filter_max = filter_data[i]; 
		else if(filter_data[i] < filter_min)			//找出最小值 
			filter_min = filter_data[i]; 
		filter_sum = filter_sum + filter_data[i];	//求和 
	} 
	i = len - 2; 
	filter_sum = filter_sum - filter_max - filter_min ; 
	filter_sum = filter_sum / i; 
	
	return filter_sum; 
}

float GetAverageValue(float * data, unsigned char len) 
{
	u8 i;
	float Sum = 0;
	
	for(i=0; i<len; i++)
	{
		Sum += data[i];
	}
	
	Sum = Sum/len;
	
	return Sum;
	
}

unsigned int ArrayToInt32(unsigned char *pDst, unsigned char count)
{
	unsigned int Result = 0;
	unsigned char i;
	
	for(i=0; i<count; i++)
	{
		Result = (pDst[i] - '0') + Result*10;
	}
	
	return Result;
}


int toupper(int c)
{
	if ((c >= 'a') && (c <= 'z'))
		return c + ('A' - 'a');
	return c;
}

void HexStrToByte(const char* source, unsigned char* dest, int sourceLen)
{
    short i;
    unsigned char highByte, lowByte;
    
    for (i = 0; i < sourceLen; i += 2)
    {
        highByte = toupper(source[i]);
        lowByte  = toupper(source[i + 1]);


        if (highByte > 0x39)
            highByte -= 0x37;
        else
            highByte -= 0x30;


        if (lowByte > 0x39)
            lowByte -= 0x37;
        else
            lowByte -= 0x30;


        dest[i / 2] = (highByte << 4) | lowByte;
    }
    return ;
}

void StrToHex(unsigned char *pbDest, unsigned char *pbSrc, int nLen)
{
	char h1,h2;
	unsigned char s1,s2;
	int i;

	for (i=0; i<nLen; i++)
	{
		h1 = pbSrc[2*i];
		h2 = pbSrc[2*i+1];

		s1 = toupper(h1) - 0x30;
		if (s1 > 9) 
		{
			s1 -= 7;
		}

		s2 = toupper(h2) - 0x30;
		if (s2 > 9) 
		{
			s2 -= 7;
		}

		pbDest[i] = s1*16 + s2;
	}
}

unsigned short Calculate_CRC16 ( unsigned char *arr_buff, unsigned short len)
{
	unsigned short crc=0xFFFF;
	unsigned char i, j;
	
	for ( j=0; j<len; j++)
	{
		crc=crc ^*arr_buff++;
		
		for ( i=0; i<8; i++)
		{
			if( ( crc&0x0001) >0)
			{
				crc=crc>>1;
				crc=crc^ 0xa001;
			}
			else
			{
				crc=crc>>1;
			}
		}
	}
	
	return ( crc);
}

unsigned char Calc_Checksum(unsigned char *buf, unsigned int len)
{
	unsigned int chksum;
	int i;

	chksum = 0;
	for (i=0;i<len;i++)
	{
		chksum += buf[i];
	}
	return (chksum & 0xff);
}
unsigned int swap_dword(unsigned int value)
{
	unsigned char b1,b2,b3,b4;
	unsigned int result;
	
	b1 = (value>>24)&0xff;
	b2 = (value>>16)&0xff;
	b3 = (value>>8)&0xff;
	b4 = value&0xff;
	
	result = (b4<<24)+(b3<<16)+(b2<<8)+b1;
	
	return result;
}
unsigned short int swap_word(unsigned short int value)
{
	unsigned char b1, b2;
	unsigned short int result;
	
	b1 = (value>>8)&0xff;
	b2 = (value&0xff);
	
	result = (b2<<8)+b1;
	
	return result;
}

unsigned short UnPackMsg(unsigned char *sbuf, unsigned short slen, unsigned char *dbuf, unsigned short *dlen)
{
	int i,j;

	j = 0;
	i = 0;
	while(i<slen)
	{
		if (sbuf[i] == CTRL_VAL)
		{
			switch(sbuf[i+1])
			{
				case 0x5e:
					dbuf[j] = BOF_VAL;
					break;
				case 0x5d:
					dbuf[j] = CTRL_VAL;
					break;
				case 0x51:
					dbuf[j] = EOF_VAL;
					break;
				default:
					return 0;
//					break;
			}
			
			i += 2;
		}
		else
		{
			dbuf[j] = sbuf[i];
			i++;
		}

		j++;
	}

	(*dlen) = j;
	
	return 1;
}

unsigned short BBC_Checksum(char *Data, unsigned char Lenth)
{
	unsigned short Checksum = 0, i = 0;
	
	while(Lenth--)
	{
		Checksum ^= (unsigned char)Data[i++];
	}

	return Checksum;
}

int GetIPAddress(char *strIP, char *ArrayIP)
{
    int d[5]={-1},di=1;
    int i,j,m;
    char p[20];
	
//    p=(char *)malloc(sizeof(char)*strlen(strIP));
    strcpy(p,strIP);  
     
    for(i=strlen(p)-1;i>=0;i--)
        if(p[i]!='.' && (p[i]<'0' || p[i]>'9'))
        for(j=i;j<strlen(p);j++)
             p[j]=p[j+1];
 
    if (p[0]=='.') return 1;   
    for(i=0;i<strlen(p);i++)
        if(p[i]=='.') {
            d[di++]=i; 
            if (di>4) return 1;  
        }
    if (di!=4) return 1;  
    d[4]=strlen(p);
 

    for(i=0;i<4;i++){
        m=0;       
        for(j=d[i]+1;j<=d[i+1]-1;j++)
            m=m*10+p[j]-'0';
        if (m<0 || m>255) return 1; 
        ArrayIP[i]=m;
    }
//    free(p);
    return 0;
} 

