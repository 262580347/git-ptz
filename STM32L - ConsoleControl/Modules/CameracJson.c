/**********************************
说明:远程相机cJSON协议通信
作者:关宇晟
版本:V2019.5.24
***********************************/
#include "CameracJson.h"
#include "main.h"

#define		DET_COM_NUM			0
#define		GET_STATUS_NUM		1
#define		POWER_OFF_NUM		2
#define		ACK_NUM				5
#define		ACK_NUM_MAX			10

typedef struct
{
	unsigned char 	CMD;
	unsigned char 	Dir;
	unsigned int 	Seq;
	unsigned char 	Err;
	unsigned char 	isWorking;
}JSON_COMM_MSG; 

//通信指令
#define		CMD_DET_COMMUNICATION	0		//检测通信
#define		CMD_GETSTM32DATA		1		//核心板从STM32读取数据
#define		CMD_GETHI3519STATUS		2		//STM32从核心板读取状态
#define		CMD_CONTROL				3		//核心板给STM32进行一些控制动作
#define		CMD_GET_STM32CONFIG		4		//核心板从STM32读取配置参数
#define		CMD_SET_STM32CONFIG		5		//核心板给STM32设置系统参数

#define		CMD_TAKE_PHOTO			6		//STM32主动发送拍照指令
#define		CMD_POWEROFF_HI3519		8		//STM32给核心板关机
#define		CMD_EXITPROCESS_HI3519	9		//STM32给核心板退出进程指令

#define		CMD_MASTER_SEND			0		//主动发送
#define		CMD_SLAVE_ACK			1		//从机回复

#define		WAITING_MSG_TIME		11000	//重发间隔,任务周期
#define		MAX_MSG_RETRY_COUNT		3		//最大重发数

static unsigned char s_Hi3519LinkFlag = FALSE;	//核心板通信标志

static unsigned char s_TimeToPowerOffHi3519Flag = FALSE;	//可以关闭核心板电源
	
static JSON_COMM_MSG	cJsonMsgInfo[ACK_NUM_MAX];		//JSON信息存储，用于重发、应答

static unsigned char s_STM32GetStatusRetryCount = 0;	//传感器数据重发次数

static unsigned char s_STM32DetCommunicationCount = 0;		//STM32测试与核心板通信重发次数

static unsigned char s_STM32PowerOffCount = 0;		//STM32关闭核心板通信重发次数

static void OnRxMatchcJsonMsg(JSON_COMM_MSG *cJsonMsg);//应答匹配

static unsigned char s_Hi3519ErrCode = 0;

static ME909STATUS s_Me909Status = MeNoModule;

ME909STATUS GetMe909Status(void)
{
	return s_Me909Status;
}

unsigned char GetHi3519ErrCode(void)	//通信错误码
{
	return s_Hi3519ErrCode;
}
//设置核心板通信状态
void SetHi3519LinkFlag(unsigned char isTrue)
{
	s_Hi3519LinkFlag = isTrue;
}
//获取核心板通信状态
unsigned char GetHi3519LinkFlag(void)
{
	return s_Hi3519LinkFlag;
}
//设置核心板电源标志
void SetTimeToPowerOffHi3519Flag(unsigned char isTrue)
{
	s_TimeToPowerOffHi3519Flag = isTrue;
}
//获取核心板电源标志
unsigned char GetTimeToPowerOffHi3519Flag(void)
{
	return s_TimeToPowerOffHi3519Flag;
}
//发送CJSON协议包应答包
static void SendcJsonACK(JSON_COMM_MSG *cJsonMsg)
{
	cJSON *Root, *Data;
	char *cJsonBuf, strTime[30], strIP[20], HttpBuf[128];
	u8  Http_Length;
	u16 MsgLen = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStructure;	
	float Latitude = 0, Longitude = 0;
	SYSTEMCONFIG *p_sys;
	
		
	Root = cJSON_CreateObject();	
	
	cJSON_AddNumberToObject(Root,"CMD", 		cJsonMsg->CMD);
	cJSON_AddNumberToObject(Root,"Dir",			CMD_SLAVE_ACK);
	cJSON_AddNumberToObject(Root,"Seq",			cJsonMsg->Seq);	
	cJSON_AddNumberToObject(Root,"Err",			0);	

	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
	sprintf(strTime, "%d-%02d-%02d %02d:%02d:%02d", RTC_DateStructure.RTC_Year + 2000, RTC_DateStructure.RTC_Month, RTC_DateStructure.RTC_Date, RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
	cJSON_AddStringToObject(Root, "DateTime",strTime);
	
	switch(cJsonMsg->CMD)
	{
		case CMD_GETSTM32DATA:	//读取传感器数据应答
			
			cJSON_AddItemToObject(Root, "Msg", 		Data = cJSON_CreateObject());
		
			cJSON_AddNumberToObject(Data,"BatVol",		Get_Battery_Vol());
			
			cJSON_AddNumberToObject(Data,"Ill",			Get_Ill_Value());
			
			cJSON_AddNumberToObject(Data,"Pos",			GetConsolePosition());

			GetPosition(&Latitude, &Longitude);
			cJSON_AddNumberToObject(Data,"Latitude",	Latitude);

			cJSON_AddNumberToObject(Data,"Longitude",	Longitude);
		break;
		
		case CMD_CONTROL:	//控制应答
			
			cJSON_AddItemToObject(Root, "Msg", 		Data = cJSON_CreateObject());
		
			if(GetConsolePowerFlag())	//云台电源状态
			{
				cJSON_AddStringToObject(Data,	"ConPower",		"On");
			}
			else
			{
				cJSON_AddStringToObject(Data,	"ConPower",		"Off");
			}
			
			if(GetGPSPowerFlag())		//GPS电源状态
			{
				cJSON_AddStringToObject(Data,	"GPSPower",		"On");
			}
			else
			{
				cJSON_AddStringToObject(Data,	"GPSPower",		"Off");
			}
		
		break;
		
		case CMD_GET_STM32CONFIG:	//获取配置参数应答
			
			p_sys = GetSystemConfig();	
		
			cJSON_AddItemToObject(Root, "Msg", 		Data = cJSON_CreateObject());
		
			cJSON_AddNumberToObject(Data,"Device",		p_sys->Device_ID);
			
			sprintf(strIP, "%d.%d.%d.%d", p_sys->Gprs_ServerIP[0], p_sys->Gprs_ServerIP[1], p_sys->Gprs_ServerIP[2], p_sys->Gprs_ServerIP[3]);
			cJSON_AddStringToObject(Data,"IP",			strIP);
			
			cJSON_AddNumberToObject(Data,"Port",			p_sys->Gprs_Port);
		
			Http_Length = Check_Area_Valid(HTTP_SERVER_ADDR);
			if (Http_Length >= 4 && Http_Length <= 120)
			{
				EEPROM_ReadBytes(HTTP_SERVER_ADDR, (u8 *)HttpBuf, sizeof(SYS_TAG) +  Http_Length);
				HttpBuf[sizeof(SYS_TAG) +  Http_Length] = 0;
			}	
			else if(Http_Length > 120)
			{
				memset(HttpBuf,0, sizeof(HttpBuf));
				u1_printf(" HttpServer Over\r\n");
			}
			cJSON_AddStringToObject(Data,"HttpServer",			&HttpBuf[sizeof(SYS_TAG)]);
			
			cJSON_AddNumberToObject(Data,"PhotoInterVal",			p_sys->Heart_interval);
			
			cJSON_AddNumberToObject(Data,"DataInterVal",			p_sys->Data_interval);
		
		break;
		
		case CMD_SET_STM32CONFIG:	//设置配置参数应答
			
		break;
		
	}
	cJsonBuf = cJSON_PrintUnformatted(Root);	
	
	MsgLen = strlen(cJsonBuf);
	
	cJSON_Delete(Root);	
	
	u2_printf("%s\r", cJsonBuf);
	
	u1_printf("<- <- <- Send cJson Ack(Size:%d) (Cmd:%d)(Seq:%d)%s\r\n", MsgLen, cJsonMsg->CMD, cJsonMsg->Seq, cJsonBuf);
	
	myfree(cJsonBuf);
	
	u1_printf(" Memory utilization:%d%%\r\n\r\n", mem_perused());
}
//通过核心板设置相机参数
static void SetHttpServer(char *strHttpServer, u8 Length)
{
	SYS_TAG tag;
	char HttpBuf[128];
	
	if(Length > 120)
	{
		u1_printf(" Http Length Over\r\n");		
	}
	else
	{
		tag.magic = VAILD;
		tag.length = Length;
		tag.chksum = Calc_Checksum((unsigned char *)strHttpServer, Length);
		
		memcpy(HttpBuf, &tag, sizeof(SYS_TAG));
		memcpy(&HttpBuf[sizeof(SYS_TAG)], strHttpServer, Length);
		
		EEPROM_EraseWords(HTTP_BLOCK1);
		EEPROM_EraseWords(HTTP_BLOCK2);
		EEPROM_WriteBytes(HTTP_SERVER_ADDR, (unsigned char *)HttpBuf, sizeof(SYS_TAG) + Length);
	}

}
//解析CJSON协议包
static int DecodecJsonMsg(void)
{
	cJSON *Root, *Temp, *Child, *tNode;
	JSON_COMM_MSG cJsonMsg;
	char *Output, ArrayIP[4], strServer[120], Err;
	u8 Cmd = 0, Dir = 0, HttpLength = 0;
	unsigned char Year, Month,Date, Hours, Minutes, Seconds;
	unsigned char DateBuf[30], len = 0;
	unsigned int SumofSeconds = 0;
	u32 Seq = 0;
	u8 i, ConfigCount = 0;
	SYSTEMCONFIG *p_sys, NewConfig;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
	
	
	Root = cJSON_Parse((const char *)g_USART2_RX_BUF);
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	if (!Root) 
	{
		u1_printf(" No cJson:[%s]\r\n", cJSON_GetErrorPtr());
	}
	else
	{	
		Temp = cJSON_GetObjectItem(Root, "CMD");
		if(Temp != NULL)
		{
			Cmd = Temp->valueint;
			
		}
		else
		{
			return -1;
		}
		
		Temp = cJSON_GetObjectItem(Root, "Dir");
		if(Temp != NULL)
		{	
			Dir = Temp->valueint;
			if(Dir == CMD_MASTER_SEND)
			{
				u1_printf("\r\n [%0.2d:%0.2d:%0.2d] Hi3519 -> STM32(%d)\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, strlen((const char *)g_USART2_RX_BUF));
			}
			else if(Dir == CMD_SLAVE_ACK)
			{
				u1_printf("\r\n [%0.2d:%0.2d:%0.2d] Hi3519 Ack STM32(%d)\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, strlen((const char *)g_USART2_RX_BUF));
			}
		}
		else
		{
			return -1;
		}
		
		Temp = cJSON_GetObjectItem(Root, "Seq");
		if(Temp != NULL)
		{
			Seq = Temp->valueint;
//			u1_printf("Seq:%d,", Temp->valueint);			
		}
		else
		{
			return -1;
		}
			
		Temp = cJSON_GetObjectItem(Root, "Err");
		if(Temp != NULL)
		{
			if(Temp->valueint)
			{
				s_Hi3519ErrCode = Temp->valueint;
				u1_printf("!!!!!!!!!!!!Err: (%d) !!!!!!!!!!!!!!\r\n", Temp->valueint);
			}
			else
			{
				s_Hi3519ErrCode = 0;
			}
			
		}
		else
		{
			
		}
			
		Temp = cJSON_GetObjectItem(Root, "DateTime");
			
		if(Temp != NULL)
		{
			len = strlen(Temp->valuestring);
			if(len < 30)
			{
				u1_printf(" Rec Datetime:%s\r\n", Temp->valuestring);
				memcpy(DateBuf, Temp->valuestring, len);
				Year = ArrayToInt32(&DateBuf[2], 2);
				Month = ArrayToInt32(&DateBuf[5], 2);
				Date = ArrayToInt32(&DateBuf[8], 2);
				Hours = ArrayToInt32(&DateBuf[11], 2);
				Minutes = ArrayToInt32(&DateBuf[14], 2);
				Seconds = ArrayToInt32(&DateBuf[17], 2);
				SumofSeconds = Hours*3600 + Minutes*60 + Seconds;
				
				RTC_TimeShow();
				
				RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
				
				if(Year > 17 && Year < 37) 
				{
					if(GetRTCSecond() > SumofSeconds)
					{
						if((DifferenceOfRTCTime(GetRTCSecond(), SumofSeconds) > 120) || RTC_DateStruct.RTC_Year != Year || RTC_DateStruct.RTC_Month != Month || RTC_DateStruct.RTC_Date != Date)
						{
							SysSetRTCTime(Year, Month, Date, Hours, Minutes, Seconds);
						}
					}
					else
					{
						if((DifferenceOfRTCTime(SumofSeconds, GetRTCSecond()) > 120) || RTC_DateStruct.RTC_Year != Year || RTC_DateStruct.RTC_Month != Month || RTC_DateStruct.RTC_Date != Date)
						{
							SysSetRTCTime(Year, Month, Date, Hours, Minutes, Seconds);
						}
					}	
				}		
				else
				{
					u1_printf("Time out of sync\r\n");
				}
			}
			else
			{
				u1_printf(" Time Type Err\r\n");
			}
		}
		else
		{

		}
					
		if(Dir == CMD_MASTER_SEND)	//收到Hi3519消息，回复应答
		{
			for(i=ACK_NUM; i<ACK_NUM_MAX; i++)
			{
				if(cJsonMsgInfo[i].isWorking == FALSE)
				{
					cJsonMsgInfo[i].CMD = Cmd;
					cJsonMsgInfo[i].Dir = CMD_SLAVE_ACK;
					cJsonMsgInfo[i].Seq = Seq;
					cJsonMsgInfo[i].isWorking = TRUE;
					break;
				}
			}
		}
		else	//收到核心板应答，注销发送任务		
		{
			cJsonMsg.CMD = Cmd;
			cJsonMsg.Dir = Dir;
			cJsonMsg.Seq = Seq;
			OnRxMatchcJsonMsg(&cJsonMsg);
		}
		
		switch(Cmd)
		{
			case CMD_DET_COMMUNICATION:		//检测通信
				u1_printf(" Normal communication with core board:");
				s_Hi3519LinkFlag = TRUE;
			break;
			
			case CMD_GETSTM32DATA:			//核心板从STM32读取数据,两STM32测试使用，实际应为核心板发起通信
				u1_printf(" Hi3519 Get Sensor\r\n");
			
				Child = cJSON_GetObjectItem(Root, "Msg");	
				if((Child != NULL) && (Child->type == cJSON_Object))
				{	
					tNode = cJSON_GetObjectItem(Child, "BatVol");
					if(tNode != NULL)
					{
						u1_printf("BatVol:%2.1f\r\n", tNode->valueint);
					}
					
					tNode = cJSON_GetObjectItem(Child, "Ill");
					if(tNode != NULL)
					{
						u1_printf("Ill:%2.1f\r\n", tNode->valuedouble);
					}
					
					tNode = cJSON_GetObjectItem(Child, "Pos");
					if(tNode != NULL)
					{
						u1_printf("Pos:%2.1f\r\n", tNode->valueint);
					}
					
					tNode = cJSON_GetObjectItem(Child, "Latitude");
					if(tNode != NULL)
					{
						u1_printf("Latitude:%2.3f\r\n", tNode->valuedouble);
					}
					
					tNode = cJSON_GetObjectItem(Child, "Longitude");
					if(tNode != NULL)
					{
						u1_printf("Longitude:%2.3f\r\n", tNode->valuedouble);
					}
				}	
				
			break;
			
			case CMD_GETHI3519STATUS:		//STM32从核心板读取状态
				u1_printf(" Get core board status:");
				Child = cJSON_GetObjectItem(Root, "Msg");
				if(cJSON_Object == Child->type)	//是对象
				{
					tNode = cJSON_GetObjectItem(Child, "Sys");
					
					if(tNode != NULL)
					{
//						u1_printf("%s\r\n", tNode->valuestring);
						if(strncmp((char *)tNode->valuestring, "OK", strlen(tNode->valuestring)) == 0)
						{
							
						}
					}
					
					tNode = cJSON_GetObjectItem(Child, "ME909");
					
					if(tNode != NULL)
					{
	//					u1_printf("%s\r\n", tNode->valuestring);
						if(strncmp((char *)tNode->valuestring, "NotConnect", strlen(tNode->valuestring)) == 0)	//通信完成,可以关闭核心板电源
						{
							s_TimeToPowerOffHi3519Flag = FALSE;
							SetLogErrCode(LOG_CODE_NOMODULE);
							s_Me909Status = MeNoModule;
						}
						else if(strncmp((char *)tNode->valuestring, "Connecting", strlen(tNode->valuestring)) == 0)	
						{
							s_TimeToPowerOffHi3519Flag = FALSE;
							SetLogErrCode(LOG_CODE_TIMEOUT);
							s_Me909Status = MeConnecting;
						}
						else if(strncmp((char *)tNode->valuestring, "Connected", strlen(tNode->valuestring)) == 0)	
						{
							s_Me909Status = MeConnected;
							SetLogErrCode(LOG_CODE_SUCCESS);
							s_TimeToPowerOffHi3519Flag = FALSE;
						}
						else if(strncmp((char *)tNode->valuestring, "LinkFail", strlen(tNode->valuestring)) == 0)	//连接失败，关闭电源
						{
							u1_printf(" Hi3519 power can be turned off\r\n");
							s_Me909Status = MeConnecting;
							SetLogErrCode(LOG_CODE_POSTFAIL);
							s_TimeToPowerOffHi3519Flag = TRUE;
						}
					}
					
					tNode = cJSON_GetObjectItem(Child, "Photo");
					
					if(tNode != NULL)
					{
//						u1_printf("%s\r\n", tNode->valuestring);
						if(strncmp((char *)tNode->valuestring, "OK", strlen(tNode->valuestring)) == 0)	//相片拍照完成，可能还未上传完成
						{		
							SetLogErrCode(LOG_CODE_PHOTOFAIL);
							u1_printf(" Taked a photo!\r\n");
						}
						else if(strncmp((char *)tNode->valuestring, "Finish", strlen(tNode->valuestring)) == 0)	//相片拍照完成且上传完成，可关闭电源
						{		
							u1_printf(" Upload photos completed!\r\n");
							s_TimeToPowerOffHi3519Flag = TRUE;
						}
						else if(strncmp((char *)tNode->valuestring, "Fail", strlen(tNode->valuestring)) == 0)	//拍照失败，关闭电源
						{
							u1_printf(" Taked a photo Fail!\r\n");
							SetLogErrCode(LOG_CODE_PHOTOFAIL);
							s_TimeToPowerOffHi3519Flag = TRUE;
						}
					}
					
					tNode = cJSON_GetObjectItem(Child, "ServerPicPath");
					
					if(tNode != NULL)
					{
						u1_printf("\r\n ServerPicPath: https://yxb.tcloudit.cn/UpFiles/WebUpload/%s\r\n\r\n", tNode->valuestring);
					}
				}
				
			break;
			
			case CMD_CONTROL:				//核心板给STM32进行一些控制动作
				u1_printf(" Hi3519 sends control instructions to STM32\r\n");
				
				Child = cJSON_GetObjectItem(Root, "Msg");	
				if((Child != NULL) && (Child->type == cJSON_Object))
				{	
					tNode = cJSON_GetObjectItem(Child, "Mode");
					
					if(tNode != NULL)
					{
						if(strncmp((char *)tNode->valuestring, "Normal", strlen(tNode->valuestring)) == 0)	//正常模式  会进入休眠
						{		
							u1_printf(" Normal Mode\r\n");
							App_Run();
						}
						else if(strncmp((char *)tNode->valuestring, "Test", strlen(tNode->valuestring)) == 0)	//调试模式  不进入休眠
						{		
							u1_printf(" Test Mode\r\n");
							TestModeApp_Run();
						}
					}
					
					tNode = cJSON_GetObjectItem(Child, "Run");
					if(tNode != NULL)
					{
						if(strncmp((char *)tNode->valuestring, "Up", strlen(tNode->valuestring)) == 0)	//正常模式  会进入休眠
						{		
							u1_printf(" Normal Mode\r\n");
							ConsoleStop();	
							delay_ms(50);					
							ConsoleUp();
						}
						else if(strncmp((char *)tNode->valuestring, "Down", strlen(tNode->valuestring)) == 0)	//调试模式  不进入休眠
						{		
							u1_printf(" Test Mode\r\n");
							ConsoleStop();	
							delay_ms(50);					
							ConsoleDown();
						}
						else if(strncmp((char *)tNode->valuestring, "Left", strlen(tNode->valuestring)) == 0)	//正常模式  会进入休眠
						{		
							u1_printf(" Normal Mode\r\n");
							ConsoleStop();	
							delay_ms(50);					
							ConsoleLeft();
						}
						else if(strncmp((char *)tNode->valuestring, "Right", strlen(tNode->valuestring)) == 0)	//调试模式  不进入休眠
						{		
							u1_printf(" Test Mode\r\n");
							ConsoleStop();	
							delay_ms(50);					
							ConsoleRight();
						}
						else if(strncmp((char *)tNode->valuestring, "Stop", strlen(tNode->valuestring)) == 0)	//调试模式  不进入休眠
						{		
							u1_printf(" Test Mode\r\n");
							ConsoleStop();	
						}
					}
										
					tNode = cJSON_GetObjectItem(Child, "Speed");
					if(tNode != NULL)
					{
						u1_printf(" Speed:(%d)\r\n", tNode->valueint);
						if(tNode->valueint == 1)
						{
							ConsoleStop();	
							delay_ms(50);					
							GotoPresetPoint(SPEED_SLOW);
						}
						else if(tNode->valueint == 2)
						{
							ConsoleStop();	
							delay_ms(50);					
							GotoPresetPoint(SPEED_MID);
						}
						else if(tNode->valueint == 3)
						{
							ConsoleStop();	
							delay_ms(50);					
							GotoPresetPoint(SPEED_FAST);
						}
					}
					
					tNode = cJSON_GetObjectItem(Child, "GotoPreset");
					if(tNode != NULL)
					{
						if(tNode->valueint < 9)
						{
							u1_printf(" GotoPreset:(%d)\r\n", tNode->valueint);
							ConsoleStop();	
							delay_ms(50);					
							GotoPresetPoint(tNode->valueint);
						}
					}
					
					tNode = cJSON_GetObjectItem(Child, "SetPreset");
					if(tNode != NULL)
					{
						u1_printf(" SetPreset:(%d)\r\n", tNode->valueint);
						if(tNode->valueint < 9)
						{
							u1_printf(" GotoPreset:(%d)\r\n", tNode->valueint);
							ConsoleStop();	
							delay_ms(50);					
							SetPresetPoint(tNode->valueint);
						}
					}
					
					tNode = cJSON_GetObjectItem(Child, "DelPreset");
					if(tNode != NULL)
					{
						u1_printf(" DelPreset:(%d)\r\n", tNode->valueint);
						if(tNode->valueint < 9)
						{
							u1_printf(" GotoPreset:(%d)\r\n", tNode->valueint);
							ConsoleStop();	
							delay_ms(50);					
							DelPresetPoint(tNode->valueint);
						}
					}
				}	
			break;
			
			case CMD_GET_STM32CONFIG:		//核心板从STM32读取配置参数
				u1_printf(" Hi3519 reads Config to STM32\r\n");
			break;
			
			case CMD_SET_STM32CONFIG:		//核心板给STM32设置系统参数
				u1_printf(" Hi3519 sets Config to STM32\r\n");
				Child = cJSON_GetObjectItem(Root, "Msg");	
				ConfigCount = 0;
				if((Child != NULL) && (Child->type == cJSON_Object))
				{	
					p_sys = GetSystemConfig();	
						
					NewConfig = *p_sys;
					
					tNode = cJSON_GetObjectItem(Child, "Device");
					if(tNode != NULL)
					{
						ConfigCount++;
						NewConfig.Device_ID = tNode->valueint;
						u1_printf("Device:%d\r\n", tNode->valueint);
					}
					
					tNode = cJSON_GetObjectItem(Child, "IP");
					if(tNode != NULL)
					{
						ConfigCount++;
						if(strlen(tNode->valuestring) >= 20)
						{
							u1_printf("IP Err:%d", strlen(tNode->valuestring));
							break;
						}
						Err = GetIPAddress(tNode->valuestring, ArrayIP);
						if(Err)
						{
							break;
						}
						u1_printf("IP:");
						for(i=0; i<4; i++)
						{
							NewConfig.Gprs_ServerIP[i] = ArrayIP[i];
							u1_printf("%d ", ArrayIP[i]);
						}
						u1_printf("\r\n");
					}
					
					tNode = cJSON_GetObjectItem(Child, "Port");
					if(tNode != NULL)
					{
						ConfigCount++;
						NewConfig.Gprs_Port = tNode->valueint;
						u1_printf("Port:%d\r\n", tNode->valueint);
					}
					
					tNode = cJSON_GetObjectItem(Child, "HttpServer");
					if(tNode != NULL)
					{
						ConfigCount++;
						HttpLength = strlen(tNode->valuestring);
						if(HttpLength >= 120)
						{
							u1_printf(" Http Length Over:%d", HttpLength);
							break;
						}
						memcpy(strServer, tNode->valuestring, HttpLength);
						u1_printf("Server:%s\r\n", strServer);
					}
					
					tNode = cJSON_GetObjectItem(Child, "PhotoInterval");
					if(tNode != NULL)
					{
						ConfigCount++;
						NewConfig.Heart_interval = tNode->valueint;
						u1_printf("PhotoInterval:%d\r\n", tNode->valueint);
					}
					
					tNode = cJSON_GetObjectItem(Child, "DataInterval");
					if(tNode != NULL)
					{
						ConfigCount++;
						NewConfig.Data_interval = tNode->valueint;
						u1_printf("DataInterval:%d\r\n", tNode->valueint);
					}
					
					if(ConfigCount == 6)	//完整收到所有数据
					{
						p_sys = GetSystemConfig();
						if(memcmp(p_sys, &NewConfig, sizeof(SYSTEMCONFIG)))
						{
							u1_printf(" Start updating parameters...\r\n");
							SetHttpServer(strServer, HttpLength);
							Set_System_Config(&NewConfig);
							u1_printf(" Update parameters completed...\r\n");
						}
						else
						{
							u1_printf(" The configuration does not need to be updated\r\n");
						}
					}
				}	
			break;
			
			case CMD_POWEROFF_HI3519:		//STM32给核心板关机
				if(Dir == 0)	//核心板主动要求关闭自己
				{
					u1_printf(" Hi3519 Request shutdown\r\n");
					s_TimeToPowerOffHi3519Flag = TRUE;
				}
				else if(Dir == 1)
				{
					u1_printf(" Received shutdown response\r\n");
				}
			break;
			
			case CMD_EXITPROCESS_HI3519:
				u1_printf(" Hi3519 exits thread reply received\r\n");
			break;
		}

		Output = cJSON_PrintUnformatted(Root);
		cJSON_Delete(Root);
		u1_printf("%s\r\n",Output);
		myfree(Output);
		u1_printf("\r\n");
	}	
	
	return 0;
}
//监测收到的协议包
static void TaskForHi3519Communication(void)
{	
	if(g_Uart2RxFlag == TRUE)	//收到一帧数据包
	{
		g_Uart2RxFlag = FALSE;
				
		if(g_USART2_RX_CNT > 1)
		{			
			if(g_USART2_RX_BUF[0] == '{')
			{
				DecodecJsonMsg();	
			}
			else
			{
				u1_printf("\r\n Rec Packs(%d):", strlen((const char *)g_USART2_RX_BUF));
				u1_printf("%s\r\n", g_USART2_RX_BUF);
			}		
		}	

	
		Clear_Uart2Buff();		
	}
}
//用于是否发送JSON应答包
static void TaskForSTM32Ack(void)	//发送对应消息包应答
{
	u8 i;
	u8 Num = ACK_NUM;
	
	for(i=ACK_NUM; i<ACK_NUM_MAX; i++)
	{
		if(cJsonMsgInfo[i].isWorking == TRUE)
		{
			SendcJsonACK(&cJsonMsgInfo[Num]);
			cJsonMsgInfo[i].isWorking = FALSE;
		}
	}
}
//初始化CJSON配置
void InitcJsonConfig(void)
{
	u8 i;
	
	mem_init();
	
	for(i=0; i<ACK_NUM_MAX; i++)
	{
		cJsonMsgInfo[i].CMD = 0;
		cJsonMsgInfo[i].Dir = 0;
		cJsonMsgInfo[i].Seq = 0;
		cJsonMsgInfo[i].Err = 0;
		cJsonMsgInfo[i].isWorking = FALSE;
	}
	
	USART2_Config(115200);		//串口2，用于Hi3519通信
	
	Task_Create(TaskForHi3519Communication, 1);
	
	Task_Create(TaskForSTM32Ack, 1);
}
//STM32主动发起检测核心板通信是否正常	0x00
void STM32ToHi3519DetCommunicationMsg(unsigned int Seq)
{
	char *cJsonBuf, strTime[30];
	cJSON *Root;
	u16 MsgLen = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStructure;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
	sprintf(strTime, "%d-%02d-%02d %02d:%02d:%02d", RTC_DateStructure.RTC_Year + 2000, RTC_DateStructure.RTC_Month, RTC_DateStructure.RTC_Date, RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);

	Root = cJSON_CreateObject();	
	
	cJSON_AddNumberToObject(Root,"CMD", 		CMD_DET_COMMUNICATION);
	cJSON_AddNumberToObject(Root,"Dir",			CMD_MASTER_SEND);
	cJSON_AddNumberToObject(Root,"Seq",			Seq);
	cJSON_AddStringToObject(Root,"DateTime",	strTime);
	
	cJsonBuf = cJSON_PrintUnformatted(Root);	
	
	MsgLen = strlen(cJsonBuf);
		
	cJSON_Delete(Root);	

	u2_printf("%s\r", cJsonBuf);
	
	u1_printf("<- <- <- STM32 Detect Communication(Size:%d)(CMD:%d)(Seq:%d)%s\r\n", MsgLen, CMD_DET_COMMUNICATION, Seq++, cJsonBuf);
	
	myfree(cJsonBuf);
	
	u1_printf(" Memory utilization:%d%%\r\n\r\n", mem_perused());	
}
//发送一帧主板传感器应答数据包	0x01
void STM32AckHi3519SensorData(unsigned int Seq)
{
	char *cJsonBuf, strTime[30];
	cJSON *Root, *Data;
	u16 MsgLen = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStructure;
	float Latitude = 0, Longitude = 0;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
	sprintf(strTime, "%d-%02d-%02d %02d:%02d:%02d", RTC_DateStructure.RTC_Year + 2000, RTC_DateStructure.RTC_Month, RTC_DateStructure.RTC_Date, RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);

	Root = cJSON_CreateObject();	
	
	cJSON_AddNumberToObject(Root,"CMD", 		CMD_GETSTM32DATA);
	cJSON_AddNumberToObject(Root,"Dir",			CMD_SLAVE_ACK);
	cJSON_AddNumberToObject(Root,"Seq",			Seq);
	cJSON_AddStringToObject(Root,"DateTime",	strTime);
	
	cJSON_AddItemToObject(Root, "Msg", 		Data = cJSON_CreateObject());
		
	cJSON_AddNumberToObject(Data,"BatVol",		Get_Battery_Vol());
	
	cJSON_AddNumberToObject(Data,"Ill",			Get_Ill_Value());
	
	cJSON_AddNumberToObject(Data,"Pos",			GetConsolePosition());

	GetPosition(&Latitude, &Longitude);
	cJSON_AddNumberToObject(Data,"Latitude",	Latitude);

	cJSON_AddNumberToObject(Data,"Longitude",	Longitude);
	
	cJsonBuf = cJSON_PrintUnformatted(Root);	
	
	MsgLen = strlen(cJsonBuf);
		
	cJSON_Delete(Root);	

	u2_printf("%s\r", cJsonBuf);
	
	u1_printf("<- <- <- STM32AckHi3519SensorData(Size:%d)(CMD:%d)(Seq:%d)\r\n", MsgLen, CMD_GETSTM32DATA, Seq++);
	
	myfree(cJsonBuf);
	
	u1_printf(" Memory utilization:%d%%\r\n\r\n", mem_perused());	
}

//获取HI3519核心板状态信息	0x02
void STM32ToHi3519GetStatus(unsigned int Seq)
{
	char *cJsonBuf, strTime[30];
	cJSON *Root;
	u16 MsgLen = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStructure;	
	
	Root = cJSON_CreateObject();	
	
	cJSON_AddNumberToObject(Root,"CMD", 		CMD_GETHI3519STATUS);
	cJSON_AddNumberToObject(Root,"Dir",			CMD_MASTER_SEND);
	cJSON_AddNumberToObject(Root,"Seq",			Seq);

	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
	sprintf(strTime, "%d-%02d-%02d %02d:%02d:%02d", RTC_DateStructure.RTC_Year + 2000, RTC_DateStructure.RTC_Month, RTC_DateStructure.RTC_Date, RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
	cJSON_AddStringToObject(Root,"DateTime",strTime);
	
	cJsonBuf = cJSON_PrintUnformatted(Root);	
	
	MsgLen = strlen(cJsonBuf);
	
	cJSON_Delete(Root);	

	u2_printf("%s\r", cJsonBuf);
	
	u1_printf("<- <- <- STM32ToHi3519GetStatus (Size:%d)(CMD:%d)(Seq:%d)\r\n", MsgLen, CMD_GETHI3519STATUS, Seq++);
	
	myfree(cJsonBuf);
	
	u1_printf(" Memory utilization:%d%%\r\n\r\n", mem_perused());
}
//发送一帧主板传感器应答数据包	0x05
void STM32ToSTM32SetConfig(unsigned int Seq)
{
	char *cJsonBuf, strTime[30], strIP[20];
	cJSON *Root, *Data;
	u16 MsgLen = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStructure;
	char HttpBuf[128];
	u8  Http_Length;
	SYSTEMCONFIG *p_sys;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
	sprintf(strTime, "%d-%02d-%02d %02d:%02d:%02d", RTC_DateStructure.RTC_Year + 2000, RTC_DateStructure.RTC_Month, RTC_DateStructure.RTC_Date, RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);

	Root = cJSON_CreateObject();	
	
	cJSON_AddNumberToObject(Root,"CMD", 		CMD_SET_STM32CONFIG);
	cJSON_AddNumberToObject(Root,"Dir",			CMD_MASTER_SEND);
	cJSON_AddNumberToObject(Root,"Seq",			Seq++);
	cJSON_AddStringToObject(Root,"DateTime",	strTime);
		
	p_sys = GetSystemConfig();	
	
	cJSON_AddItemToObject(Root, "Msg", 		Data = cJSON_CreateObject());

	cJSON_AddNumberToObject(Data,"Device",		p_sys->Device_ID);
	
	sprintf(strIP, "%d.%d.%d.%d", p_sys->Gprs_ServerIP[0], p_sys->Gprs_ServerIP[1], p_sys->Gprs_ServerIP[2], p_sys->Gprs_ServerIP[3]);
	cJSON_AddStringToObject(Data,"IP",			strIP);
	
	cJSON_AddNumberToObject(Data,"Port",			p_sys->Gprs_Port);

	Http_Length = Check_Area_Valid(HTTP_SERVER_ADDR);
	if (Http_Length >= 4 && Http_Length <= 120)
	{
		EEPROM_ReadBytes(HTTP_SERVER_ADDR, (u8 *)HttpBuf, sizeof(SYS_TAG) +  Http_Length);
		HttpBuf[sizeof(SYS_TAG) +  Http_Length] = 0;
	}	
	else if(Http_Length > 120)
	{
		memset(HttpBuf,0, sizeof(HttpBuf));
		u1_printf(" HttpServer Over\r\n");
	}
	cJSON_AddStringToObject(Data, "HttpServer",			&HttpBuf[sizeof(SYS_TAG)]);
	
	cJSON_AddNumberToObject(Data, "PhotoInterVal",			p_sys->Heart_interval);
	
	cJSON_AddNumberToObject(Data, "DataInterVal",			p_sys->Data_interval);
	
	cJsonBuf = cJSON_PrintUnformatted(Root);	
	
	MsgLen = strlen(cJsonBuf);
	
	cJSON_Delete(Root);	

	u2_printf("%s\r", cJsonBuf);
	
	u1_printf("<- <- <- STM32 SetConfig(Size:%d)(CMD:%d)(Seq:%d)\r\n", MsgLen, CMD_SET_STM32CONFIG, Seq);
	
	myfree(cJsonBuf);
	
	u1_printf(" Memory utilization:%d%%\r\n\r\n", mem_perused());
}
//STM32向核心板发送开始拍照指令		0x06
//void STM32ToHi3519PowerOff(unsigned int Seq)
//{
//	char *cJsonBuf, strTime[30];
//	cJSON *Root;

//	u16 MsgLen = 0;
//	RTC_TimeTypeDef RTC_TimeStructure;
//	RTC_DateTypeDef RTC_DateStructure;	
//	
//	Root = cJSON_CreateObject();	
//	
//	cJSON_AddNumberToObject(Root,"CMD", 		CMD_TAKE_PHOTO);
//	cJSON_AddNumberToObject(Root,"Dir",			CMD_MASTER_SEND);
//	cJSON_AddNumberToObject(Root,"Seq",			Seq);

//	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
//	RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
//	sprintf(strTime, "%d-%02d-%02d %02d:%02d:%02d", RTC_DateStructure.RTC_Year + 2000, RTC_DateStructure.RTC_Month, RTC_DateStructure.RTC_Date, RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
//	cJSON_AddStringToObject(Root,"DateTime",strTime);
//	
//	
//	cJsonBuf = cJSON_PrintUnformatted(Root);	
//	
//	MsgLen = strlen(cJsonBuf);
//		
//	cJSON_Delete(Root);	

//	u2_printf("%s\r", cJsonBuf);
//	
//	u1_printf("<- <- <- STM32ToHi3519PowerOff (Size:%d)(CMD:%d)(Seq:%d)\r\n", MsgLen, CMD_POWEROFF_HI3519, Seq);
//	
//	myfree(cJsonBuf);
//	
//	u1_printf(" Memory utilization:%d%%\r\n\r\n", mem_perused());
//}

//STM32向核心板发送关机指令		0x08
void STM32ToHi3519PowerOff(unsigned int Seq)
{
	char *cJsonBuf, strTime[30];
	cJSON *Root;

	u16 MsgLen = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStructure;	
	
	Root = cJSON_CreateObject();	
	
	cJSON_AddNumberToObject(Root,"CMD", 		CMD_POWEROFF_HI3519);
	cJSON_AddNumberToObject(Root,"Dir",			CMD_MASTER_SEND);
	cJSON_AddNumberToObject(Root,"Seq",			Seq);

	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
	sprintf(strTime, "%d-%02d-%02d %02d:%02d:%02d", RTC_DateStructure.RTC_Year + 2000, RTC_DateStructure.RTC_Month, RTC_DateStructure.RTC_Date, RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
	cJSON_AddStringToObject(Root,"DateTime",strTime);
	
	cJsonBuf = cJSON_PrintUnformatted(Root);	
	
	MsgLen = strlen(cJsonBuf);
		
	cJSON_Delete(Root);	

	u2_printf("%s\r", cJsonBuf);
	
	u1_printf("<- <- <- STM32ToHi3519PowerOff (Size:%d)(CMD:%d)(Seq:%d)\r\n", MsgLen, CMD_POWEROFF_HI3519, Seq);
	
	myfree(cJsonBuf);
	
	u1_printf(" Memory utilization:%d%%\r\n\r\n", mem_perused());
}

//STM32向核心板发送关闭进程指令		0x09
void STM32ToHi3519ExitProcess(unsigned int Seq)
{
	char *cJsonBuf, strTime[30];
	cJSON *Root;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStructure;	
	u16 MsgLen = 0;
	
	Root = cJSON_CreateObject();	
	
	cJSON_AddNumberToObject(Root,"CMD", 		CMD_EXITPROCESS_HI3519);
	cJSON_AddNumberToObject(Root,"Dir",			CMD_MASTER_SEND);
	cJSON_AddNumberToObject(Root,"Seq",			Seq);

	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
	sprintf(strTime, "%d-%02d-%02d %02d:%02d:%02d", RTC_DateStructure.RTC_Year + 2000, RTC_DateStructure.RTC_Month, RTC_DateStructure.RTC_Date, RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
	cJSON_AddStringToObject(Root,"DateTime",strTime);
	
	cJsonBuf = cJSON_PrintUnformatted(Root);	
	
	MsgLen = strlen(cJsonBuf);

	cJSON_Delete(Root);	

	u2_printf("%s\r", cJsonBuf);
	
	u1_printf("<- <- <- STM32ToHi3519ExitProcess(Size:%d)(CMD:%d)(Seq:%d)\r\n", MsgLen, CMD_EXITPROCESS_HI3519, Seq);
	
	myfree(cJsonBuf);
	
	u1_printf(" Memory utilization:%d%%\r\n\r\n", mem_perused());
}

//STM32内部测试指令
void STM32ToSTM32TestCmd(unsigned int Seq, unsigned char CMD)
{
	char *cJsonBuf, strTime[30];
	cJSON *Root;
	u16 MsgLen = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStructure;	
	
	Root = cJSON_CreateObject();	
	
	cJSON_AddNumberToObject(Root,"CMD", 		CMD);
	cJSON_AddNumberToObject(Root,"Dir",			CMD_MASTER_SEND);
	cJSON_AddNumberToObject(Root,"Seq",			Seq);

	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
	sprintf(strTime, "%d-%02d-%02d %02d:%02d:%02d", RTC_DateStructure.RTC_Year + 2000, RTC_DateStructure.RTC_Month, RTC_DateStructure.RTC_Date, RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
	cJSON_AddStringToObject(Root,"DateTime",strTime);
	
	cJsonBuf = cJSON_PrintUnformatted(Root);	
	
	MsgLen = strlen(cJsonBuf);
	
	cJSON_Delete(Root);	

	u2_printf("%s\r", cJsonBuf);
	
	u1_printf("<- <- <- STM32ToSTM32 Test Cmd(Size:%d)(CMD:%d)(Seq:%d)\r\n", MsgLen, CMD, Seq);
	
	myfree(cJsonBuf);
	
	u1_printf(" Memory utilization:%d%%\r\n\r\n", mem_perused());
}
//重发STM32检测核心板通信任务
static void TaskforRetryDetComMsg(void)
{
	s_STM32DetCommunicationCount++;
	
	if(s_STM32DetCommunicationCount >= 15)
	{
		s_STM32DetCommunicationCount = 0;
		Task_Kill(TaskforRetryDetComMsg);
		return;
	}
	
	STM32ToHi3519DetCommunicationMsg(cJsonMsgInfo[DET_COM_NUM].Seq);
}
//重发STM32获取核心板状态任务
static void TaskforRetryGetHi3519Status(void)
{
	s_STM32GetStatusRetryCount++;
	
	if(s_STM32GetStatusRetryCount >= MAX_MSG_RETRY_COUNT)
	{
		s_STM32GetStatusRetryCount = 0;
		Task_Kill(TaskforRetryGetHi3519Status);
		return;
	}
	STM32ToHi3519GetStatus(cJsonMsgInfo[GET_STATUS_NUM].Seq);	
}
//重发STM32关闭核心板通信
static void TaskforRetryPowerOff(void)
{
	s_STM32PowerOffCount++;
	
	if(s_STM32PowerOffCount >= MAX_MSG_RETRY_COUNT)
	{
		s_STM32PowerOffCount = 0;
		Task_Kill(TaskforRetryPowerOff);
		return;
	}
	STM32ToHi3519PowerOff(cJsonMsgInfo[POWER_OFF_NUM].Seq);
}
//仲裁当前接收报文与发送报文响应的匹配
static void OnRxMatchcJsonMsg(JSON_COMM_MSG *cJsonMsg)
{
	if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[DET_COM_NUM].Seq) && cJsonMsg->CMD == CMD_DET_COMMUNICATION )	//是应答包且包序号匹配
	{
		u1_printf(" Rec Ack, Kill the DetCOM\r\n");
		Task_Kill(TaskforRetryDetComMsg);
	}
	else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[GET_STATUS_NUM].Seq) && cJsonMsg->CMD == CMD_GETHI3519STATUS)	//是应答包且包序号匹配
	{
		u1_printf(" Rec Ack, Kill the Get Status CMD\r\n");
		Task_Kill(TaskforRetryGetHi3519Status);
	}
	else if((cJsonMsg->Dir == CMD_SLAVE_ACK) && (cJsonMsg->Seq == cJsonMsgInfo[POWER_OFF_NUM].Seq) && cJsonMsg->CMD == CMD_POWEROFF_HI3519)	//是应答包且包序号匹配
	{
		u1_printf(" Rec Ack, Kill the Power Off CMD\r\n");
		Task_Kill(TaskforRetryPowerOff);
	}

}

void TaskForHi3519CommunicationEnd(void)
{
	Task_Kill(TaskforRetryDetComMsg);
	Task_Kill(TaskforRetryGetHi3519Status);
	Task_Kill(TaskforRetryPowerOff);
}
//使用带重发的方式测试与核心板之间的通信
void SendSTM32DetCommunicationbyRetryMode(void)
{
	static unsigned int Seq = 0;
	
	STM32ToHi3519DetCommunicationMsg(Seq);
	
	cJsonMsgInfo[DET_COM_NUM].CMD = CMD_DET_COMMUNICATION;
	cJsonMsgInfo[DET_COM_NUM].Dir = CMD_MASTER_SEND;
	cJsonMsgInfo[DET_COM_NUM].Seq = Seq++;
	
	Task_Create(TaskforRetryDetComMsg, 50000);	//20000个任务周期重发一次
	
	s_STM32DetCommunicationCount = 0;
}
//使用带重发的方式获取核心板状态
void SendSTM32GetStatusCmdbyRetryMode(void)
{
	static unsigned int Seq = 0;
	
	STM32ToHi3519GetStatus(Seq);
	
	cJsonMsgInfo[GET_STATUS_NUM].CMD = CMD_GETHI3519STATUS;
	cJsonMsgInfo[GET_STATUS_NUM].Dir = CMD_MASTER_SEND;
	cJsonMsgInfo[GET_STATUS_NUM].Seq = Seq++;
	
	Task_Create(TaskforRetryGetHi3519Status, 10000);	//20000个任务周期重发一次
	
	s_STM32GetStatusRetryCount = 0;
}
//使用带重发的方式关闭核心板通信
void SendSTM32PowerOffbyRetryMode(void)
{
	static unsigned int Seq = 0;
	
	STM32ToHi3519PowerOff(Seq);
	
	cJsonMsgInfo[POWER_OFF_NUM].CMD = CMD_POWEROFF_HI3519;
	cJsonMsgInfo[POWER_OFF_NUM].Dir = CMD_MASTER_SEND;
	cJsonMsgInfo[POWER_OFF_NUM].Seq = Seq++;
	
	Task_Create(TaskforRetryPowerOff, 5000);	//20000个任务周期重发一次
	
	s_STM32PowerOffCount = 0;
}

















