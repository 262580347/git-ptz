#ifndef __POWERMANAGE_H_
#define __POWERMANAGE_H_	

typedef enum
{
	BAT_WORKING 	= 0,//���ڼ���ص�ѹ
	BAT_ERROR  		= 1,//��ѹ״̬����
	BAT_IDLE 	 	= 2,//��ѹ������
	BAT_OTHER    	= 3,//����״̬
}BAT_STATUS;

extern BAT_STATUS BatStatus;

typedef __packed struct
{
	unsigned char 			Magic; 	//0x55
	unsigned short 			Length;	//�洢���ݳ���	
	unsigned char 			Chksum;	//У���
	unsigned char			Enable;
	unsigned char			ForceFlag;
}CHARGING_CFG;

extern unsigned char g_BQ24650_EN_Flag;

unsigned short Get_Battery_Vol(void);

void StartGetBatVol(void);

void ReadBQ24650Flag(void);

void SetBQ24650ENFlag(unsigned char isTrue);
	
unsigned char GetBQ24650ENFlag(void);

unsigned char GetForcedChargeFlag(void);

void SetForcedChargeFlag(unsigned char isTrue);

CHARGING_CFG* GetChargingStatus(void);

void BatVolDet_Init(void);

#endif 
