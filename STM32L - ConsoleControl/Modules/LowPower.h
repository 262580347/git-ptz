#ifndef  _LOW_POWER_H_
#define  _LOW_POWER_H_

#define		MEASUREMENT_INTERVAL  1 

#define		STOP_TIME		10

#define		ERR_STOP_TIME	2

void EnterStopMode(unsigned char Second);

void StartSensorMeasure(void);

void LowPowerMode_Init(void);

void SetStopModeTime(unsigned char isTrue, unsigned char StopTime);

#endif
