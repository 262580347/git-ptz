#include "Protocol.h"
#include "main.h"


//读取设备通信配置信息 --0x04 =========================================
unsigned int System_get_config(USART_TypeDef* USARTx, CLOUD_HDR *pMsg, unsigned char *data, unsigned int len)
{
	unsigned char send_buf[256], Packet[256];
	unsigned int send_len;
	u16 nMsgLen, i;
	CLOUD_HDR *hdr;
	SYSTEMCONFIG *sys_cfg;
	COM_SYSTEM_CFG  *com_sys_cfg;
	
	
	memcpy(send_buf, pMsg, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	
	hdr = (CLOUD_HDR *)send_buf;
	hdr->protocol = swap_word(hdr->protocol);	
	hdr->device_id = swap_dword(hdr->device_id);
	hdr->seq_no = swap_word(hdr->seq_no);	
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+1);	
	sys_cfg = GetSystemConfig();
	com_sys_cfg = (COM_SYSTEM_CFG *)&send_buf[sizeof(CLOUD_HDR)];
	
	com_sys_cfg->cfg_id1 = CFG_ID_1;

	com_sys_cfg->device_id = swap_dword(sys_cfg->Device_ID);	//非低功耗设备

	
	com_sys_cfg->net_type = sys_cfg->Type;
	
	com_sys_cfg->cfg_id2 = CFG_ID_2;
	com_sys_cfg->zigbee_net = swap_word(sys_cfg->Net);
	com_sys_cfg->zigbee_node = swap_word(sys_cfg->Node);
	com_sys_cfg->zigbee_gateway = swap_word(sys_cfg->Gateway);
	com_sys_cfg->zigbee_channel = sys_cfg->Channel;
	com_sys_cfg->zigbee_LowpowerFlag = sys_cfg->LowpowerFlag;
	
	com_sys_cfg->cfg_id3 = CFG_ID_3;
	memcpy(com_sys_cfg->gprs_server, sys_cfg->Gprs_ServerIP, 4);
	com_sys_cfg->gprs_port = swap_word(sys_cfg->Gprs_Port);
	
	com_sys_cfg->cfg_id4 = CFG_ID_4;
	com_sys_cfg->retry_times = sys_cfg->Retry_Times;		
	com_sys_cfg->h_interval = swap_word(sys_cfg->Heart_interval);		
	com_sys_cfg->d_interval = swap_word(sys_cfg->Data_interval);		
	com_sys_cfg->s_interval = swap_word(sys_cfg->State_interval);		
	
	send_len+=sizeof(SYSTEMCONFIG)+1;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,200);
	//发送消息

	USART2_DMA_Send(Packet, nMsgLen);	

	u1_printf("[Mod][Config] -> %d (%d):", (sys_cfg->Device_ID)&0xfffffff, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ", Packet[i]);
	}	
	u1_printf("\r\n");	
	return 1;
}

//写入通道配置信息 0x05 ===============================================
unsigned int System_set_config(USART_TypeDef* USARTx, unsigned char *data, unsigned int len)
{
	unsigned char send_buf[256], Packet[256];
	unsigned int send_len;
	u16 nMsgLen, i;
	
	CLOUD_HDR *hdr;
	unsigned char *ptr;
	
	SYSTEMCONFIG new_sys_cfg;
	SYSTEMCONFIG *sys_cfg, *p_sys;
	COM_SYSTEM_CFG *com_sys_cfg;
	
	uint16 val_u16;
	unsigned int val_u32;
	unsigned int payload_len;

	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	
	ptr = &data[sizeof(CLOUD_HDR)];
	hdr = (CLOUD_HDR *)data;
	
	payload_len = swap_word(hdr->payload_len);
	
	sys_cfg = GetSystemConfig();
	memcpy(&new_sys_cfg, sys_cfg, sizeof(SYSTEMCONFIG) - 3);
	
	while(payload_len)
	{
		switch((*ptr))
		{
			case CFG_ID_1:	  	//系统信息分组ID
				ptr++;
				payload_len--;
				val_u32 = *(unsigned int *)ptr;
				new_sys_cfg.Device_ID = (swap_dword(val_u32)) & 0xfffffff ;
				ptr += sizeof(unsigned int);
				new_sys_cfg.Type = (*ptr);
				ptr++;
				payload_len-=5;

				u1_printf("[Set]:Device=%u,Net type=%d\n",new_sys_cfg.Device_ID,new_sys_cfg.Type );
				
				if((new_sys_cfg.Type != NETTYPE_LORA) && (new_sys_cfg.Type != NETTYPE_SIM800C) && (new_sys_cfg.Type != NETTYPE_GPRS) )
				{
					return 0;
				}
				
				break;
			case CFG_ID_2: 	//Zigbee信息分组ID
				ptr++;
				payload_len--;
			
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Net = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Node = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Gateway = swap_word(val_u16);
				ptr += sizeof(uint16);
				new_sys_cfg.Channel = (*ptr);
				ptr++;
				new_sys_cfg.LowpowerFlag = (*ptr);
				ptr++;
				payload_len-=8;

				u1_printf("[Set]:Lora Net num=%u,Node=%d,Gateway=%d,Channel=%d,Go to Sleep=%d\r\n"
						,new_sys_cfg.Net
						,new_sys_cfg.Node
						,new_sys_cfg.Gateway
						,new_sys_cfg.Channel
						,new_sys_cfg.LowpowerFlag
						 );
				if(new_sys_cfg.Channel > 32)
				{
					u1_printf("\r\n Channel Err\r\n");
					return 0;
				}
				if(new_sys_cfg.Gateway != new_sys_cfg.Net)
				{
					u1_printf("\r\n Net Err\r\n");
					return 0;
				}
				
				break;
			case CFG_ID_3:		//GPRS分组信息ID
				ptr++;
				payload_len--;
			
				memcpy(new_sys_cfg.Gprs_ServerIP, ptr, 4);
				ptr += 4;
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Gprs_Port = swap_word(val_u16);
				ptr += sizeof(uint16);
				payload_len-=6;

				u1_printf("%u.%u.%u.%u,Port=%d\n"
						,(new_sys_cfg.Gprs_ServerIP[0])
						,(new_sys_cfg.Gprs_ServerIP[1])
						,(new_sys_cfg.Gprs_ServerIP[2])
						,(new_sys_cfg.Gprs_ServerIP[3])
						,new_sys_cfg.Gprs_Port
						 );
				break;
			case CFG_ID_4: 	//通信参数分组ID
				ptr++;
				payload_len--;
			
				new_sys_cfg.Retry_Times = (*ptr);
				ptr++;
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Heart_interval = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Data_interval = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.State_interval = swap_word(val_u16);
				ptr += sizeof(uint16);
				payload_len-=7;

				u1_printf("Count=%u.Heart=%u.Data=%u.Wait=%u\n"
						,new_sys_cfg.Retry_Times
						,new_sys_cfg.Heart_interval
						,new_sys_cfg.Data_interval
						,new_sys_cfg.State_interval
						 );
						 
				if(new_sys_cfg.Heart_interval > 30000)
				{
					return 0;
				}
				
				if(new_sys_cfg.Data_interval > 30000)
				{
					return 0;
				}
				
				if(new_sys_cfg.State_interval > 30000)
				{
					return 0;
				}
				
				break;
			default:
				ptr++;
				payload_len--;
				break;
		}
	}
	
	if (memcmp(&new_sys_cfg, sys_cfg, sizeof(SYSTEMCONFIG) - 3))
	{
		u1_printf("\r\n Updata Config...");
		new_sys_cfg.AutoResetFlag = sys_cfg->AutoResetFlag;
		new_sys_cfg.AutoResetTime = sys_cfg->AutoResetTime;
		Set_System_Config(&new_sys_cfg);
	}
	
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+1);
	send_len = sizeof(CLOUD_HDR);
	
	sys_cfg = GetSystemConfig();
	com_sys_cfg = (COM_SYSTEM_CFG *)&send_buf[sizeof(CLOUD_HDR)];
	
	com_sys_cfg->cfg_id1 = CFG_ID_1;
	
	p_sys = GetSystemConfig();			
	if(p_sys->LowpowerFlag == 0)
	{
		com_sys_cfg->device_id = swap_dword(sys_cfg->Device_ID | 0x10000000);	//低功耗设备
	}
	else if(p_sys->LowpowerFlag == 1)
	{
		com_sys_cfg->device_id = swap_dword(sys_cfg->Device_ID);	//非低功耗设备
	}

	com_sys_cfg->net_type = sys_cfg->Type;
	
	com_sys_cfg->cfg_id2 = CFG_ID_2;
	com_sys_cfg->zigbee_net = swap_word(sys_cfg->Net);
	com_sys_cfg->zigbee_node = swap_word(sys_cfg->Node);
	com_sys_cfg->zigbee_gateway = swap_word(sys_cfg->Gateway);
	com_sys_cfg->zigbee_channel = sys_cfg->Channel;
	com_sys_cfg->zigbee_LowpowerFlag = sys_cfg->LowpowerFlag;
	
	com_sys_cfg->cfg_id3 = CFG_ID_3;
	memcpy(com_sys_cfg->gprs_server, sys_cfg->Gprs_ServerIP, 4);
	com_sys_cfg->gprs_port = swap_word(sys_cfg->Gprs_Port);
	
	com_sys_cfg->cfg_id4 = CFG_ID_4;
	com_sys_cfg->retry_times = sys_cfg->Retry_Times;		
	com_sys_cfg->h_interval = swap_word(sys_cfg->Heart_interval);		
	com_sys_cfg->d_interval = swap_word(sys_cfg->Data_interval);		
	com_sys_cfg->s_interval = swap_word(sys_cfg->State_interval);	
	
	send_len+=sizeof(SYSTEMCONFIG)+1;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,200);
	//发送消息
	u1_printf("[Mod][Updata Config] -> %d (%d):",  (sys_cfg->Device_ID)&0xfffffff, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ", send_buf[i]);
	}	
	u1_printf("\r\n");
	

	USART2_DMA_Send(Packet, nMsgLen);	

	
	return 1;
}
