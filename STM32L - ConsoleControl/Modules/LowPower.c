/**********************************
说明:低功耗模式下的进入与退出管理
	  主要重新对时钟、外设、端口进行配置
	  清除对应标志位
	  
作者:关宇晟
版本:V2017.4.3
***********************************/
#include "main.h"
#include "LowPower.h"
 //RTC唤醒标志
static unsigned char s_RTCAlarmFlag = FALSE, s_Exti2Flag = FALSE;
//进入休眠标志，休眠时间
static unsigned char s_EnterStopFlag = FALSE, s_StopTime = 0;

unsigned char GetStopModeFlag(void)
{
	return s_EnterStopFlag;
}

void SetStopModeTime(unsigned char isTrue, unsigned char StopTime)
{
	s_EnterStopFlag = isTrue;
	s_StopTime = StopTime;
}
//启动传感器测量
void StartSensorMeasure(void)
{
	StartGetBatVol();

//	StartSHTSensor();
//	
//	StartBMPSensor();
	
	StartMAXSensor();
}

static void TaskForLowPowerControl(void)
{
	if(s_EnterStopFlag)
	{
		if((Sht31Status != SHT_WORKING) && (MaxStatus != MAX_WORKING) && (BmpStatus != BMP_WORKING) &&(BatStatus != BAT_WORKING))
		s_EnterStopFlag = FALSE;
		
		EnterStopMode(s_StopTime);
	}
}

static void TaskForWakeup(void)
{
//	static unsigned int n_OverTime = 0;
	RTC_TimeTypeDef RTC_TimeStructure;	

	if(s_RTCAlarmFlag)			//闹钟唤醒
	{
		s_RTCAlarmFlag = FALSE;
		
		Wake_SysInit();
		
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);		
		u1_printf("\r\n %02d:%02d:%02d\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
//		
//		EXTI5_Reset();
		
		EXTI17_Reset();
		
//		if(DifferenceOfRTCTime(GetRTCSecond(), n_OverTime) >= 60)
//		{
//			n_OverTime = GetRTCSecond();
////			
////			RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);		
////			
////			u1_printf("\r\n %02d:%02d:%02d\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
//			
//			StartSensorMeasure();
//		}
	}
	
	if(s_Exti2Flag)				//外部唤醒
	{			
		s_Exti2Flag = FALSE;
		
		Wake_SysInit();
		
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);		
		u1_printf("\r\n [%02d:%02d:%02d]  Aux\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);

		EXTI5_Reset();
		
		EXTI17_Reset();
	}	
}

void LowPowerMode_Init(void)
{
	Task_Create(TaskForLowPowerControl, 1);
	
	Task_Create(TaskForWakeup, 1);
}
//进入停止模式前的配置，GPIO、中断、外设，使用快速唤醒模式

void EnterStopMode(unsigned char Second)	
{  	
	if(Second == 0)
	{
		return;
	}	
	while (DMA_GetCurrDataCounter(DMA1_Channel4));
	
	__disable_irq();
	
	IWDG_ReloadCounter();
	
	while (DMA_GetCurrDataCounter(DMA1_Channel4));	
	USART_Cmd(USART2,DISABLE); 
	USART_DeInit(USART2);	
	
	while (DMA_GetCurrDataCounter(DMA1_Channel2));
	USART_Cmd(USART3,DISABLE); 
	USART_DeInit(USART3);	
			
	ADC_DeInit(ADC1); 
				
	RTC_SetAlarmA(Second);
	
	while (DMA_GetCurrDataCounter(DMA1_Channel4));	
	
	USART_Cmd(USART1, DISABLE);	
	USART_DeInit(USART1);	
	
	if(DMA_GetFlagStatus(DMA1_FLAG_TC4) == SET)
		DMA_ClearFlag(DMA1_FLAG_TC4);
	DMA_Cmd(DMA1_Channel4, DISABLE ); 
	DMA_DeInit(DMA1_Channel4);			
//	if(DMA_GetFlagStatus(DMA1_FLAG_TC2) == SET)
//		DMA_ClearFlag(DMA1_FLAG_TC2);
//	DMA_Cmd(DMA1_Channel2, DISABLE );  //关闭USART3 TX DMA1 所指示的通道     
//	DMA_DeInit(DMA1_Channel2);

//	if(DMA_GetFlagStatus(DMA1_FLAG_TC7) == SET)
//		DMA_ClearFlag(DMA1_FLAG_TC7);
//	DMA_Cmd(DMA1_Channel7, DISABLE );  //关闭USART2 TX DMA1 所指示的通道     
	
	Stop_GPIO_Init();	
	
	IWDG_ReloadCounter();
	
//	EXTI5_Config();
	/* Disable the SysTick timer */
	SysTick->CTRL &= (~SysTick_CTRL_ENABLE);

	/* Enable Power Interface clock */
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;

	/* configure MCU to go in stop mode after WFI instruction and not in standby */
	PWR->CR &= (~PWR_CR_PDDS);
	
	PWR->CR |= PWR_CR_FWU;
	
	/* configure MCU to go in stop/standby mode after WFI instruction and not in sleep */
	SCB->SCR |= SCB_SCR_SLEEPDEEP;

	/* Configure MCU to go in stop mode with regulator in low power mode */
	PWR->CR |= PWR_CR_LPSDSR;

	/* Disable VREFINT to save current */
	PWR->CR |= PWR_CR_ULP;

	/* Disable PVDE to save current */
	PWR->CR &= (~PWR_CR_PVDE);
	
	__enable_irq();
    /* Wait for interrupt instruction, device go to sleep mode */
	__WFI();
}


void RTC_Alarm_IRQHandler(void)	//闹钟中断
{
	if(RTC_GetFlagStatus(RTC_FLAG_ALRAF) != RESET)
	{		
		if(s_EnterStopFlag == TRUE)
		{
			s_EnterStopFlag = FALSE;
		}
		
		SysClockForMSI(RCC_MSIRange_6);
					
		IWDG_ReloadCounter();
		
		if(PWR_GetFlagStatus(PWR_FLAG_WU) != RESET)
		{
			PWR_ClearFlag(PWR_FLAG_WU);//????
		}
		
		EXTI_ClearITPendingBit(EXTI_Line17);
		
		RTC_ClearITPendingBit(RTC_IT_ALRA);
		
		s_RTCAlarmFlag = TRUE;
	}	
}


void EXTI9_5_IRQHandler(void)	//外部中断
{	
	if(EXTI_GetITStatus(EXTI_Line5) != RESET)
	{			
		SysClockForMSI(RCC_MSIRange_6);
		
		
		if(PWR_GetFlagStatus(PWR_FLAG_WU) != RESET)
		{
			PWR_ClearFlag(PWR_FLAG_WU);//????
		}
		
		s_Exti2Flag = TRUE;
		
		EXTI_ClearITPendingBit(EXTI_Line5);
	}
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

