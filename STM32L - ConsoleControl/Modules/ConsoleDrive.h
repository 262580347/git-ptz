#ifndef		_CONSOLEDRIVE_H_
#define		_CONSOLEDRIVE_H_

#define		MOTOR_POWER_PIN		GPIO_Pin_6
#define		MOTOR_POWER_TYPE	GPIOA

#define		RS485_POWER_PIN		GPIO_Pin_1
#define		RS485_POWER_TYPE	GPIOB

#define		PRESET_INDX		5	

#define		SPEED_SLOW		15
#define		SPEED_MID		16
#define		SPEED_FAST		17
#define		AUTOPOINT		18		//自动巡航点

//云台指令索引
#define		CONTROL_STOP				0		//停止以下命令
#define		CONTROL_UP					1		//上
#define		CONTROL_DOWN				2		//下
#define		CONTROL_LEFT				3		//左
#define		CONTROL_RIGHT				4		//右
#define		CONTROL_ZOOM_LONG			5		//变倍短
#define		CONTROL_ZOOM_SHORT			6		//变倍长
#define		CONTROL_FOCUS_CLOSE			7		//聚焦近
#define		CONTROL_FOCUS_FAR			8		//聚焦远
#define		CONTROL_APERTUR_ZOOMOUT		9		//光圈小
#define		CONTROL_APERTUR_ZOOMINT		10		//光圈大
#define		CONTROL_LIGHT_ON			11		//灯光开
#define		CONTROL_LIGHT_OFF			12		//灯光关
#define		CONTROL_GO_TO_PRESET		13		//转至预置点
#define		CONTROL_SET_PRESET			14		//设置预置点
#define		CONTROL_CLEAR_PRESET		15		//删除预置点


void SendConsoleCMD(unsigned char ConsoleCMD);

void SetPresetPoint(unsigned char Num);

void GotoPresetPoint(unsigned char Num);

void DelPresetPoint(unsigned char Num);

void ConsoleUp(void);

void ConsoleDown(void);

void ConsoleLeft(void);

void ConsoleRight(void);

void ConsoleStop(void);

void MotorPowerOn(void);

void MotorTestPowerOn(void);

void MotorPowerOff(void);

unsigned char GetConsolePosition(void);

void MotorPowerControl(unsigned char IsTrue);

unsigned char GetConsolePowerFlag(void);

void SetConsolePosition(unsigned char Point);

#endif

