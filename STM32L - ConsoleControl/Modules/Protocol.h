#ifndef _PROTOCOL_H_
#define _PROTOCOL_H_

#include "mytype.h"
#include "stm32l1xx_usart.h"

unsigned int Mod_Report_Data(USART_TypeDef* USARTx);

void Mod_Send_Alive(USART_TypeDef* USARTx);

unsigned int System_set_config(USART_TypeDef* USARTx, unsigned char *data, unsigned int len);

unsigned int System_get_config(USART_TypeDef* USARTx, CLOUD_HDR *pMsg, unsigned char *data, unsigned int len);

unsigned int Mod_Report_LowPower_Data(USART_TypeDef* USARTx);

void Mod_Send_Ack(USART_TypeDef* USARTx, CLOUD_HDR *hdr, unsigned char *data, unsigned char lenth);


#endif
