#ifndef _CPU_H_
#define	_CPU_H_

#define		SYS_CLOCK	31999 //k 4194

#define STM32_FLASH_BASE 		0x08000000 

void SysClockForHSE(void);

void SysClockForMSI(uint32_t RCC_MSIRange);

void GetSTM32ChipID(void);

void SetIWDG(unsigned int msTime);

void Delay_Init(u32 SYSCLK);	

void delay_us(u32 nus);

void delay_ms(u16 nms);

void Updata_IWDG(void);
#endif

