#include "ConsoleControl.h"
#include "main.h"

#define 	HOR_AUTO_PLUSE		100000
#define		VER_AUTO_PLUSE		22000
unsigned char s_MotorStatus = MOTOR_STOP;


typedef __packed struct
{
	unsigned char 			Magic; 	//0x55
	unsigned short 			Length;	//存储内容长度	
	unsigned char 			Chksum;	//校验和
	unsigned int			HorizontalPluse;	//水平角度脉冲数
	unsigned int			VerticalPluse;		//垂直角度脉冲数
}ANGLESTORE_CFG;

static ANGLESTORE_CFG	AngleStoreConfig;

static ANGLESTORE_CFG	*pthis = NULL;

static unsigned char s_BufData[sizeof(ANGLESTORE_CFG)] ;

static unsigned int s_NowHorizontalPluse = 0, s_NowVerticalPluse = 0;	//当前角度

static unsigned int s_SetHorizontalPluse = 0, s_SetVerticalPluse = 0;	//设置角度

static unsigned int s_SetPWMChannelCount = 0;	//设置的通道1的脉冲数

static unsigned int s_NowPWMChannelCount = 0;	//当前PWM通道1脉冲数

static volatile unsigned char s_MotorRunFlag = FALSE;	//电机是否在运行

static unsigned char s_InterruptFlag = FALSE;	//打断正在运行的电机标志

static unsigned char s_StorePluseFlag = FALSE;	//存储脉冲数据标志

static unsigned char s_MotorAutoRun = FALSE;	//电机自检标志

unsigned char GetMotorAutoRunFlag(void)
{
	return s_MotorAutoRun;
}

unsigned int GetNowHorizontalPluse(void)
{
	return s_NowHorizontalPluse;
}

unsigned int GetNowVerticalPluse(void)
{
	return s_NowVerticalPluse;
}

unsigned char GetMotorStatus(void)
{
	return s_MotorStatus;
}

void SetPWMPluse(unsigned int Pluse)
{
	s_NowPWMChannelCount = 0;
	s_SetPWMChannelCount = Pluse;
}

unsigned char GetMotorRunFlag(void)
{
	return s_MotorRunFlag;
}

typedef __packed struct
{
	unsigned char 			Magic; 	//0x55
	unsigned short 			Length;	//存储内容长度	
	unsigned char 			Chksum;	//校验和
	unsigned short			RunModeStatus;
}RUNMODE_CFG;

RUNMODE_CFG	RunModeConfig;
static RUNMODE_CFG	*pRunMode = NULL;

u8 s_RunModeData[sizeof(RUNMODE_CFG)] ;

void MotorRunModeInit(void)
{
	if (Check_Area_Valid(RUNMODE_FLAG_ADDR))
	{
		EEPROM_ReadBytes(RUNMODE_FLAG_ADDR, s_RunModeData, sizeof(RUNMODE_CFG));
		
		pRunMode = (RUNMODE_CFG *)s_RunModeData;
		
		RunModeConfig = *pRunMode;
		
		if(RunModeConfig.RunModeStatus == 0)
		{
			u1_printf(" 正式模式\r\n");
		}
		else if(RunModeConfig.RunModeStatus == 1)
		{
			u1_printf(" 测试模式\r\n");
		}
		else
		{
			u1_printf(" 错误模式\r\n");
		}
	}
	else	//第一次上电初始化
	{
		RunModeConfig.Magic 		= VAILD;
		RunModeConfig.Length 		= 0X01;
		RunModeConfig.RunModeStatus = 0;
		RunModeConfig.Chksum = Calc_Checksum((unsigned char *)&RunModeConfig.RunModeStatus, 1);
		EEPROM_WriteBytes(RUNMODE_FLAG_ADDR, (unsigned char *)&RunModeConfig, sizeof(RUNMODE_CFG));
		u1_printf(" First Power On, Run Mode is 0\r\n");
	}
}

void WriteMotorRunMode(unsigned char RunMode)
{
	pRunMode->RunModeStatus = RunMode;
	pRunMode->Chksum = Calc_Checksum((unsigned char *)&pRunMode->RunModeStatus, 1);
	EEPROM_WriteBytes(RUNMODE_FLAG_ADDR, (unsigned char *)pRunMode, sizeof(RUNMODE_CFG));
}

unsigned char ReadMotorRunMode(void)
{
	if (Check_Area_Valid(RUNMODE_FLAG_ADDR))
	{
		EEPROM_ReadBytes(RUNMODE_FLAG_ADDR, s_RunModeData, sizeof(RUNMODE_CFG));
		
		pRunMode = (RUNMODE_CFG *)s_RunModeData;
		RunModeConfig = *pRunMode;
		
		return RunModeConfig.RunModeStatus;
	}
	else
	{
		return 255;
	}
}

void ConsoleConfigInit(void)
{
	if (Check_Area_Valid(ANGLE_STORE_ADDR))
	{
		EEPROM_ReadBytes(ANGLE_STORE_ADDR, s_BufData, sizeof(ANGLESTORE_CFG));
		
		pthis = (ANGLESTORE_CFG *)s_BufData;
		
		AngleStoreConfig = *pthis;
		s_NowHorizontalPluse = AngleStoreConfig.HorizontalPluse;
		s_NowVerticalPluse = AngleStoreConfig.VerticalPluse;
		
		u1_printf("\r\n Now HorizontalPluse:%d, VerticalPluse:%d\r\n", s_NowHorizontalPluse, s_NowVerticalPluse);
	}
	else	//第一次上电初始化
	{
		AngleStoreConfig.Magic 				= VAILD;
		AngleStoreConfig.Length 			= sizeof(ANGLESTORE_CFG) - 4;
		AngleStoreConfig.HorizontalPluse 	= 0;
		AngleStoreConfig.VerticalPluse 		= 0;
		AngleStoreConfig.Chksum = Calc_Checksum((unsigned char *)&AngleStoreConfig.HorizontalPluse, 8);
		EEPROM_WriteBytes(ANGLE_STORE_ADDR, (unsigned char *)&AngleStoreConfig, sizeof(ANGLESTORE_CFG));
		u1_printf(" The first power on Angle reset\r\n");
		
		EEPROM_ReadBytes(ANGLE_STORE_ADDR, s_BufData, sizeof(ANGLESTORE_CFG));
		
		pthis = (ANGLESTORE_CFG *)s_BufData;
		
		AngleStoreConfig = *pthis;
		s_NowHorizontalPluse = AngleStoreConfig.HorizontalPluse;
		s_NowVerticalPluse = AngleStoreConfig.VerticalPluse;
	}	
}

void ReadStoreAngle(void)
{
	if (Check_Area_Valid(ANGLE_STORE_ADDR))
	{
		EEPROM_ReadBytes(ANGLE_STORE_ADDR, s_BufData, sizeof(ANGLESTORE_CFG));
		
		if(pthis != NULL)
		{
			pthis = (ANGLESTORE_CFG *)s_BufData;
			AngleStoreConfig = *pthis;
			
			s_NowHorizontalPluse = AngleStoreConfig.HorizontalPluse;
			s_NowVerticalPluse = AngleStoreConfig.VerticalPluse;
			
			u1_printf("HorizontalPluse:%d, VerticalPluse:%d\r\n", s_NowHorizontalPluse, s_NowVerticalPluse);
		}
	}
	else
	{
		u1_printf(" Read Angle ErrorStatus\r\n");
	}
}

static void WriteNowAngle(unsigned int HorizontalPluse, unsigned int VerticalPluse)
{
	if(pthis != NULL)
	{
		pthis->HorizontalPluse = HorizontalPluse;
		pthis->VerticalPluse = VerticalPluse;
		pthis->Chksum = Calc_Checksum((unsigned char *)&pthis->HorizontalPluse, 8);
		EEPROM_WriteBytes(ANGLE_STORE_ADDR, (unsigned char *)pthis, sizeof(ANGLESTORE_CFG));
	}
}

static void StorePluse(void)
{
	if(s_StorePluseFlag)
	{
		s_StorePluseFlag = FALSE;
		
		WriteNowAngle(s_NowHorizontalPluse, s_NowVerticalPluse);
	}
}

static void MotorStart(void)
{
	u32 i = 0;
	
	if(s_MotorRunFlag)
	{
		s_InterruptFlag = TRUE;
		
		i = 0;
		while(s_MotorRunFlag)
		{
			i++;
			if(i > 0xffffff)
			{
				u1_printf(" Over Err\r\n");
				break;
			}
		}
		StorePluse();
		delay_ms(20);
	}

}

void MotorControl_Stop(void)
{
	if(s_MotorRunFlag)
	{
		s_InterruptFlag = TRUE;		
	}
}

void MotorControl_Up(unsigned int Pluse)
{
	if(s_MotorAutoRun)
	{
		return;
	}
	
	MotorStart();
	
	HOR_MOTOR_DISABLE();

	VER_MOTOR_ENABLE();
		
	VER_DIR_UP();

	delay_ms(10);
	
	SetPWMPluse(Pluse);
	TIM_Cmd(TIM3, ENABLE);
	s_MotorRunFlag = TRUE;
	
	s_MotorStatus = MOTOR_RUN_UP;
}

void MotorControl_Down(unsigned int Pluse)
{
	if(s_MotorAutoRun)
	{
		return;
	}
	
	MotorStart();
	
	HOR_MOTOR_DISABLE();
	VER_MOTOR_ENABLE();
	
	VER_DIR_DOWN();
	
	delay_ms(10);
	
	SetPWMPluse(Pluse);
	TIM_Cmd(TIM3, ENABLE);
	
	s_MotorRunFlag = TRUE;
		
	s_MotorStatus = MOTOR_RUN_DOWN;
	
}

void MotorControl_Left(unsigned int Pluse)
{
	if(s_MotorAutoRun)
	{
		return;
	}
	
	MotorStart();
	
	HOR_MOTOR_ENABLE();
	VER_MOTOR_DISABLE();
	
	HOR_DIR_LEFT();
	
	delay_ms(10);
	
	SetPWMPluse(Pluse);
	TIM_Cmd(TIM3, ENABLE);
	
	s_MotorRunFlag = TRUE;
	
	s_MotorStatus = MOTOR_RUN_LEFT;
}

void MotorControl_Right(unsigned int Pluse)
{
	if(s_MotorAutoRun)
	{
		return;
	}
	
	MotorStart();
	
	HOR_MOTOR_ENABLE();
	VER_MOTOR_DISABLE();
	
	delay_ms(10);
	HOR_DIR_RIGHT();
	
	SetPWMPluse(Pluse);
	TIM_Cmd(TIM3, ENABLE);
	
	s_MotorRunFlag = TRUE;
	
	s_MotorStatus = MOTOR_RUN_RIGHT;	
}
/**********************************************
		设置固定角度
**********************************************/
static void MotorGotoAngleSubTask2(void)
{
	if(s_MotorRunFlag == FALSE)
	{
		u1_printf(" To the Ver Position\r\n");
		
		s_MotorAutoRun = FALSE;
		
		Task_Kill(MotorGotoAngleSubTask2);
		
		s_MotorStatus = MOTOR_STOP;
	}	
}

static void MotorGotoAngleSubTask1(void)
{
	if(s_MotorRunFlag == FALSE)
	{
		u1_printf(" To the Hor Position\r\n");
		
		s_MotorAutoRun = FALSE;
		
		if(s_SetVerticalPluse > s_NowVerticalPluse)
		{
			delay_ms(10);
//			MotorControl_Up(s_SetVerticalPluse - s_NowVerticalPluse);		//垂直角度最低为0度电
			MotorControl_Down(s_SetVerticalPluse - s_NowVerticalPluse);		//垂直角度水平为0度点
		}
		else if(s_SetVerticalPluse < s_NowVerticalPluse)
		{
			delay_ms(10);
//			MotorControl_Down(s_NowVerticalPluse - s_SetVerticalPluse);
			MotorControl_Up(s_NowVerticalPluse - s_SetVerticalPluse);

		}
		else
		{
			MotorStart();
		}
	
		s_MotorAutoRun = TRUE;
		
		Task_Create(MotorGotoAngleSubTask2, 10);
		
		Task_Kill(MotorGotoAngleSubTask1);
	}
}

void Motor_GotoSetAngle(unsigned int Hor, unsigned int Ver)	//实际角度
{
	Hor *= HOR_ANGLETOPLUSE;
		
	Ver *= VER_ANGLETOPLUSE;

	if(Hor > MAX_HOR_PLUSE)
	{
		Hor = MAX_HOR_PLUSE;
	}
	
	if(Ver > MAX_VER_PLUSE)
	{
		Ver = MAX_VER_PLUSE;
	}
							
	MotorControl_GotoAngle(Hor, Ver);
}
			
void MotorControl_GotoAngle(unsigned int Hor_Angle, unsigned int Ver_Angle)	//脉冲数
{
	s_SetHorizontalPluse = Hor_Angle;
	s_SetVerticalPluse = Ver_Angle;
	
	if(s_SetHorizontalPluse > s_NowHorizontalPluse)
	{
		MotorControl_Left(s_SetHorizontalPluse - s_NowHorizontalPluse);		
	}
	else if(s_SetHorizontalPluse < s_NowHorizontalPluse)
	{
		MotorControl_Right(s_NowHorizontalPluse - s_SetHorizontalPluse);
	}
	else
	{
		MotorStart();
	}
	s_MotorAutoRun = TRUE;
	Task_Create(MotorGotoAngleSubTask1, 10);
	
}
/**********************************************
	自动返回原点
**********************************************/
static void MotorAutoRunSubTask2(void)
{
	if(s_MotorRunFlag == FALSE)
	{
		u1_printf(" To the Down\r\n");
		
		s_MotorAutoRun = FALSE;
		
		Task_Kill(MotorAutoRunSubTask2);
		
		s_MotorStatus = MOTOR_STOP;
	}	
}

static void MotorAutoRunSubTask1(void)
{
	if(s_MotorRunFlag == FALSE)
	{
		u1_printf(" To the Right\r\n");
		
		s_MotorAutoRun = FALSE;
		
		delay_ms(10);
		MotorControl_Up(VER_AUTO_PLUSE);
	
		s_MotorAutoRun = TRUE;
		
		Task_Create(MotorAutoRunSubTask2, 10);
		
		Task_Kill(MotorAutoRunSubTask1);
	}
}


void MotorAutoRun(void)
{
	MotorControl_Right(HOR_AUTO_PLUSE);
	
	s_MotorAutoRun = TRUE;
	
	Task_Create(MotorAutoRunSubTask1, 10);
}

void Init_Motor_Port(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;	
			
	GPIO_InitStructure.GPIO_Pin = HORIZONTAL_DIR_PIN | HORIZONTAL_EN_PIN | HORIZONTAL_RST_PIN \
	| HORIZONTAL_SLEEP_PIN | VERTICAL_DIR_PIN | VERTICAL_EN_PIN | VERTICAL_RST_PIN | VERTICAL_SLEEP_PIN;	
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
	GPIO_Init(CONSOLE_CONTROL_TYPE, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = MOTOR_STEP_MS1_PIN | MOTOR_STEP_MS2_PIN | MOTOR_STEP_MS3_PIN;
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
	GPIO_Init(MOTOR_STEP_TYPE, &GPIO_InitStructure);
	
		// MS1 MS2 MS3 4W1-2 Phase
	GPIO_SetBits(MOTOR_STEP_TYPE,MOTOR_STEP_MS1_PIN);
	GPIO_SetBits(MOTOR_STEP_TYPE,MOTOR_STEP_MS2_PIN);
	GPIO_SetBits(MOTOR_STEP_TYPE,MOTOR_STEP_MS3_PIN);
	
	GPIO_SetBits(CONSOLE_CONTROL_TYPE,HORIZONTAL_RST_PIN);
	GPIO_SetBits(CONSOLE_CONTROL_TYPE,VERTICAL_RST_PIN);
	
	delay_ms(80);
	
	GPIO_ResetBits(CONSOLE_CONTROL_TYPE,HORIZONTAL_RST_PIN);
	GPIO_ResetBits(CONSOLE_CONTROL_TYPE,VERTICAL_RST_PIN);
	
	delay_ms(80);
	
	GPIO_SetBits(CONSOLE_CONTROL_TYPE,HORIZONTAL_RST_PIN);
	GPIO_SetBits(CONSOLE_CONTROL_TYPE,VERTICAL_RST_PIN);	
			
	GPIO_SetBits(CONSOLE_CONTROL_TYPE,HORIZONTAL_RST_PIN);
	GPIO_SetBits(CONSOLE_CONTROL_TYPE,VERTICAL_RST_PIN);
	
	GPIO_SetBits(CONSOLE_CONTROL_TYPE,HORIZONTAL_SLEEP_PIN);
	GPIO_SetBits(CONSOLE_CONTROL_TYPE,VERTICAL_SLEEP_PIN);
	
	HOR_DIR_LEFT();
	VER_DIR_UP();
}

static void TaskForObserStorePluse(void)
{
	if(s_StorePluseFlag)
	{
		s_StorePluseFlag = FALSE;
		
		WriteNowAngle(s_NowHorizontalPluse, s_NowVerticalPluse);
	}	
}

void Console_Control_Init(void)
{
	Init_Motor_Port();
	
	ConsoleConfigInit();
	
	Task_Create(TaskForObserStorePluse, 1);
	
	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
	
}
void Timer3_PWMOutoutModeInit(void)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	uint16_t CCR1_Val = 1000;
	uint16_t CCR2_Val = 1000;

//	uint16_t PrescalerValue = 0;
	/* --------------------------- System Clocks Configuration ---------------------*/
	/* TIM3 clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	/* GPIOA and GPIOB clock enable */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA|RCC_AHBPeriph_GPIOB, ENABLE);

	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	/*--------------------------------- GPIO Configuration -------------------------*/
	/* GPIOA Configuration: Pin 6 and 7 */
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_4 | GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;

	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource4, GPIO_AF_TIM3);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_TIM3);


   /* TIM3 configuration: One Pulse mode ------------------------
     The external signal is connected to TIM3_CH2 pin (PA.07), 
     The Rising edge is used as active edge,
     The One Pulse signal is output on TIM3_CH1 pin (PA.06)
     The TIM_Pulse defines the delay value 
     The (TIM_Period - TIM_Pulse) defines the One Pulse value.
     TIM3CLK = SystemCoreClock, we want to get TIM3 counter clock at 16 MHz:
     - Prescaler = (TIM3CLK / TIM3 counter clock) - 1
     The Autoreload value is 65535 (TIM3->ARR), so the maximum frequency value 
     to trigger the TIM3 input is 16000000/65535 = 244.144 Hz.

     The TIM_Pulse defines the delay value, the delay value is fixed 
     to 1.023 ms:
     delay =  CCR1/TIM3 counter clock = 1.023 ms. 
     The (TIM_Period - TIM_Pulse) defines the One Pulse value, 
     the pulse value is fixed to 3.072 ms:
     One Pulse value = (TIM_Period - TIM_Pulse) / TIM3 counter clock = 3.072 ms.

  * SystemCoreClock is set to 32 MHz for Low-density devices
	------------------------------------------------------------ */
//	PrescalerValue = (uint16_t) (SystemCoreClock / 8000000)*1 - 1;
	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 1999;
	TIM_TimeBaseStructure.TIM_Prescaler = 1;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	/* PWM1 Mode configuration: Channel1 */
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = CCR1_Val;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;

	TIM_OC1Init(TIM3, &TIM_OCInitStructure);

	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);

	/* PWM1 Mode configuration: Channel2 */
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = CCR2_Val;

	TIM_OC2Init(TIM3, &TIM_OCInitStructure);

	TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);

	//  TIM_ARRPreloadConfig(TIM3, ENABLE);



	/* TIM3 enable counter */
	TIM_Cmd(TIM3, DISABLE);
	
	TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
	
	TIM_ClearFlag(TIM3,TIM_FLAG_Update);
}



void TIM4_PWM_Input(void)
{
	TIM_ICInitTypeDef TIM_ICInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
		/* TIM3 clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	/* GPIOA and GPIOB clock enable */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA|RCC_AHBPeriph_GPIOB, ENABLE);

	/*--------------------------------- GPIO Configuration -------------------------*/
	/* GPIOA Configuration: Pin 6 and 7 */
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;

	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_TIM4);
  /* TIM3 configuration: PWM Input mode ------------------------
     The external signal is connected to TIM3 CH2 pin (PA.07), 
     The Rising edge is used as active edge,
     The TIM3 CCR2 is used to compute the frequency value 
     The TIM3 CCR1 is used to compute the duty cycle value
  ------------------------------------------------------------ */
  
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_1;
	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;
	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
	TIM_ICInitStructure.TIM_ICFilter = 0x0;

	TIM_PWMIConfig(TIM4, &TIM_ICInitStructure);

	/* Input Trigger selection */
	TIM_SelectInputTrigger(TIM4, TIM_TS_TI2FP2);

	/* Select the slave Mode: Reset Mode */
	TIM_SelectSlaveMode(TIM4, TIM_SlaveMode_Reset);

	/* Enable the Master/Slave Mode */
	TIM_SelectMasterSlaveMode(TIM4, TIM_MasterSlaveMode_Enable);

	/* TIM3 enable counter */
	TIM_Cmd(TIM4, DISABLE);

	/* Enable the CC2 Interrupt Request */
	TIM_ITConfig(TIM4, TIM_IT_CC1, ENABLE);

//	Task_Create(TaskForShowInfo, 80000);
}

void TIM3_IRQHandler(void)
{	
	if(TIM_GetITStatus(TIM3,TIM_IT_Update))
	{
		/* Clear TIM3 Capture compare interrupt pending bit */
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
		
		s_NowPWMChannelCount++;
		
		if(s_InterruptFlag || s_NowPWMChannelCount >= s_SetPWMChannelCount)
		{
			TIM_Cmd(TIM3, DISABLE);
			
			s_InterruptFlag = FALSE;
			
			switch(s_MotorStatus)
			{
				case MOTOR_RUN_UP:
					
					if(s_NowVerticalPluse >= s_NowPWMChannelCount)
					{
						s_NowVerticalPluse -= s_NowPWMChannelCount;
					}
					else
					{
						s_NowVerticalPluse = 0;
					}
														
				break;
					
				case MOTOR_RUN_DOWN:
					
					s_NowVerticalPluse += s_NowPWMChannelCount;
	
					if(s_NowVerticalPluse > MAX_VER_PLUSE )
					{
						s_NowVerticalPluse = MAX_VER_PLUSE;
					}
				
				break;
				
				case MOTOR_RUN_LEFT:
					
					s_NowHorizontalPluse += s_NowPWMChannelCount;
		
					if(s_NowHorizontalPluse > MAX_HOR_PLUSE )
					{
						s_NowHorizontalPluse = MAX_HOR_PLUSE;
					}
				
				break;
				
				case MOTOR_RUN_RIGHT:
					
					if(s_NowHorizontalPluse >= s_NowPWMChannelCount)
					{
						s_NowHorizontalPluse -= s_NowPWMChannelCount;
					}
					else
					{
						s_NowHorizontalPluse = 0;
					}
					
				break;
					
				default:
					
				break;
				
			}

			if(s_MotorAutoRun == FALSE)
			{
				s_MotorStatus = MOTOR_STOP;
			}
			
			s_StorePluseFlag = TRUE;
			
			s_MotorRunFlag = FALSE;	
			
			HOR_MOTOR_DISABLE();
			VER_MOTOR_DISABLE();	
		}
	}
}

