#include "main.h"
#include "GPRS.h"

#define COM_DATA_SIZE	256

static void GPRSObser(unsigned short nMain10ms);

static u8 m_CommBuff[COM_DATA_SIZE];

static u16 m_CirWPinter = 0;

static u16 m_CirRPinter = 0;

static u8 s_HeartPackFlag = FALSE;

static u8 s_DataPackFlag = FALSE;

static u8 s_GPRSReadFlag = FALSE;

unsigned char GetGPRSReadyFlag( void )
{
	return s_GPRSReadFlag;
}

void SetGPRSReadyFlag( unsigned char ucFlag )
{
	s_GPRSReadFlag = ucFlag;
}

static void Clear_COMBuff(void)
{
	u16 i;
	
	for(i=0; i<COM_DATA_SIZE; i++)
	{
		m_CommBuff[i] = 0;		
	}
		m_CirWPinter = 0;

		m_CirRPinter = 0;	
}

void OnGPRSComm(u8 CommData)
{
	m_CommBuff[m_CirWPinter++] = CommData;
	
	if(m_CirWPinter >= COM_DATA_SIZE)
	{
		m_CirWPinter = 0;
	}
	if(m_CirWPinter == m_CirRPinter)
	{
		m_CirRPinter++;
		if(m_CirRPinter	>= COM_DATA_SIZE)
		{
		    m_CirRPinter = 0; 
		}
	}	
}

void GPRS_Process(unsigned short nMain10ms)
{
	static unsigned short s_LastTime;
	static unsigned char Run_Once = FALSE, s_State = 1, s_RetryCount = 0;
	static u8 s_OverCount;
	SYSTEMCONFIG *p_sys;

	p_sys = GetSystemConfig();
	
	if(Run_Once == FALSE)
	{
		Run_Once = TRUE;
		u1_printf("\r\n GPRS Init ...\r\n");
		USART2_Config(115200);
		s_LastTime = nMain10ms;
	}
	
	switch(s_State)
	{
		case 1: //等待连上网络
			
			if(GetGPRSReadyFlag())
			{
				s_LastTime = nMain10ms;
				s_State++;
				s_RetryCount = 0;
			}
			else if(CalculateTime(nMain10ms, s_LastTime) >= 300)
			{
				Mod_Send_Alive(GPRS_COM);
				s_LastTime = nMain10ms;
				s_RetryCount++;
				if(s_RetryCount >= 10)
				{
						u1_printf("\r\n GPRS网络连接超时  重启\r\n");
						while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);	
						Sys_Soft_Reset();
				}
			}			
		break;
		
		case 2:
			if(GetGPRSReadyFlag())
			{
				Clear_Flag();
				s_State++;
				s_LastTime = nMain10ms;
			}
			else
			{
				s_State--;
			}
		break;
		
		case 3:
			if(g_PowerDetectFlag)
			{
				s_HeartPackFlag = FALSE;
				Mod_Send_Alive(GPRS_COM);
				s_State++;
				s_LastTime = nMain10ms;
			}
		break;

		case 4:
			if(s_HeartPackFlag)
			{
				s_HeartPackFlag = FALSE;
				Mod_Report_Data(GPRS_COM);
				s_State++;
				s_LastTime = nMain10ms;
			}
			else
			{
				if( CalculateTime(nMain10ms,s_LastTime) >= 500 )
				{
					s_State--;
					s_LastTime = nMain10ms;
					u1_printf("超时未收到心跳回复包\r\n");
					s_OverCount++;
					if(s_OverCount >= 5)
					{
						s_OverCount = 0;
						SetGPRSReadyFlag(FALSE);
					}
				}
			}
		break;	

		case 5:
			if(s_DataPackFlag)
			{
				s_State++;
				s_LastTime = nMain10ms;
				s_DataPackFlag = FALSE;
				s_HeartPackFlag = FALSE;
			}
			else
			{
				if( CalculateTime(nMain10ms,s_LastTime) >= 500 )
				{
					s_State--;
					s_LastTime = nMain10ms;
					u1_printf("超时未收到数据回复包\r\n");
					s_OverCount++;
					if(s_OverCount >= 5)
					{
						s_OverCount = 0;
						SetGPRSReadyFlag(FALSE);
					}
				}
			}			
		break;
			
		case 6:
			if( CalculateTime(nMain10ms,s_LastTime) >= p_sys->Heart_interval*100 )
			{
				s_State = 2;
			}
		break;
	}
	
	
	GPRSObser(nMain10ms);

}

void OnRecGPRSData(CLOUD_HDR *pMsg, u8 *data, u8 lenth)
{
	CLOUD_HDR *hdr;
	u8 i, sendbuf[100], sendlenth;
	RTC_TimeTypeDef RTC_TimeStructure;	
	RTC_DateTypeDef RTC_DateStruct;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
	//解析协议包
	u1_printf("\r\n [%02d:%02d:%02d][GPRS] <- 协议号：%02d 设备号：%04d 方向：%X 包序号：%04X 包长度：%X 命令字：%02X 数据:", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, pMsg->protocol, pMsg->device_id, pMsg->dir, pMsg->seq_no, pMsg->payload_len, pMsg->cmd);
	for(i=0; i<lenth; i++)
		u1_printf("%02X ", data[i]);
	u1_printf("\r\n");

	if(pMsg->cmd == CMD_HEATBEAT)
	{
		u1_printf("收到服务器心跳包回复,同步服务器时间\r\n");
			
		RTC_TimeStructure.RTC_H12     = RTC_H12_AM;
				
		RTC_TimeStructure.RTC_Hours   = data[2];
		RTC_TimeStructure.RTC_Minutes = data[1];
		RTC_TimeStructure.RTC_Seconds = data[0];

		RTC_DateStruct.RTC_Date  	= data[3];
		RTC_DateStruct.RTC_Month  	= data[4];
		RTC_DateStruct.RTC_Year  	= data[5];	
		RTC_SetDate(RTC_Format_BIN, &RTC_DateStruct);
		
		if(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure)  != ERROR)
		{
			u1_printf("\r\n 同步远程时间成功\r\n");
			RTC_TimeShow();
		}
		else
		{
			u1_printf("\r\n 同步远程时间失败\r\n");
		}
		
		s_HeartPackFlag = TRUE;
		
		SetGPRSReadyFlag(TRUE);
	}
	
	if(pMsg->cmd == CMD_REPORT_D)
	{
		u1_printf("收到服务器上报数据回复,上报数据成功\r\n");
		
		s_DataPackFlag = TRUE;
	}
	
	if(pMsg->cmd == CMD_GET_CONFIG)//获取设备配置
	{
		System_get_config(GPRS_COM, pMsg, data, lenth);
	}
	
	if(pMsg->cmd == CMD_SET_CONFIG) //设置设备配置
	{
		memcpy(sendbuf, pMsg, sizeof(CLOUD_HDR));
		sendlenth = sizeof(CLOUD_HDR);
		
		hdr = (CLOUD_HDR *)sendbuf;
		hdr->protocol = swap_word(hdr->protocol);	
		hdr->device_id = swap_dword(hdr->device_id);
		hdr->seq_no = swap_word(hdr->seq_no);	
		hdr->dir = 1;
		hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+4);
		memcpy(&sendbuf[sizeof(CLOUD_HDR)], data, lenth);
		System_set_config(GPRS_COM, sendbuf, lenth + sendlenth);
		u1_printf("更新设备参数，等待设备重启\r\n");
		while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);	
		Sys_Soft_Reset();
	}
}
	
static void GPRSObser(unsigned short nMain10ms)
{
	static u8 FrameID = 0, special_flag = FALSE, s_HeadFlag = FALSE;
	static u16 data_lenth = 0, s_LastTime = 0, i = 0;
	static CLOUD_HDR m_RxFrame;
	static u8 RxData, databuff[100];
	u8 checksum;
	
	if(s_HeadFlag == TRUE)
	{
		if(CalculateTime(nMain10ms, s_LastTime) >= 100)
		{
			s_HeadFlag = FALSE;
			u1_printf("\r\n 接收超时\r\n");
			Clear_Uart2Buff();
			memset(databuff, 0, 100);
			data_lenth = 0;
			i = 0;
			FrameID = 0;
			return ;
		}
	}
		
	if(m_CirRPinter != m_CirWPinter)
	{
		RxData = m_CommBuff[m_CirRPinter++];
		if(m_CirRPinter >= COM_DATA_SIZE)
		{
			m_CirRPinter = 0;
		}
		if(RxData == 0x7d && special_flag == FALSE)
		{
			special_flag = TRUE;
			return;
		}
		if(special_flag == TRUE)
		{
			special_flag = FALSE;
			if(RxData == 0x5d)
			{
				RxData = 0x7d;
			}
			else if(RxData == 0x5e)
			{
				RxData = 0x7e;
			}
			else if(RxData == 0x51)
			{
				RxData = 0x21;
			}
		}



		switch(FrameID)
		{
			case 0:
				if(RxData == 0X7e)
				{
					s_HeadFlag = TRUE;
					s_LastTime = nMain10ms;
					FrameID++;
				}
				break;
			case 1:
				m_RxFrame.protocol = RxData;
				FrameID++;
			break;
			case 2:
				m_RxFrame.protocol = (m_RxFrame.protocol << 8) | RxData;
				FrameID++;
			break;
			case 3:
				m_RxFrame.device_id = RxData;
				FrameID++;
			break;
			case 4:
				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
				FrameID++;
			break;
			case 5:
				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
				FrameID++;
			break;
			case 6:
				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
				FrameID++;
			
			break;
			case 7:
				m_RxFrame.dir = RxData;
				FrameID++;
			break;
			case 8:
				m_RxFrame.seq_no = RxData;
				FrameID++;
			break;
			case 9:
				m_RxFrame.seq_no = (m_RxFrame.seq_no << 8) | RxData;
				FrameID++;
			break;
			case 10:
				m_RxFrame.payload_len = RxData;
				FrameID++;
			break;
			case 11:
				m_RxFrame.payload_len = (m_RxFrame.payload_len << 8) | RxData;
				data_lenth = m_RxFrame.payload_len;
				FrameID++;
			break;
			case 12:
				m_RxFrame.cmd = RxData;				
				i = 0;
			    memset(databuff, 0, 100);
				FrameID++;
				if(data_lenth == 0)
				{
					FrameID = 14;
					break;
				}	
				else if(data_lenth >= 100)
				{
					u1_printf("\r\n too long %d\r\n", data_lenth );
					Clear_Uart2Buff();
					memset(databuff, 0, 100);
					data_lenth = 0;
					i = 0;
					FrameID = 0;
				}
			break;
			case 13:				
				databuff[i++] = RxData;
				if(i >= data_lenth)
				{
					FrameID++;
				}
			break;		
			case 14:
				checksum = RxData;
				if(CheckHdrSum(&m_RxFrame, databuff, data_lenth) != checksum)
				{
					u1_printf("[GPRS]Check error...");
					u1_printf("Rec:%02X, CheckSum:%02X\r\n", checksum, CheckHdrSum(&m_RxFrame, databuff, data_lenth));
					FrameID = 0;	
					s_HeadFlag = FALSE;					
					Clear_Uart3Buff();	
					Clear_COMBuff();
					s_HeadFlag = FALSE;		
					memset(databuff, 0, 100);
					data_lenth = 0;
					i = 0;
				}else
				FrameID++;
			break;
			case 15:
				if(RxData == 0x21)	
				{								
					OnRecGPRSData(&m_RxFrame, databuff, data_lenth);
				}
				else
				{				
					u1_printf("end error!\r\n");
				}
				s_HeadFlag = FALSE;		
				Clear_Uart2Buff();
				memset(databuff, 0, 100);
				data_lenth = 0;
				i = 0;
			    FrameID = 0;				
			break;
			default:
				s_HeadFlag = FALSE;		
				Clear_Uart2Buff();
				memset(databuff, 0, 100);
				data_lenth = 0;
				i = 0;
			    FrameID = 0;	 
			break;	
		}
	}	
}

