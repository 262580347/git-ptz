#ifndef		_LORA_H_
#define		_LORA_H_
#include "stm32l1xx.h"


#define 	CMD_HEATBEAT			0
#define		CMD_CLOSE_SOCKET	(0x0063)
#define 	CMD_REPORT_D		(0x0009)	//上报输入参量数据
#define 	CMD_REPORT_S		(0x000A)	//上报输出控制设备状态
#define 	CMD_REPORT_HS_D		(0x000B)	//上报历史保存传感器数据
#define	 	CMD_REGISTER		(0x000D)
#define 	CMD_POLLING			(0x000E)
#define 	CMD_GET_CONFIG	 	(0x0004)	//读取设备通信配置信息
#define 	CMD_SET_CONFIG	  	(0x0005)	//写入设备通信配置信息
#define		CMD_TEST			(0xAA)
#define		CMD_REBOOT			(0x07)

#define CFG_ID_1			(0x9B)
#define CFG_ID_2			(0x9C)
#define CFG_ID_3			(0x9D)
#define CFG_ID_4			(0x9E)

//通信过程中用的结构体
typedef __packed struct
{
	unsigned char cfg_id1; 		//		//分组标识ID
	unsigned int device_id;			//设备编号
	unsigned char net_type;				//网络类型
	unsigned char cfg_id2;				//分组标识ID
	unsigned short int zigbee_net;			//网络号
	unsigned short int zigbee_node;			//设备地址
	unsigned short int zigbee_gateway;		//网关地址
	unsigned char zigbee_channel;		//通道
	unsigned char zigbee_LowpowerFlag;		//拓扑结构
	unsigned char cfg_id3;				//分组标识ID
	unsigned char gprs_server[4];		//GPRS服务器地址
	unsigned short int gprs_port;			//GPRS端口
	unsigned char cfg_id4;				//分组标识ID
	unsigned char retry_times;		 	//重发次数
	unsigned short int h_interval;			//心跳间隔
	unsigned short int d_interval;			//数据间隔
	unsigned short int s_interval;			//状态间隔
}COM_SYSTEM_CFG;

typedef  struct
{
	unsigned char Addr_H;
	unsigned char Addr_L;
	unsigned char Speed;
	unsigned char Channel;
	unsigned char Option;
}LORA_CONFIG;

extern LORA_CONFIG g_LoraConfig;

typedef __packed struct
{
	unsigned char magic; 	//0x55
	unsigned short length;	//存储内容长度	
	unsigned char chksum;	//校验和
}SYS_TAG;

#define		LORA_WORK_DELAY 		delay_ms(100)
#define		LORA_SLEEP_DELAY 		delay_ms(20)

#define		LORA_ENABLE				1	

#define		LORA_GPIO_TYPE		GPIOA
#define		LORA_M0_PIN			GPIO_Pin_0	//m0 connect m1
#define		LORA_M1_PIN			GPIO_Pin_1	//m0 connect m1

#define		LORA_AUX_TYPE		GPIOA		//未使用
#define		LORA_AUX_PIN		GPIO_Pin_4

#define		LORA_UART_TYPE		GPIOB
#define		LORA_TX_PIN			GPIO_Pin_10
#define		LORA_RX_PIN			GPIO_Pin_11

#define		LORA_PWR_PIN		GPIO_Pin_5
#define		LORA_PWR_TYPE		GPIOA
#define		LORA_PWR_ON()		GPIO_SetBits(LORA_PWR_TYPE, LORA_PWR_PIN);
#define		LORA_PWR_OFF()		GPIO_ResetBits(LORA_PWR_TYPE, LORA_PWR_PIN);

#define		LORA_WORK_MODE()		GPIO_ResetBits(LORA_GPIO_TYPE, LORA_M0_PIN);	GPIO_ResetBits(LORA_GPIO_TYPE, LORA_M1_PIN)
#define		LORA_SLEEP_MODE() 		GPIO_SetBits(LORA_GPIO_TYPE, LORA_M0_PIN);		GPIO_SetBits(LORA_GPIO_TYPE, LORA_M1_PIN)
#define		LORA_ROUSE_MODE()		GPIO_SetBits(LORA_GPIO_TYPE, LORA_M0_PIN);		GPIO_ResetBits(LORA_GPIO_TYPE, LORA_M1_PIN)
#define		LORA_LOWPOWER_MODE()	GPIO_ResetBits(LORA_GPIO_TYPE, LORA_M0_PIN);	GPIO_SetBits(LORA_GPIO_TYPE, LORA_M1_PIN)

void Lora_Send_Data(unsigned short int Tagetaddr, unsigned char channel, unsigned char *data, unsigned int len);

unsigned int Lora_Report_LowPower_Data(void);

void LoraProcess(unsigned short nMain100ms);

void LoraPort_Init(void);

void Lora_Power(FunctionalState NewState);

void OnComm(unsigned char CommData);
	
void Clear_LoraCOMBuff(void);

unsigned char GetLoraRegisterFlag(void);

unsigned char GetLowPowerFlag(void);

void SetLowPowerFlag(unsigned char isTRUE);

unsigned char GetLoraState(void);
#endif

