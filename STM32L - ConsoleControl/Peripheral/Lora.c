/**********************************
说明：LORA通信模块，使用500ms唤醒间隔，TXD，AUX推挽输出,RXD 上拉输入
	  FEC开启，20dBm发射功率，2.4K空中速率
作者：关宇晟
版本：V2018.4.3
***********************************/

#include "Lora.h"
#include "main.h"

#define COM_DATA_SIZE	256

//#define	LORA_SPEED_INIT 	0x3A	//2.4K
#define	LORA_SPEED_INIT 	0x3D		//19.2K
#define	LORA_OPTION			0xC0

LORA_CONFIG g_LoraConfig;
LORA_CONFIG *p_Lora_Config;

static u8 m_CommBuff[COM_DATA_SIZE];

static u16 m_CirWPinter = 0;

static u16 m_CirRPinter = 0;

static u8 s_RecDataPackFlag = FALSE;

static u16 s_RandTimeSlot;//在时间间隙取值范围内产生随机时隙

static u8 s_RegistID = 0;

static u8 s_Data_Interval = 10;

static u8 s_GetConfigFlag = FALSE;

static u8 s_LoraState = TRUE;

unsigned char GetLoraState(void)
{
	return s_LoraState;
}

void LoraObser(unsigned short nMain10ms);

const LORA_CONFIG DefaultLoraConfig = 
{
	0x0f,
	0xff,
	LORA_SPEED_INIT,
	0x13,
	LORA_OPTION,
};

void Clear_LoraCOMBuff(void)
{
	u16 i;
	
	for(i=0; i<COM_DATA_SIZE; i++)
	{
		m_CommBuff[i] = 0;		
	}
		m_CirWPinter = 0;

		m_CirRPinter = 0;	
}

void Lora_Power(FunctionalState NewState)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	
	if (NewState != DISABLE)
	{
		LORA_PWR_ON();
	}
	else
	{
		LORA_PWR_OFF();
		
		USART_DeInit(USART2);
		
		GPIO_InitStructure.GPIO_Pin = LORA_AUX_PIN;	//LORA  AUX  
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_Init(LORA_AUX_TYPE, &GPIO_InitStructure);	

		GPIO_InitStructure.GPIO_Pin = LORA_TX_PIN ; //LORA 	TX 
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
		GPIO_Init(LORA_UART_TYPE, &GPIO_InitStructure);	

		GPIO_InitStructure.GPIO_Pin = LORA_RX_PIN; //LORA  RX
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
		GPIO_Init(LORA_UART_TYPE, &GPIO_InitStructure);	

		GPIO_InitStructure.GPIO_Pin = LORA_M0_PIN;//LORA 	M0 
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
		GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);	

		GPIO_InitStructure.GPIO_Pin = LORA_M1_PIN ;//LORA  M1 
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
		GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);	

		GPIO_InitStructure.GPIO_Pin = LORA_PWR_PIN;   //LORA  PWR 
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
		GPIO_Init(LORA_PWR_TYPE, &GPIO_InitStructure);	
	}
}

void Lora_Send_Data(unsigned short int Tagetaddr, unsigned char channel, unsigned char *data, unsigned int len)
{
	u8 LoraPack[256];
	u8 Lora_lenth;
	
	LoraPack[0] = Tagetaddr>>8;
	LoraPack[1] = Tagetaddr;
	LoraPack[2] = channel;
	Lora_lenth = 3;
	memcpy(&LoraPack[Lora_lenth], data, len);
	
	Lora_lenth += len;
	
	Uart_Send_Data(LORA_COM, LoraPack, Lora_lenth);
}

void LoraPort_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB , ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = LORA_AUX_PIN;	//LORA  AUX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(LORA_AUX_TYPE, &GPIO_InitStructure);	
		
	LORA_SLEEP_MODE();
	
	GPIO_InitStructure.GPIO_Pin = LORA_M0_PIN;//LORA M0 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = LORA_M1_PIN ;//LORA  M1
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = LORA_PWR_PIN;   //LORA  PWR
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(LORA_PWR_TYPE, &GPIO_InitStructure);	
	

}

static u8 LoraInit()
{
	static u8 s_State = 0, Reset_LoraConfig_Flag = FALSE;
	static unsigned short s_LastTime = 0;
	u8 i, sendbuf[6];
	SYSTEMCONFIG *p_sys;
	const u8 CMD_C1[3] = {0xc1, 0xc1, 0xc1};
	const u8 CMD_C3[3] = {0xc3, 0xc3, 0xc3};
	const u8 CMD_C5[3] = {0xc5, 0xc5, 0xc5};
	const u8 CMD_F3[3] = {0xf3, 0xf3, 0xf3};
	
	p_sys = GetSystemConfig();	
	
	switch(s_State)
	{
		case 0:			
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 3) 
			{
				u1_printf("\r\n [Lora]Sleep Mode...\r\n");
				LORA_SLEEP_MODE();
				s_LastTime = GetSystem10msCount();
				s_State++;	
			}	
		break;
		case 1:
			u1_printf("\r\n [Lora]Software Version:");
			Uart_Send_Data(LORA_COM, (u8 *)CMD_F3, 3);

			while(1)
			{
				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;
					for(i=0; i<g_USART2_RX_CNT; i++)
						u1_printf("%c", g_USART2_RX_BUF[i]);
					g_USART2_RX_CNT = 0;
					break;
				}
				
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20) 
				{
					s_LastTime = GetSystem10msCount();					
					u1_printf("\r\n [Lora]获取版本超时\r\n");
					s_LoraState = FALSE;
					break;
				}
			
			}	
			s_State++;
			g_USART2_RX_CNT = 0;		
		break;
			
		case 2:
			u1_printf("\r\n [Lora]Hardware Version:");
			Uart_Send_Data(LORA_COM, (u8 *)CMD_C3, 3);
			s_LastTime = GetSystem10msCount();
			while(1)
			{
				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;
					for(i=0; i<g_USART2_RX_CNT; i++)
						u1_printf("%c", g_USART2_RX_BUF[i]);
					g_USART2_RX_CNT = 0;
					break;
				}
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20) 
				{
					s_LastTime = GetSystem10msCount();					
					u1_printf("\r\n [Lora]获取版本超时\r\n");
					s_LoraState = FALSE;
					break;
				}

			}	
			s_State++;
			g_USART2_RX_CNT = 0;

		break;
		case 3:
			u1_printf("\r\n [Lora]电源电压：");
			Uart_Send_Data(LORA_COM, (u8 *)CMD_C5, 3);
			s_LastTime = GetSystem10msCount();
			while(1)
			{
				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;
					u1_printf(" %1.3fV",(g_USART2_RX_BUF[1]*256 + g_USART2_RX_BUF[2])/1000.0);
					g_USART2_RX_CNT = 0;
					u1_printf("\r\n");		
					break;
				}
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20) 
				{
					s_LastTime = GetSystem10msCount();					
					u1_printf("\r\n [Lora]获取电源电压超时\r\n");
					s_LoraState = FALSE;
					break;
				}
			}	
			s_State++;
			g_USART2_RX_CNT = 0;
			delay_ms(80);
		break;

		case 4:	
			u1_printf("\r\n [Lora]配置：");
			Uart_Send_Data(LORA_COM, (u8 *)CMD_C1, 3);

			s_LastTime = GetSystem10msCount();
			while(1)
			{
				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;					
					for(i=1; i<g_USART2_RX_CNT; i++)
						u1_printf("%02X ", g_USART2_RX_BUF[i]);
					u1_printf("\r\n");
					g_USART2_RX_CNT = 0;
					if(((p_sys->Node)&0xffff) != ((g_USART2_RX_BUF[1]<<8) + g_USART2_RX_BUF[2]) || p_sys->Channel != g_USART2_RX_BUF[4] || g_USART2_RX_BUF[3] != LORA_SPEED_INIT || g_USART2_RX_BUF[5] != LORA_OPTION )					
					{
						Reset_LoraConfig_Flag = TRUE;
						u1_printf("\r\n 与设定值不符，重新配置LORA参数\r\n");
					}					
					else
					{
						s_LastTime = GetSystem10msCount();		
						LORA_WORK_MODE();
						s_LoraState = TRUE;
						s_State++;
						break;
					}
				}
						
					if(Reset_LoraConfig_Flag)
					{
						Reset_LoraConfig_Flag = FALSE;
						sendbuf[0] = 0xC0;
						sendbuf[1] = (p_sys->Node >> 8)&0xff;
						sendbuf[2] = p_sys->Node &0xff;
						sendbuf[3] = LORA_SPEED_INIT;
						if(p_sys->Channel <= 31)
							sendbuf[4] = p_sys->Channel;
						else
							sendbuf[4] = 15;
						sendbuf[5] = LORA_OPTION;		
						Uart_Send_Data(LORA_COM, sendbuf, sizeof(sendbuf));
						s_LastTime = GetSystem10msCount();	

						while(1)
						{
							if(g_Uart2RxFlag == TRUE)
							{
								g_Uart2RxFlag = FALSE;
								if(g_USART2_RX_BUF[0] == 'O' && g_USART2_RX_BUF[1] == 'K' )
								{
									
									s_State++;
									u1_printf("\r\n [Lora]重新配置Lora参数成功\r\n");		
									
									u1_printf("\r\n 等待重启...\r\n");
									delay_ms(100);
									while (DMA_GetCurrDataCounter(DMA1_Channel4));
									Sys_Soft_Reset();
								}	
								else
								{
									u1_printf("\r\n [Lora]重新配置Lora失败\r\n");	
									s_State++;
								}
								g_USART2_RX_CNT = 0;								
								break;
							}
							
							if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 100) 
							{
								s_LastTime = GetSystem10msCount();					
								u1_printf("\r\n [Lora]重新配置LORA超时，Lora初始化失败...\r\n");
								return TRUE;
							}
						}
						break;
					}

				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 30) 
				{
					s_LastTime = GetSystem10msCount();					
					u1_printf("\r\n [Lora]获取配置超时\r\n");
					s_LoraState = FALSE;
					g_USART2_RX_CNT = 0;
					s_State++;
					break;
				}
		
			}				
		break;
		case 5:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 5) 
			{
				s_LastTime = GetSystem10msCount();		
				u1_printf("\r\n [Lora]进入工作模式...\r\n");
				LORA_WORK_MODE();
				return TRUE;	
			}
		
		default:
			
		break;
	}
	return FALSE;
}


//系统功能: 上报心跳信息包======================================================
//void Lora_Send_Alive(void)
//{
//	u16 nMsgLen, i, CellVol;
//	unsigned char send_buf[64];
//	unsigned char AlivePack[64];
//	unsigned int len;
//	static uint16 seq_no = 0;
//	CLOUD_HDR *hdr;
//	SYSTEMCONFIG *p_sys;
//	
//	p_sys = GetSystemConfig();
//	hdr = (CLOUD_HDR *)&send_buf[0];
//	hdr->protocol = swap_word(500);							//通信协议版本号
//	hdr->device_id = swap_dword(p_sys->Device_ID);		//设备号
//	hdr->dir = 0;													//方向
//	hdr->seq_no = swap_word(seq_no++);								//序号

//	hdr->payload_len = swap_word(6);							//信息域长度
//	hdr->cmd = CMD_HEATBEAT;									//命令字

//	len = sizeof(CLOUD_HDR);									//未加校验的数据包长度

//	//if(g_sys_ctrl.sys_info )
//	//信息域
//	CellVol = (Get_Battery_Vol()*2/100);	
//	if(CellVol > 255)
//		CellVol = 255;
//	send_buf[len++] = 	(unsigned char)CellVol;	//电池电压，传输数据=电压*10;
//	
//	Int32ToArray(&send_buf[len], p_sys->Gateway);
//	len +=4;
//	send_buf[len++] = 0x55;	//收到网关数据时的信号强度

//	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
//	len++;
//	
//	nMsgLen = PackMsg(send_buf, len, AlivePack, 100);		//数据包转义处理	
//	u1_printf("\r\n [Lora][心跳包] -> %d(%d): ", p_sys->Gateway, nMsgLen);
//	for(i=0;i<nMsgLen;i++)
//	{
//		u1_printf("%02X ",AlivePack[i]);
//	}
//	u1_printf("\r\n");	

//	Lora_Send_Data(p_sys->Gateway, p_sys->Channel, AlivePack, nMsgLen);
//}

void Lora_Send_Register()
{
	u16 nMsgLen, i;
	unsigned char send_buf[64];
	unsigned char RegisterPack[64];
	unsigned int len;
	static uint16 seq_no = 0;
	CLOUD_HDR *hdr;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();
	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(500);							//通信协议版本号
	hdr->device_id = swap_dword(p_sys->Device_ID);		//设备号
	hdr->dir = 0;													//方向
	hdr->seq_no = swap_word(seq_no++);								//序号

	hdr->payload_len = swap_word(2);							//信息域长度
	hdr->cmd = CMD_REGISTER;									//命令字

	len = sizeof(CLOUD_HDR);
	
	send_buf[len++] = s_RandTimeSlot >> 8;
	send_buf[len++] = s_RandTimeSlot;
	
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;

	nMsgLen = PackMsg(send_buf, len, RegisterPack, 100);		//数据包转义处理
	
	u1_printf("\r\n [Lora][注册包] -> %d(%d): ", p_sys->Gateway, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",RegisterPack[i]);
	}
	u1_printf("\r\n");	
	Lora_Send_Data(p_sys->Gateway, p_sys->Channel, RegisterPack, nMsgLen);
}


unsigned int Lora_Report_LowPower_Data(void)
{
	u16 nMsgLen;
	static u16 seq;
	unsigned int send_len;
	unsigned int payload_len;
	u8 i, count, Packet[256], send_buf[256];

	CLOUD_HDR *hdr;
	unsigned char *ptr;
	uint16 val_u16, ch;
	float val_float;
	RTC_TimeTypeDef RTC_TimeStructure;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	
	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(PROTOCOL_CONTROL_ID);			//通信版本号
	hdr->device_id = swap_dword(p_sys->Device_ID);	  //设备号
	hdr->dir = 0;												  //方向
	hdr->seq_no = swap_word(seq);							  //包序号
	seq++;
	hdr->cmd = CMD_REPORT_D;									  //命令字

	ptr = (unsigned char *)&send_buf[sizeof(CLOUD_HDR)];
	ptr++;
	
	count = 4;
		
	ch = 1;
	val_u16 = swap_word(ch);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(KQW_ID);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_FLOAT;	//数据标识
	ptr++;
	val_float = encode_float(Get_Temp_Value());	//数据
	memcpy(ptr, (void *)&val_float, sizeof(float));
	ptr += sizeof(float);


	ch = 2;
	val_u16 = swap_word(ch);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(KQS_ID);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_FLOAT;	//数据标识
	ptr++;
	val_float = encode_float(Get_Humi_Value());	//数据
	memcpy(ptr, (void *)&val_float, sizeof(float));
	ptr += sizeof(float);
	
	ch = 3;
	val_u16 = swap_word(ch);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(GZD_ID);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_FLOAT;	//数据标识
	ptr++;
	val_float = encode_float(Get_Ill_Value());	//数据
	memcpy(ptr, (void *)&val_float, sizeof(float));
	ptr += sizeof(float);
	
	ch = 4;
	val_u16 = swap_word(ch);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(AIR_PRESS_ID);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_FLOAT;	//数据标识
	ptr++;
	val_float = encode_float(GetBMPPress());	//数据
	memcpy(ptr, (void *)&val_float, sizeof(float));
	ptr += sizeof(float);
	
	
	val_u16 = swap_word(count+1);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(BAT_VOL);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_UINT16;//数据标识
	ptr++;
	val_u16 = swap_word(Get_Battery_Vol());	//数据
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	count++;	
	
	send_buf[sizeof(CLOUD_HDR)] = count;
	send_len = ptr - send_buf;
    payload_len = send_len - sizeof(CLOUD_HDR);
	hdr->payload_len = swap_word(payload_len);
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,100);
	//发送消息

	Lora_Send_Data(p_sys->Gateway, p_sys->Channel, Packet, nMsgLen);
	
	u1_printf("\r\n 空气温度:%2.2f℃  湿度:%2.2f%%  光照度：%.1fLux   大气压力：%2.2fhPa 电池电压:%dmV\r\n", Get_Temp_Value(), Get_Humi_Value(), Get_Ill_Value(), GetBMPPress(), Get_Battery_Vol());
	
	u1_printf("\r\n [%02d:%02d:%02d][Lora][上报数据] %d -> %d(%d): ", RTC_TimeStructure.RTC_Hours,  RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, p_sys->Device_ID, p_sys->Gateway, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",Packet[i]);
	}
	u1_printf("\r\n");	
	return nMsgLen;	
}

void Lora_TEST_ACK()
{
	u16 nMsgLen, i;
	unsigned char send_buf[64];
	unsigned char AckPack[64];
	unsigned int len = 0;
	static uint16 seq_no = 0;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();
	
	send_buf[len++] = 0X01;
	send_buf[len++] = 0XF4;
	
	send_buf[len++] = p_sys->Device_ID >> 24;
	send_buf[len++] = p_sys->Device_ID >> 16;	
	send_buf[len++] = p_sys->Device_ID >> 8;
	send_buf[len++] = p_sys->Device_ID;	
	
	send_buf[len++]  = 0;
	
	send_buf[len++]  = seq_no >> 8;
	send_buf[len++]  = seq_no++;
	
	send_buf[len++]  = 0;
	send_buf[len++]  = 2;

	send_buf[len++] = CMD_TEST;									//命令字
	
	send_buf[len++] = 'O';
	send_buf[len++] = 'K';

	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, AckPack, 100);		//数据包转义处理	
	u1_printf("\r\n [Lora][测试指令应答OK] -> %d(%d): ", p_sys->Device_ID, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",AckPack[i]);
	}
	u1_printf("\r\n");	

	Lora_Send_Data(p_sys->Gateway, p_sys->Channel, AckPack, nMsgLen);
}

unsigned char JudgmentTimePoint(unsigned char Num)
{
	unsigned char Minute, Second;
	unsigned short TimePoint = 0;
	static unsigned int s_RTCTime = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	Minute = RTC_TimeStructure.RTC_Minutes%s_Data_Interval;
	Second = RTC_TimeStructure.RTC_Seconds;
	Num = Num%(s_Data_Interval*60/STOP_TIME);
	TimePoint = Num*STOP_TIME;
	
	if( (Minute*60 + Second >= TimePoint) && (Minute*60 + Second < TimePoint + STOP_TIME) && (DifferenceOfRTCTime(GetRTCSecond(), s_RTCTime) > 30) )
	{
		s_RTCTime = GetRTCSecond();
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

void LoraProcess(unsigned short nMain10ms)
{
	static u8 RunOnce = FALSE,  s_State = 1, s_RetryCount = 0;
	static u16 s_LastTime = 0, s_RebootCount = 0;
	unsigned char SleepTime = 0;

	RTC_TimeTypeDef RTC_TimeStructure;
		
	if(RunOnce == FALSE)
	{	
		RunOnce = TRUE;
		
		LoraPort_Init();
		
		LORA_SLEEP_MODE();
				
		LORA_PWR_ON();
		
		delay_ms(60);
		
		USART2_Config(9600);		
				
		u1_printf("\r\n Init LORA\r\n");
		while(LoraInit() == FALSE)		//初始化LORA
		{
			
		}
		
		USART2_Config(LORA_BAND_RATE);
		
		USART_ITConfig(USART2, USART_IT_IDLE, DISABLE);
		
		s_LastTime = GetSystem10msCount();
		
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
		srand(RTC_TimeStructure.RTC_Seconds+nMain10ms); 	
		s_RegistID = rand() % 10;		//初始化时产生0~9的随机序号
		u1_printf("\r\n Random ID:%d\r\n", s_RegistID);
		return;
	}
	
	LoraObser(GetSystem10msCount());
	
	
	switch(s_State)
	{
		case 1:
			s_State++;
		break;
		
		case 2:						//等待传感器检测完成
			if(g_PowerDetectFlag && g_SHTGetFlag && g_MAXGetFlag && g_BMPGetFlag)
			{
				if(GPIO_ReadInputDataBit(LORA_AUX_TYPE, LORA_AUX_PIN) == SET)
				{
					s_State++;
				}
				else
				{
					delay_ms(3);
					s_State++;
				}
			}
		break;
		
		case 3:							//上报数据
				Lora_Report_LowPower_Data();
				s_RecDataPackFlag = FALSE;
				s_State++;
				s_LastTime = GetSystem10msCount();		
		break;
			
		case 4:
			if(s_RecDataPackFlag)	//收到应答，进入休眠
			{
				RunLED();
				s_RecDataPackFlag = FALSE;
				s_State++;
				s_RebootCount = 0;
				s_RetryCount = 0;
			}
			else if((s_RetryCount == 0) && (CalculateTime(GetSystem10msCount(), s_LastTime) >= (50 + s_RegistID*2)))//第一次上报等待500ms + 0~500ms
			{
				s_RetryCount++;
				s_State--;		
			}
			else if((s_RetryCount == 1) && (CalculateTime(GetSystem10msCount(), s_LastTime) >= 15))//重发等待150ms
			{				
				s_RetryCount = 0;
				s_State++;
				LORA_SLEEP_MODE();
				s_RebootCount++;
				u1_printf("\r\n 2次上报失败,休眠. Count:%d\r\n", s_RebootCount);				
			}
		break;
			
		case 5:
			if(s_RebootCount >= 12)
			{
				s_RebootCount = 0;
				u1_printf(" 2小时没有收到应答,设备重启\r\n");
				delay_ms(100);
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				Sys_Soft_Reset();				
			}
			s_LastTime = GetSystem10msCount();
			if(g_PowerDetectFlag && g_SHTGetFlag && g_MAXGetFlag && g_BMPGetFlag)
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				SleepTime = STOP_TIME - RTC_TimeStructure.RTC_Seconds%10;
				SetStopModeTime(TRUE, SleepTime);	//停止模式入口
			}				
			s_State++;		
		break;	
		
		case 6:
			if(JudgmentTimePoint(s_RegistID))	//判断是否到达上报数据时间点
			{
				LoraPort_Init();
				Awake_USART2_Config();				
				LORA_SLEEP_MODE();
				LORA_PWR_ON();
		//		Clear_Flag();
				LORA_WORK_MODE();	
				s_State++;
				s_RetryCount = 0;
				s_LastTime = GetSystem10msCount();
			}	
			else
			{
				s_State--;
//				u1_printf(" %ds\r\n", RemainingTime);
			}
		break;
					
		case 7:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 5)	//10ms切换时间  节约电流
			{	
				s_State = 1;
			}
		break;
	}
}

void OnComm(u8 CommData)
{
	m_CommBuff[m_CirWPinter++] = CommData;
	
	if(m_CirWPinter >= COM_DATA_SIZE)
	{
		m_CirWPinter = 0;
	}
	if(m_CirWPinter == m_CirRPinter)
	{
		m_CirRPinter++;
		if(m_CirRPinter	>= COM_DATA_SIZE)
		{
		    m_CirRPinter = 0; 
		}
	}	
}

void OnRecLoraData(CLOUD_HDR *pMsg, u8 *data, u8 lenth)
{
	u8 sendbuf[100], sendlenth, ReturnVal = 0;
	RTC_TimeTypeDef RTC_TimeStructure;	

	CLOUD_HDR *hdr;
	static u8 s_buff[sizeof(CLOUD_HDR)];
		
	Clear_Uart2Buff();	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
	//解析协议包
//	u1_printf("\r\n [%02d:%02d:%02d][%d] <- 协议号:%02d 设备号:%04d 方向：%d 包序号:%d 包长度:%d 命令字:%02X 数据:", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, p_sys->Device_ID, pMsg->protocol, (int)pMsg->device_id, pMsg->dir, pMsg->seq_no, pMsg->payload_len, pMsg->cmd);
//	for(i=0; i<lenth; i++)
//		u1_printf("%02X ", data[i]);
//	u1_printf("\r\n");
	
	if(pMsg->cmd == CMD_REPORT_D)
	{
		s_RecDataPackFlag = TRUE;
		RTC_TimeStructure.RTC_Hours   = data[0];
		RTC_TimeStructure.RTC_Minutes = data[1];
		RTC_TimeStructure.RTC_Seconds = data[2];
		s_RegistID = data[3];			//上报时间节点
		s_Data_Interval = data[4];		//上报间隔
		
		if(s_Data_Interval < 1)
		{
			u1_printf("s_Data_Interval = %d, 最低上报间隔为2min...\r\n", s_Data_Interval);
			s_Data_Interval = 1;			
		}
		else if(s_Data_Interval > 120)
		{
			u1_printf("s_Data_Interval = %d, 最高上报间隔为120min...\r\n", s_Data_Interval);
			s_Data_Interval = 120;
		}

		
		if((data[0] == 255) || (data[1] == 255) || (data[2] == 255))
		{
			u1_printf(" 收到应答,网关未在线,不同步时间...\r\n");
		}
		else 
		{
			if(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure) != ERROR)
			{
	//			u1_printf(" 同步时间成功\r\n");
				
				if(s_GetConfigFlag == TRUE)
				{		
					pMsg = (CLOUD_HDR *)s_buff;
					System_get_config(LORA_COM, pMsg, data, lenth);
					delay_ms(10);
					s_GetConfigFlag = FALSE;
				}
				LORA_SLEEP_MODE();
				RTC_TimeShow();
			}
			else
			{
				u1_printf(" 同步时间失败\r\n");
			}
			
		}
		u1_printf("\r\n Time Point: %d,Data_Interval: %dmin\r\n", s_RegistID, s_Data_Interval);
		return;
	}
	
	if(pMsg->cmd == CMD_GET_CONFIG)//获取设备配置
	{
		u1_printf(" 获取设备配置..\r\n");
		s_GetConfigFlag = TRUE;
		
		memcpy(s_buff, pMsg, sizeof(CLOUD_HDR));
//		System_get_config(LORA_COM, pMsg, data, lenth);
		return;
	}
	
	if(pMsg->cmd == CMD_SET_CONFIG) //设置设备配置
	{
		memcpy(sendbuf, pMsg, sizeof(CLOUD_HDR));
		sendlenth = sizeof(CLOUD_HDR);
		
		hdr = (CLOUD_HDR *)sendbuf;
		hdr->protocol = swap_word(hdr->protocol);	
		hdr->device_id = swap_dword(hdr->device_id);
		hdr->seq_no = swap_word(hdr->seq_no);	
		hdr->dir = 1;
		hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+4);
		memcpy(&sendbuf[sizeof(CLOUD_HDR)], data, lenth);
		ReturnVal = System_set_config(LORA_COM, sendbuf, lenth + sendlenth);
		if(ReturnVal == 0)
		{
			return;
		}
		u1_printf("更新设备参数，等待设备重启\r\n");
		delay_ms(500);
		while (DMA_GetCurrDataCounter(DMA1_Channel4));
		Sys_Soft_Reset();
	}
}
static void LoraObser(unsigned short nMain10ms)
{
	static u8 FrameID = 0, special_flag = FALSE, s_HeadFlag = FALSE;
	static u16 data_lenth = 0, s_LastTime = 0, i = 0;
	static CLOUD_HDR m_RxFrame;
	static u8 RxData, databuff[100];
	u8 checksum;
	
	if(s_HeadFlag == TRUE)
	{
		if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20)
		{
			if(g_USART2_RX_BUF[0] == 0x7e && g_USART2_RX_BUF[g_USART2_RX_CNT - 1] == 0x21)
			{
				OnRecLoraData(&m_RxFrame, databuff, data_lenth);	
				u1_printf("\r\n+++++++Lora\r\n");
			}
			else
			{
				u1_printf("\r\n LORA接收超时,收到%d个数据\r\n", g_USART2_RX_CNT);
				
				u1_printf("Rec:  ");
				for(i=0; i<g_USART2_RX_CNT; i++)
				{
					u1_printf("%02X ", g_USART2_RX_BUF[i]);
				}
				u1_printf("\r\n");
			}
			
			Clear_Uart2Buff();
			Clear_LoraCOMBuff();
			memset(databuff, 0, 100);
			data_lenth = 0;
			i = 0;
			FrameID = 0;
			return ;
		}
	}
		

	
	if(m_CirRPinter != m_CirWPinter)
	{
		RxData = m_CommBuff[m_CirRPinter++];
		if(m_CirRPinter >= COM_DATA_SIZE)
		{
			m_CirRPinter = 0;
		}
		if(RxData == 0x21 && FrameID != 15)
		{
			u1_printf("\r\n *************收到不完整的数据包，清空缓存**************\r\n");
			Clear_Uart2Buff();
			Clear_LoraCOMBuff();
			memset(databuff, 0, 100);
			data_lenth = 0;
			i = 0;
			FrameID = 0;
			return ;
		}
		if(RxData == 0x7d && special_flag == FALSE)
		{
			special_flag = TRUE;
			return;
		}
		if(special_flag == TRUE)
		{
			special_flag = FALSE;
			if(RxData == 0x5d)
			{
				RxData = 0x7d;
			}
			else if(RxData == 0x5e)
			{
				RxData = 0x7e;
			}
			else if(RxData == 0x51)
			{
				RxData = 0x21;
			}
		}

		switch(FrameID)
		{
			case 0:
				if(RxData == 0X7e)
				{
					s_HeadFlag = TRUE;
					s_LastTime = GetSystem10msCount();
					FrameID++;
				}
				break;
			case 1:
				m_RxFrame.protocol = RxData;
				FrameID++;
			break;
			case 2:
				m_RxFrame.protocol = (m_RxFrame.protocol << 8) | RxData;
				FrameID++;
			break;
			case 3:
				m_RxFrame.device_id = RxData;
				FrameID++;
			break;
			case 4:
				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
				FrameID++;
			break;
			case 5:
				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
				FrameID++;
			break;
			case 6:
				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
				FrameID++;
			
			break;
			case 7:
				m_RxFrame.dir = RxData;
				if(!(RxData != 0 || RxData != 1))
				{
					u1_printf("\r\n *************LORA接收方向包错误*********** %d\r\n", RxData );
					Clear_Uart2Buff();
					Clear_LoraCOMBuff();
					s_HeadFlag = FALSE;
					memset(databuff, 0, 100);
					data_lenth = 0;
					i = 0;
					FrameID = 0;
				}
				FrameID++;
			break;
			case 8:
				m_RxFrame.seq_no = RxData;
				FrameID++;
			break;
			case 9:
				m_RxFrame.seq_no = (m_RxFrame.seq_no << 8) | RxData;
				FrameID++;
			break;
			case 10:
				m_RxFrame.payload_len = RxData;
				FrameID++;
			break;
			case 11:
				m_RxFrame.payload_len = (m_RxFrame.payload_len << 8) | RxData;
				data_lenth = m_RxFrame.payload_len;
				FrameID++;
			break;
			case 12:
				m_RxFrame.cmd = RxData;				
				i = 0;
			    memset(databuff, 0, 100);
				FrameID++;
				if(data_lenth == 0)
				{
					FrameID = 14;
					break;
				}	
				else if(data_lenth >= 100)
				{
					u1_printf("\r\n *************数据长度错误*********** %d\r\n", data_lenth );
					Clear_Uart2Buff();
					Clear_LoraCOMBuff();
					s_HeadFlag = FALSE;
					memset(databuff, 0, 100);
					data_lenth = 0;
					i = 0;
					FrameID = 0;
				}
			break;
			case 13:				
				databuff[i++] = RxData;
				if(i >= data_lenth)
				{
					FrameID++;
				}
			break;		
			case 14:
				checksum = RxData;
				if(CheckHdrSum(&m_RxFrame, databuff, data_lenth) != checksum)
				{
					u1_printf("[LORA]Check error...");
					u1_printf("Rec:%02X, CheckSum:%02X\r\n", checksum, CheckHdrSum(&m_RxFrame, databuff, data_lenth));
					FrameID = 0;	
					s_HeadFlag = FALSE;					
					Clear_LoraCOMBuff();
					s_HeadFlag = FALSE;		
					memset(databuff, 0, 100);
					data_lenth = 0;
					i = 0;
				}else
				FrameID++;
			break;
			case 15:
				if(RxData == 0x21)	
				{								
					OnRecLoraData(&m_RxFrame, databuff, data_lenth);
				}
				else
				{				
					u1_printf("end error!\r\n");
				}
				s_HeadFlag = FALSE;		
				Clear_Uart2Buff();
				memset(databuff, 0, 100);
				data_lenth = 0;
				i = 0;
			    FrameID = 0;				
			break;
			default:
				s_HeadFlag = FALSE;		
				Clear_Uart2Buff();
				memset(databuff, 0, 100);
				data_lenth = 0;
				i = 0;
			    FrameID = 0;	 
			break;	
		}	
	}	
}

