#ifndef	_ADC_H_
#define	_ADC_H_

#define		MAX_ADC_CHANNEL		12
#define		ADC_BUFF_COUNT		50		//每个通道采集次数

#define 	CUR1_ADC_PIN			GPIO_Pin_0//	Channel 8
#define		CUR1_ADC_TYPE			GPIOB

#define 	CUR2_ADC_PIN			GPIO_Pin_6//	Channel 6
#define		CUR2_ADC_TYPE			GPIOA

#define 	CUR3_ADC_PIN			GPIO_Pin_4//	Channel 4
#define		CUR3_ADC_TYPE			GPIOA

#define 	CUR4_ADC_PIN			GPIO_Pin_13//	Channel 19
#define		CUR4_ADC_TYPE			GPIOB

#define 	CUR5_ADC_PIN			GPIO_Pin_0//	Channel 0
#define		CUR5_ADC_TYPE			GPIOA

#define 	CUR6_ADC_PIN			GPIO_Pin_2//	Channel 2
#define		CUR6_ADC_TYPE			GPIOA

#define 	BAT1_VOL_PIN			GPIO_Pin_1//	Channel 9
#define		BAT1_VOL_TYPE			GPIOB

#define 	BAT2_VOL_PIN			GPIO_Pin_7//	Channel 7
#define		BAT2_VOL_TYPE			GPIOA

#define 	BAT3_VOL_PIN			GPIO_Pin_5//	Channel 5
#define		BAT3_VOL_TYPE			GPIOA

#define 	BAT4_VOL_PIN			GPIO_Pin_14//	Channel 20
#define		BAT4_VOL_TYPE			GPIOB

#define 	BAT5_VOL_PIN			GPIO_Pin_1//	Channel 1
#define		BAT5_VOL_TYPE			GPIOA

#define 	BAT6_VOL_PIN			GPIO_Pin_3//	Channel 3
#define		BAT6_VOL_TYPE			GPIOA


void Adc_Init(void);

void Open_AdcChannel(void);

void Adc_Reset(void);

unsigned short GetADCValue(unsigned char Num, unsigned char Channel);





#endif



