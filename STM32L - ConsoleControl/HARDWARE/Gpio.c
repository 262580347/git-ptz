#include "main.h"
#include "Gpio.h"

#define		RUN_LED_GPIO_PIN		GPIO_Pin_1
#define		RUN_LED_GPIO_TYPE		GPIOA



#define		LED_DEFAULT_TIME		50000

void LEDNoWork(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;	
	
	GPIO_InitStructure.GPIO_Pin = RUN_LED_GPIO_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(RUN_LED_GPIO_TYPE, &GPIO_InitStructure);

	GPIO_ResetBits(RUN_LED_GPIO_TYPE,RUN_LED_GPIO_PIN);

}
static void TaskForRunLED(void)
{
	static u8 s_LastStatus = 0;
	
	if(s_LastStatus != GetMotorStatus())
	{
		s_LastStatus = GetMotorStatus();
		if(s_LastStatus == MOTOR_STOP)
		{
			Task_SetTime(TaskForRunLED, LED_DEFAULT_TIME);
		}
		else
		{
			Task_SetTime(TaskForRunLED, 5000);
		}
	}
	
	GPIO_ToggleBits(RUN_LED_GPIO_TYPE,RUN_LED_GPIO_PIN);	

}

void STM32_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOH, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOC, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = RUN_LED_GPIO_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(RUN_LED_GPIO_TYPE, &GPIO_InitStructure);
	/* Force a low level on LEDs*/ 	
	GPIO_ResetBits(RUN_LED_GPIO_TYPE,RUN_LED_GPIO_PIN);
	
	Task_Create(TaskForRunLED, LED_DEFAULT_TIME);
}

void TaskForLEDBlinkRun(void)
{
	Task_Create(TaskForRunLED, LED_DEFAULT_TIME);
}

void TaskForLEDBlinkEnd(void)
{
	Task_Kill(TaskForRunLED);
}

