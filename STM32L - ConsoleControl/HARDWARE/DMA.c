
#include "main.h"
#include "DMA.h"

__align(8) u8 USART1_TX_BUF[350]; 	      //发送缓冲,最大256字节
__align(8) u8 USART2_TX_BUF[400]; 	      //发送缓冲,最大100
__align(8) u8 USART3_TX_BUF[50]; 	      //发送缓冲,最大USART3_MAX_SEND_LEN字节,cJSON协议包较长，注意是否溢出

void DMA_Config(void)
{
	DMA_InitTypeDef           DMA_InitStructure;
	/* Enable DMA1 clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	DMA_DeInit(DMA1_Channel4);
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&USART1->DR;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&USART1_TX_BUF;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStructure.DMA_BufferSize = 0;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel4, &DMA_InitStructure);

	USART_DMACmd(USART1,USART_DMAReq_Tx,ENABLE); 
}

void DMA_Usart2_Config(void)
{
	DMA_InitTypeDef           DMA_InitStructure;
	/* Enable DMA1 clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	
	DMA_DeInit(DMA1_Channel7);
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&USART2->DR;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&USART2_TX_BUF;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStructure.DMA_BufferSize = 0;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel7, &DMA_InitStructure);

	USART_DMACmd(USART2,USART_DMAReq_Tx,ENABLE); 	
}

void DMA_Usart3_Config(void)
{
	DMA_InitTypeDef           DMA_InitStructure;
	/* Enable DMA1 clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	
	DMA_DeInit(DMA1_Channel2);
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&USART3->DR;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&USART3_TX_BUF;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStructure.DMA_BufferSize = 0;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel2, &DMA_InitStructure);

	USART_DMACmd(USART3,USART_DMAReq_Tx,ENABLE); 	
}
void DMAUsart1RxConfig(void)
{
	DMA_InitTypeDef           DMA_InitStructure;
	/* Enable DMA1 clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	DMA_DeInit(DMA1_Channel5);
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&USART1->DR;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&g_USART_RX_BUF;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructure.DMA_BufferSize = USART_REC_LEN;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel5, &DMA_InitStructure);
	/* Enable DMA1 Channel5 */ 
	DMA_Cmd(DMA1_Channel5, ENABLE);	
	USART_DMACmd(USART1,USART_DMAReq_Rx,ENABLE); 
}

void DMAUsart2RxConfig(void)
{
	DMA_InitTypeDef           DMA_InitStructure;
	/* Enable DMA1 clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	DMA_DeInit(DMA1_Channel6);
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&USART2->DR;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&g_USART2_RX_BUF;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructure.DMA_BufferSize = USART2_REC_LEN;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel6, &DMA_InitStructure);
	/* Enable DMA1 Channel3 */ 
	DMA_Cmd(DMA1_Channel6, ENABLE);	
	USART_DMACmd(USART2,USART_DMAReq_Rx,ENABLE);
}

void DMAUsart3RxConfig(void)
{
	DMA_InitTypeDef           DMA_InitStructure;
	/* Enable DMA1 clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	DMA_DeInit(DMA1_Channel3);
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&USART3->DR;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&g_USART3_RX_BUF;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructure.DMA_BufferSize = USART3_REC_LEN;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel3, &DMA_InitStructure);
	/* Enable DMA1 Channel3 */ 
	DMA_Cmd(DMA1_Channel3, ENABLE);	
	USART_DMACmd(USART3,USART_DMAReq_Rx,ENABLE);
}

void u1_printf(char* fmt,...)  
{  
	u16 i;
	va_list ap;
	
	while (DMA_GetCurrDataCounter(DMA1_Channel4));
	if(DMA_GetFlagStatus(DMA1_FLAG_TC4) == SET)
		DMA_ClearFlag(DMA1_FLAG_TC4);
	DMA_Cmd(DMA1_Channel4, DISABLE );  //关闭USART1 TX DMA1 所指示的通道      
	memset(USART1_TX_BUF, 0, sizeof(USART1_TX_BUF));
	va_start(ap,fmt);
	vsprintf((char*)USART1_TX_BUF,fmt,ap);
	va_end(ap);
	i=strlen((const char*)USART1_TX_BUF);//此次发送数据的长度	
 
	DMA_SetCurrDataCounter(DMA1_Channel4,i);//DMA通道的DMA缓存的大小
	
	DMA_Cmd(DMA1_Channel4, ENABLE);  //使能USART1 TX DMA1 所指示的通道 
}
void USART1_DMA_Send(unsigned char *buffer, unsigned short buffer_size)
{
	while (DMA_GetCurrDataCounter(DMA1_Channel4));
	if(DMA_GetFlagStatus(DMA1_FLAG_TC4) == SET)
		DMA_ClearFlag(DMA1_FLAG_TC4);
	DMA_Cmd(DMA1_Channel4, DISABLE );  //关闭USART1 TX DMA1 所指示的通道      

	memcpy(&USART1_TX_BUF[0], buffer, buffer_size);
 
	DMA_SetCurrDataCounter(DMA1_Channel4,buffer_size);
	DMA_Cmd(DMA1_Channel4, ENABLE);
}
//串口2,printf 函数
//确保一次发送数据不超过USART2_MAX_SEND_LEN字节
void u2_printf(char* fmt,...)  
{  
	u16 i;
	va_list ap;
	
	while (DMA_GetCurrDataCounter(DMA1_Channel7));
	if(DMA_GetFlagStatus(DMA1_FLAG_TC7) == SET)
		DMA_ClearFlag(DMA1_FLAG_TC7);
	DMA_Cmd(DMA1_Channel7, DISABLE );  //关闭USART3 TX DMA1 所指示的通道      
	memset(USART2_TX_BUF, 0, sizeof(USART2_TX_BUF));
	va_start(ap,fmt);
	vsprintf((char*)USART2_TX_BUF,fmt,ap);
	va_end(ap);
	i=strlen((const char*)USART2_TX_BUF);//此次发送数据的长度	
 
	DMA_SetCurrDataCounter(DMA1_Channel7,i);//DMA通道的DMA缓存的大小
	
	DMA_Cmd(DMA1_Channel7, ENABLE);  //使能USART3 TX DMA1 所指示的通道 
}


void USART2_DMA_Send(unsigned char *buffer, unsigned short buffer_size)
{
	while (DMA_GetCurrDataCounter(DMA1_Channel7));
	if(DMA_GetFlagStatus(DMA1_FLAG_TC7) == SET)
		DMA_ClearFlag(DMA1_FLAG_TC7);
	DMA_Cmd(DMA1_Channel7, DISABLE );  //关闭USART1 TX DMA1 所指示的通道      

	memcpy(&USART2_TX_BUF[0], buffer, buffer_size);
 
	DMA_SetCurrDataCounter(DMA1_Channel7,buffer_size);
	DMA_Cmd(DMA1_Channel7, ENABLE);
}

//串口3,printf 函数
//确保一次发送数据不超过USART3_MAX_SEND_LEN字节
void u3_printf(char* fmt,...)  
{  
	u16 i;
	va_list ap;
	
	while (DMA_GetCurrDataCounter(DMA1_Channel2));
	if(DMA_GetFlagStatus(DMA1_FLAG_TC2) == SET)
		DMA_ClearFlag(DMA1_FLAG_TC2);
	DMA_Cmd(DMA1_Channel2, DISABLE );  //关闭USART3 TX DMA1 所指示的通道      
	
	va_start(ap,fmt);
	vsprintf((char*)USART3_TX_BUF,fmt,ap);
	va_end(ap);
	i=strlen((const char*)USART3_TX_BUF);//此次发送数据的长度	
 
	DMA_SetCurrDataCounter(DMA1_Channel2,i);//DMA通道的DMA缓存的大小
	
	DMA_Cmd(DMA1_Channel2, ENABLE);  //使能USART3 TX DMA1 所指示的通道 
}

void USART3_DMA_Send(unsigned char *buffer, unsigned short buffer_size)
{
	while (DMA_GetCurrDataCounter(DMA1_Channel2));
	if(DMA_GetFlagStatus(DMA1_FLAG_TC2) == SET)
		DMA_ClearFlag(DMA1_FLAG_TC2);
	DMA_Cmd(DMA1_Channel2, DISABLE );  //关闭USART1 TX DMA1 所指示的通道      

	memcpy(&USART3_TX_BUF[0], buffer, buffer_size);
 
	DMA_SetCurrDataCounter(DMA1_Channel2,buffer_size);
	DMA_Cmd(DMA1_Channel2, ENABLE);
}
