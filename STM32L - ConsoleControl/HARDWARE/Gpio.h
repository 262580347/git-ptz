#ifndef __LED_H
#define __LED_H	 

void LEDNoWork(void);

void STM32_GPIO_Init(void);

void RunLED(void);

void WakeUp_GPIO_Init(void);

void Stop_GPIO_Init(void);

void TaskForLEDBlinkRun(void);

void TaskForLEDBlinkEnd(void);

#endif
