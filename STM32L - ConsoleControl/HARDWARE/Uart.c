#include "Uart.h"
#include "main.h"

#define DEBUG_UART1      //调试使用的COM端口号

u8 		g_USART_RX_BUF[USART_REC_LEN] ;
u16 	g_USART_RX_CNT=0;	
u8 		g_UartRxFlag = FALSE;

u8 		g_USART2_RX_BUF[USART2_REC_LEN] ;
u16 	g_USART2_RX_CNT=0;	
u8 		g_Uart2RxFlag = FALSE;

u8 		g_USART3_RX_BUF[USART3_REC_LEN] ;
u16 	g_USART3_RX_CNT=0;	
u8 		g_Uart3RxFlag = FALSE;
#if 1
#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE 
{ 
	int handle; 

}; 

FILE __stdout;       
//定义_sys_exit()以避免使用半主机模式    
_sys_exit(int x) 
{ 
	x = x; 
} 
//重定义fputc函数 
int fputc(int ch, FILE *f)
{      
	#ifdef DEBUG_UART1
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
    USART1->DR = (u8) ch;    
	#endif
	#ifdef DEBUG_UART2
	while((USART2->SR&0X40)==0);//循环发送,直到发送完毕   
    USART2->DR = (u8) ch;    
	#endif
	#ifdef DEBUG_UART3
	while((USART3->SR&0X40)==0);//循环发送,直到发送完毕   
    LORA_COM->DR = (u8) ch;    
	#endif
	#ifdef DEBUG_UART4
	while((UART4->SR&0X40)==0);//循环发送,直到发送完毕   
    UART4->DR = (u8) ch;    
	#endif
	#ifdef DEBUG_UART5
	while((UART5->SR&0X40)==0);//循环发送,直到发送完毕   
    UART5->DR = (u8) ch;    
	#endif
	return ch;
}
#endif 

void Clear_Uart1Buff(void)
{
	memset(g_USART_RX_BUF, 0,USART_REC_LEN);
	g_USART_RX_CNT = 0;
	g_UartRxFlag = FALSE;
}

void Clear_Uart2Buff(void)
{
	memset(g_USART2_RX_BUF, 0,USART2_REC_LEN);
	g_USART2_RX_CNT = 0;
	g_Uart2RxFlag = FALSE;
}

void Clear_Uart3Buff(void)
{
	memset(g_USART3_RX_BUF, 0,USART3_REC_LEN);
	g_USART3_RX_CNT = 0;
	g_Uart3RxFlag = FALSE;
}
void USART1_Config(unsigned int bound)
{
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	/* Enable GPIO clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

	/* Enable USART clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	/* Connect PXx to USARTx_Tx */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
	//connect PA.10 to usart1's rx
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);

	/* Configure USART Tx and Rx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	USART_InitStructure.USART_BaudRate = bound;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);
	
//	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);
	/* Enable USART */
	USART_Cmd(USART1, ENABLE);
	
	DMA_Config();
	
	DMAUsart1RxConfig();
	
	Task_Create(TaskForDebugCOM, 1);
}

void Uart3ChannelSet(COM3_CHANNEL Com_Channel)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = USART3_CHANNEL_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(USART3_CHANNEL_TYPE, &GPIO_InitStructure);	
	
	if(Com_Channel == CHANNEL_MOTOR)
	{
		GPIO_ResetBits(USART3_CHANNEL_TYPE,USART3_CHANNEL_PIN);	
	}
	else if(Com_Channel == CHANNEL_GPS)
	{
		GPIO_SetBits(USART3_CHANNEL_TYPE,USART3_CHANNEL_PIN);	
	}
}

void USART2_Config(unsigned int bound)
{
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	/* Enable GPIO clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

	/* Enable USART clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	/* Connect PXx to USARTx_Tx */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
	//connect PA.10 to usart1's rx
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);

	/* Configure USART Tx and Rx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	USART_InitStructure.USART_BaudRate = bound;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART2, &USART_InitStructure);
	
//	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	USART_ITConfig(USART2, USART_IT_IDLE, ENABLE);

	DMA_Usart2_Config();
	
	DMAUsart2RxConfig();
	/* Enable USART */
	USART_Cmd(USART2, ENABLE);
}

void USART3_Config(unsigned int bound)
{
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	/* Enable GPIO clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);

	/* Enable USART clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

	/* Connect PXx to USARTx_Tx */
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_USART3);
	//connect PA.10 to usart1's rx
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_USART3);

	/* Configure USART Tx and Rx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	USART_InitStructure.USART_BaudRate = bound;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART3, &USART_InitStructure);
	
//	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
	USART_ITConfig(USART3, USART_IT_IDLE, ENABLE);
	
	DMA_Usart3_Config();
	
	DMAUsart3RxConfig();
	/* Enable USART */
	USART_Cmd(USART3, ENABLE);
}

void Awake_USART2_Config(void)
{
	RCC->APB1ENR |= 0x00020000;
	
	GPIOA->AFR[0] &= 0xFF0F;
	
	GPIOA->MODER &= 0xFFFFFF0F;
	GPIOA->MODER |= 0x000000A0;

	GPIOA->OSPEEDR &= 0xFFFFFF0F;
	GPIOA->OSPEEDR |= 0x000000F0;

	GPIOA->OTYPER &= 0xFFF3;///

	GPIOA->PUPDR &= 0xFFFFFF0F;
	GPIOA->PUPDR |= 0x00000050;
	
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	
	USART2->CR1 |= 0x202C;	
	USART2->CR2 &= 0;	
	USART2->CR3 &= 0;
	
	USART2->BRR = 0x0024 ;
}
void Uart_Send_Char(USART_TypeDef* USARTx, u8 ch)
{ 
    while(USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET);
	USART_SendData(USARTx, ch); 
}
void Uart_Send_Data(USART_TypeDef* USARTx, u8 *data, u8 len)
{
	while(len--)
	{
		while(USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET);
		USART_SendData(USARTx, *(data++)); 
	}
}	

void Uart_Send_Str(USART_TypeDef* USARTx, char *data)
{
	u8 len;
	
	len = strlen((const char *)data);
	while(len--)
	{
		while(USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET);
		USART_SendData(USARTx, *(data++)); 
	}
}	

void USART1_IRQHandler(void)                
{
	if(USART_GetITStatus(USART1,USART_IT_IDLE) == SET)
	{
		USART1->SR;
		USART1->DR; //Clear Flag
		DMA_Cmd(DMA1_Channel5,DISABLE);
		
		g_USART_RX_CNT = USART_REC_LEN - DMA_GetCurrDataCounter(DMA1_Channel5);
		g_USART_RX_BUF[g_USART_RX_CNT] = '\0';		
		g_UartRxFlag = TRUE;
		DMA_SetCurrDataCounter(DMA1_Channel5,USART_REC_LEN);
		DMA_Cmd(DMA1_Channel5,ENABLE);  //??DMA??
	}		
} 

void USART2_IRQHandler(void)            
{
	if(USART_GetITStatus(USART2,USART_IT_IDLE) == SET)
	{
		USART2->SR;
		USART2->DR; //Clear Flag
		DMA_Cmd(DMA1_Channel6,DISABLE);
		
		g_USART2_RX_CNT = USART2_REC_LEN - DMA_GetCurrDataCounter(DMA1_Channel6);
		g_USART2_RX_BUF[g_USART2_RX_CNT] = '\0';		
		g_Uart2RxFlag = TRUE;
		DMA_SetCurrDataCounter(DMA1_Channel6,USART2_REC_LEN);
		DMA_Cmd(DMA1_Channel6,ENABLE);  //??DMA??
	}	

} 


void USART3_IRQHandler(void)            
{
	if(USART_GetITStatus(USART3,USART_IT_IDLE) == SET)
	{
		USART3->SR;
		USART3->DR; //Clear Flag
		DMA_Cmd(DMA1_Channel3,DISABLE);
		
		g_USART3_RX_CNT = USART3_REC_LEN - DMA_GetCurrDataCounter(DMA1_Channel3);
		g_USART3_RX_BUF[g_USART3_RX_CNT] = '\0';		
		g_Uart3RxFlag = TRUE;
		DMA_SetCurrDataCounter(DMA1_Channel3,USART3_REC_LEN);
		DMA_Cmd(DMA1_Channel3,ENABLE);  //??DMA??
	}	
} 

