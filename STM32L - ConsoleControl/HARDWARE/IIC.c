#include "main.h"
#include "IIC.h"

#define	SDA_PIN		GPIO_Pin_0
#define	SDA_TYPE	GPIOA
#define	SCK_PIN		GPIO_Pin_1
#define	SCK_TYPE	GPIOA


#define SDA_IN()  											\
{															\
	GPIO_InitTypeDef  GPIO_InitStructure;  					\
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					\
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;			\
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			\
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;		\
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		\
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);				\
}
#define SDA_OUT()   										\
{															\
	GPIO_InitTypeDef  GPIO_InitStructure;  					\
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					\
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			\
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;			\
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;		\
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		\
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);				\
}


#define IIC_SCK_HIGH()    	GPIO_SetBits(SCK_TYPE, SCK_PIN)
#define IIC_SCK_LOW() 		GPIO_ResetBits(SCK_TYPE, SCK_PIN)
#define IIC_SDA_HIGH()   	GPIO_SetBits(SDA_TYPE, SDA_PIN)
#define IIC_SDA_LOW() 		GPIO_ResetBits(SDA_TYPE, SDA_PIN)
#define READ_SDA()   		GPIO_ReadInputDataBit(SDA_TYPE, SDA_PIN)

#define	IIC_DELAY()	delay_us(4)

void IIC_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;  	
	
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);		

	GPIO_InitStructure.GPIO_Pin = SCK_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SCK_TYPE, &GPIO_InitStructure);	

	GPIO_SetBits(SDA_TYPE, SDA_PIN);
	GPIO_SetBits(SCK_TYPE, SCK_PIN);
}

void IIC_Start(void)
{
	SDA_OUT();
	IIC_SDA_HIGH();
	IIC_SCK_HIGH();
	IIC_DELAY();
	IIC_SDA_LOW();
	IIC_DELAY();
	IIC_SCK_LOW();
}

void IIC_Stop(void)
{
	SDA_OUT();
	IIC_SCK_LOW();
	IIC_SDA_LOW();
	IIC_DELAY();
	IIC_SCK_HIGH();
	IIC_DELAY();
	IIC_SDA_HIGH();
	IIC_DELAY();
}

static void IIC_ACK(void)
{
	IIC_SCK_LOW();
	SDA_OUT();
	IIC_SDA_LOW();
	IIC_DELAY();
	IIC_SCK_HIGH();
	IIC_DELAY();
	IIC_SCK_LOW();
}

static void IIC_NoACK(void)
{
	IIC_SCK_LOW();	
	SDA_OUT();
	IIC_SDA_HIGH();
	IIC_DELAY();
	IIC_SCK_HIGH();
	IIC_DELAY();
	IIC_SCK_LOW();
}

unsigned char IIC_WaitACK(void)
{
	unsigned char ucErrTime=0;
	SDA_IN();
	IIC_SDA_HIGH();	
	IIC_DELAY();	
	IIC_SCK_HIGH();
	IIC_DELAY();
	while(READ_SDA())
	{
		ucErrTime++;
		if(ucErrTime >250)
		{
			IIC_Stop();
			return 1;
		}
	}
	IIC_SCK_LOW();
	return 0;
}

void IIC_WriteByte(unsigned char data)
{
	unsigned char i;
	
	SDA_OUT();
	IIC_SCK_LOW();
	for(i=0; i<8; i++)
	{
		if(data & 0x80)
			IIC_SDA_HIGH();
		else
			IIC_SDA_LOW();
		data <<= 1;
		IIC_DELAY();
		IIC_SCK_HIGH();
		IIC_DELAY();
		IIC_SCK_LOW();
		IIC_DELAY();
	}	
}

unsigned char IIC_ReadByte(unsigned char IsAck)
{
	unsigned char i, Receive = 0;
	
	SDA_IN();
	for(i=0; i<8; i++)
	{
		IIC_SCK_LOW();
		IIC_DELAY();
		IIC_SCK_HIGH();
		IIC_DELAY();
		Receive <<= 1;
		if(READ_SDA())
			Receive++;
		IIC_DELAY();
	}
	if(IsAck)
		IIC_ACK();
	else
		IIC_NoACK();
	
	return Receive;
}	
