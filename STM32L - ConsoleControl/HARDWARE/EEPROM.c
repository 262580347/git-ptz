/**********************************
说明:STM32配置信息由FLASH迁移至EEPROM
	  EEPROM共4096字节，分为64个Block，每个Block有64字节
	  可供64种类别的参数使用
	  EEPROM地址为0x08080000	~	0x08080FFF	
作者:关宇晟
版本:V2019.5.7
***********************************/
#include "main.h"
#include "EEPROM.h"

#define 	FLASH_WAITETIME  	50000          //FLASH等待超时时间
#define		DATA_LENGTH_MAX 	128

//从EEPROM按字节读取
void EEPROM_ReadBytes(unsigned int Addr,unsigned char *Buffer,unsigned short Length)
{
	while(Length--)
	{
		*Buffer++ =*(__IO uint8_t *)Addr;
		Addr++;
	}	
}
//从EEPROM按字读取
void EEPROM_ReadWords(unsigned int Addr,unsigned int *Buffer,unsigned short Length)
{	
	while(Length--)
	{
		*Buffer++ =*(__IO uint32_t *)Addr;
		Addr += 4;
	}	
}

//向EEPROM按字节写入
void EEPROM_WriteBytes(unsigned int Addr,unsigned char *Buffer,unsigned short Length)
{
	FLASH_Status FlashSta;

	DIS_INT;
	DATA_EEPROM_Unlock();
	
	while(Length > 0)
	{
		while(FLASH_WaitForLastOperation(FLASH_WAITETIME) != FLASH_COMPLETE);
		
		FlashSta = DATA_EEPROM_ProgramByte(Addr, *Buffer);
		
		if(FlashSta == FLASH_COMPLETE)
		{
//			u1_printf("%02X ", *Buffer);
		}
		else
		{
			u1_printf("err\r\n");
		}
		Length--;
		Buffer++;
		Addr++;
	}
	
	DATA_EEPROM_Lock();
	EN_INT;
}
//向EEPROM按字写入
void EEPROM_WriteWords(unsigned int Addr,unsigned int *Buffer,unsigned short Length)
{
	FLASH_Status FlashSta;

	DIS_INT;
	DATA_EEPROM_Unlock();
	
	while(Length > 0)
	{
		while(FLASH_WaitForLastOperation(FLASH_WAITETIME) != FLASH_COMPLETE);
		
		FlashSta = DATA_EEPROM_ProgramWord(Addr, *Buffer);
		
		if(FlashSta == FLASH_COMPLETE)
		{
//			u1_printf("%08X ", *Buffer);
		}
		else
		{
			u1_printf("err\r\n");
		}
		Length--;
		Buffer++;
		Addr += 4;
	}
	
	DATA_EEPROM_Lock();
	EN_INT;
}

//EEPROM擦除
void EEPROM_EraseWords(unsigned int BlockNum)
{
	u8 i;
	
	DIS_INT;
	DATA_EEPROM_Unlock();
	for(i=0; i<16; i++)
	{
		while(FLASH_WaitForLastOperation(FLASH_WAITETIME) != FLASH_COMPLETE);
		DATA_EEPROM_EraseWord(BlockNum*EEPROM_BLOCK_SIZE + i*4 + EEPROM_BASE_ADDR);
	}
	
	DATA_EEPROM_Lock();
	EN_INT;
}

unsigned char EEPROM_CheckSum(unsigned int addr, unsigned int lenth)
{
	unsigned int chksum = 0;
	int i;
	u8 data[DATA_LENGTH_MAX];
	
	if(lenth > DATA_LENGTH_MAX)
	{
		u1_printf(" Check Length Err\r\n");
		return 0;
	}
	EEPROM_ReadBytes(addr, data, lenth);
	for (i=0;i<lenth;i++)
	{
		chksum += data[i];		
	}	

	return (chksum & 0xff);	
}

