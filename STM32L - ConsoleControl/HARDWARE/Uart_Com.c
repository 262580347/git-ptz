/**********************************
说明:调试使用的串口代码
	  
作者:关宇晟
版本:V2017.4.3
***********************************/
#include "Uart_Com.h"
#include "main.h"

unsigned char g_ComTestFlag = FALSE;

static void COM1Protocol(void)
{
	unsigned char PackBuff[130], DataPack[130];
	u16   PackLength = 0, RecLength = 0, i;
	static CLOUD_HDR *hdr;
	
	UnPackMsg(g_USART_RX_BUF+1, g_USART_RX_CNT-2, PackBuff, &PackLength);	//解包

	if (PackBuff[(PackLength)-1] == Calc_Checksum(PackBuff, (PackLength)-1))
	{	
		hdr = (CLOUD_HDR *)PackBuff;
		hdr->protocol = swap_word(hdr->protocol);
		hdr->device_id = swap_dword(hdr->device_id);
		hdr->seq_no = swap_word(hdr->seq_no);
		hdr->payload_len = swap_word(hdr->payload_len);
		
		RecLength = hdr->payload_len + sizeof(CLOUD_HDR) + 1;
					
		if (RecLength != (PackLength))
		{
			u1_printf(" Pack Length Err\r\n");
		}
		else
		{
			PackLength--;	
			memcpy(&DataPack, PackBuff, sizeof(CLOUD_HDR));

			if(hdr->payload_len != 0)
				memcpy(&DataPack[sizeof(CLOUD_HDR)], &PackBuff[sizeof(CLOUD_HDR)], hdr->payload_len);

			OnDebug(DataPack, PackLength);
		}	
	}
	else
	{
		for(i=0; i<PackLength; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");
		u1_printf(" CRC Error :%02X\r\n", Calc_Checksum(PackBuff, (PackLength)-1));
	}	
}

void TaskForDebugCOM(void)
{
	u16   i, j, nMain10ms, CRCVal;
	u8  databuf[256], Num, data[64];
	unsigned int  databuf4[64];
	unsigned int Addr, Hor, Ver;
	
	if(g_UartRxFlag == TRUE)
	{	
		if(strncmp((char *)g_USART_RX_BUF, "RESET", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n RESET CMD\r\n");
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			Sys_Soft_Reset();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "TEST MODE", g_USART_RX_CNT) == 0)
		{
			if(g_ComTestFlag == FALSE)
			{
				g_ComTestFlag = TRUE;
			}
			else
			{
				u1_printf("\r\n Exit debugging mode, communication resume\r\n");
				g_ComTestFlag = FALSE;
				App_Run();
			}
			
		}
		else if(strncmp((char *)g_USART_RX_BUF, "PowerDetect", g_USART_RX_CNT) == 0)
		{
			u1_printf("PowerDetect\r\n");
		}		
		else if(strncmp((char *)g_USART_RX_BUF, "EraseLog", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n Erase EEPROM Data\r\n");
			for(i=9; i< EEPROM_BLOCK_COUNT; i++)
			{
				EEPROM_EraseWords(i);
				u1_printf(" Clear %d Block\r\n", i+1);
			}
			
			LogStorePointerInit();		//日志功能区
		}		
		else if((strncmp((char *)g_USART_RX_BUF, "log", g_USART_RX_CNT) == 0) || (strncmp((char *)g_USART_RX_BUF, "LOG", g_USART_RX_CNT) == 0))
		{
			ShowLogContent();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "READ1", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\nEEPROM Read1\r\n");
			
			for(j=0; j<16; j++)
			{
				EEPROM_ReadBytes(EEPROM_BASE_ADDR + j*256, databuf, 256); 
				
				for(i=0; i<256; i++)
				{
					u1_printf("%02X ", databuf[i]);
				}
				u1_printf("\r\n");
			}
			u1_printf("\r\n");
		}
		else if(strncmp((char *)g_USART_RX_BUF, "READ4", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\nEEPROM Read4\r\n");
					
			for(j=0; j<64; j++)
			{
				EEPROM_ReadWords(EEPROM_BASE_ADDR + j*64, databuf4, 16); 
				
				for(i=0; i<16; i++)
				{
					u1_printf("%08X ", databuf4[i]);
				}
				u1_printf("\r\n");
			}
			u1_printf("\r\n");
		}
		else if(strncmp((char *)g_USART_RX_BUF, "WRITE1 ", 7) == 0)
		{
			Addr = ArrayToInt32(&g_USART_RX_BUF[7], g_USART_RX_CNT-7);

			u1_printf("\r\nWRITE1 :%08X\r\n", Addr*64 + EEPROM_BASE_ADDR);
			for(i=0; i<64; i++)
			{
				databuf[i] = i;
			}
			EEPROM_WriteBytes(Addr*64 + EEPROM_BASE_ADDR, databuf, 64); 
		}
		else if(strncmp((char *)g_USART_RX_BUF, "WRITE4 ", 7) == 0)
		{
			Addr = ArrayToInt32(&g_USART_RX_BUF[7], g_USART_RX_CNT-7);

			u1_printf("\r\nWRITE4 :%08X\r\n", Addr*64 + EEPROM_BASE_ADDR);
			for(i=0; i<16; i++)
			{
				databuf4[i] = i;
			}
			EEPROM_WriteWords(Addr*64 + EEPROM_BASE_ADDR, databuf4, 16); 
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Erase1 ", 7) == 0)
		{
			Addr = ArrayToInt32(&g_USART_RX_BUF[7], g_USART_RX_CNT-7);

			u1_printf("\r\nErase :%08X\r\n", Addr*64 + EEPROM_BASE_ADDR);
			
			EEPROM_EraseWords(Addr);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "EraseAll", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n Erase All EEPROM Data\r\n");
			for(i=0; i< EEPROM_BLOCK_COUNT; i++)
			{
				EEPROM_EraseWords(i);
				u1_printf(" Clear %d Block\r\n", i+1);
			}
		}
				else if(strncmp((char *)g_USART_RX_BUF, "485Code ", 8) == 0)
		{
			Num = (g_USART_RX_CNT - 7)/3;
			u1_printf("Send 485 Code:");
			for(i=0; i<Num; i++)
			{
				StrToHex(&data[i], &g_USART_RX_BUF[8+i*3], Num);
				u1_printf("%02X ", data[i]);
			}				
			
			CRCVal = Calculate_CRC16(data, Num);
			data[Num++] = CRCVal&0xff;
			data[Num++] = CRCVal >> 8;
			
			u1_printf(" %02X %02X", CRCVal&0xff, CRCVal >> 8);
			
			u1_printf("\r\n");
			
			Uart_Send_Data(USART2, data, Num);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "TIME ", 5) == 0)
		{			
			Addr = ArrayToInt32(&g_USART_RX_BUF[5], g_USART_RX_CNT-5);
			u1_printf(" Set Time:%d\r\n", Addr);
//			Task_SetTime(TimeTest, Addr);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "KillTimer ", 10) == 0)
		{			
			Addr = ArrayToInt32(&g_USART_RX_BUF[10], g_USART_RX_CNT-10);
			u1_printf("Kill Timer Num:%d\r\n", Addr);
			KillTimer(Addr);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "GPIO12", g_USART_RX_CNT) == 0)
		{
			u1_printf("GPIO12\r\n");
			GPIO_ToggleBits(GPIOB, GPIO_Pin_12);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "GPIO13", g_USART_RX_CNT) == 0)
		{
			u1_printf("GPIO13\r\n");
			GPIO_ToggleBits(GPIOB, GPIO_Pin_13);
		}
		else if(g_USART_RX_BUF[0] == 0xff && g_USART_RX_BUF[1] == 0x01)
		{
			for(i=0; i<g_USART_RX_CNT; i++)
			{
				u1_printf("%02X ", g_USART_RX_BUF[i]);
			}
			u1_printf("\r\n\r\n");
		}		
		else if(strncmp((char *)g_USART_RX_BUF, "PLUSE ", 6) == 0)
		{		
			Addr = ArrayToInt32(&g_USART_RX_BUF[6], g_USART_RX_CNT-6);
			SetPWMPluse(Addr);
			u1_printf(" Set Pulse1 %d\r\n", Addr);
			TIM_Cmd(TIM3, ENABLE);
		}
		
		else if(strncmp((char *)g_USART_RX_BUF, "TEST ", 5) == 0)
		{		
			GPIO_InitTypeDef GPIO_InitStructure;	
			
			GPIO_InitStructure.GPIO_Mode = GPIO_Pin_4 | GPIO_Pin_5;
			GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
			GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
			GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
			GPIO_Init(GPIOB, &GPIO_InitStructure);
			
			GPIO_SetBits(GPIOB,GPIO_Pin_4);
			GPIO_SetBits(GPIOB,GPIO_Pin_5);
			
			Addr = ArrayToInt32(&g_USART_RX_BUF[5], g_USART_RX_CNT-5);
			
			HOR_MOTOR_ENABLE();
			VER_MOTOR_ENABLE();
			HOR_DIR_LEFT();	
			VER_DIR_DOWN();
			
			GPIO_ResetBits(CONSOLE_CONTROL_TYPE,HORIZONTAL_RST_PIN);
			GPIO_ResetBits(CONSOLE_CONTROL_TYPE,VERTICAL_RST_PIN);
			
			GPIO_SetBits(CONSOLE_CONTROL_TYPE,HORIZONTAL_SLEEP_PIN);
			GPIO_SetBits(CONSOLE_CONTROL_TYPE,VERTICAL_SLEEP_PIN);
			
			delay_ms(100);
			
			GPIO_SetBits(CONSOLE_CONTROL_TYPE,HORIZONTAL_RST_PIN);
			GPIO_SetBits(CONSOLE_CONTROL_TYPE,VERTICAL_RST_PIN);	
			u1_printf(" Test Pluse %d\r\n", Addr);
			
			for(i=0; i<Addr; i++)
			{
				GPIO_ToggleBits(GPIOB,GPIO_Pin_4);	
				GPIO_ToggleBits(GPIOB,GPIO_Pin_5);	
				delay_ms(200);
			}
		}
		else if(strncmp((char *)g_USART_RX_BUF, "ShowPluse", g_USART_RX_CNT) == 0)
		{		
			ReadStoreAngle();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Auto", g_USART_RX_CNT) == 0)
		{		
			u1_printf(" Auto Run\r\n", Addr);
			MotorAutoRun();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Status", g_USART_RX_CNT) == 0)
		{	
			u1_printf(" Motor Status:%d\r\n", GetMotorStatus());
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Set ", 4) == 0)
		{		
			Hor = ArrayToInt32(&g_USART_RX_BUF[4], 4);
			Ver = ArrayToInt32(&g_USART_RX_BUF[9], 3);
			u1_printf(" Set: %d %d\r\n", Hor, Ver);
			MotorControl_GotoAngle(Hor, Ver);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "RunMode ", 8) == 0)
		{		
			Hor = ArrayToInt32(&g_USART_RX_BUF[8], 1);
			u1_printf(" RunMode: %d\r\n", Hor);
			if(Hor == 0 || Hor == 1)
			{
				WriteMotorRunMode(Hor);
			}
		}
		else if(strncmp((char *)g_USART_RX_BUF, "AT", 2) == 0)
		{
			u1_printf("%s\r\n", &g_USART_RX_BUF);
			u3_printf("%s\r\n", &g_USART_RX_BUF);
			
			nMain10ms = GetSystem10msCount();
		
			while(CalculateTime(GetSystem10msCount(), nMain10ms) < 100)
			{
				if(g_Uart3RxFlag == TRUE)	//收到一帧数据包
				{		
					u1_printf("%s\r\n", g_USART3_RX_BUF);
					Clear_Uart3Buff();
					Clear_Uart1Buff();
					return;
				}				
			}
			
			u1_printf(" AT No Ack...\r\n");
		}		
		else
		{		
			if(g_USART_RX_BUF[0] == BOF_VAL && g_USART_RX_BUF[g_USART_RX_CNT-1] == EOF_VAL && g_USART_RX_CNT >= 10)
			{
				COM1Protocol();
			}
		}
		

		Clear_Uart1Buff();
	}	
	

}










