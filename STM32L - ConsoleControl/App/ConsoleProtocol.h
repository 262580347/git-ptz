#ifndef		_CONSOLEPROTOCOL_H_
#define		_CONSOLEPROTOCOL_H_

//#define		CMD_CONTROL_UP				0x01		//上
//#define		CMD_CONTROL_DOWN			0x02		//下
//#define		CMD_CONTROL_LEFT			0x03		//左
//#define		CMD_CONTROL_RIGHT			0x04		//右
//#define		CMD_CONTROL_STOP			0x05		//停止
//#define		CMD_CONTROL_AUTO			0x06		//自检操作
//#define		CMD_CONTROL_ANGLE			0x07		//转至指定角度
//#define		CMD_CONTROL_GO_PRESET		0x08		//转至预置点
//#define		CMD_CONTROL_SET_PRESET		0x09		//设置预置点
//#define		CMD_CONTROL_DEL_PRESET		0x0A		//删除预置点

//云台指令索引
#define		CONTROL_STOP				0		//停止
#define		CONTROL_UP					1		//上
#define		CONTROL_DOWN				2		//下
#define		CONTROL_LEFT				3		//左
#define		CONTROL_RIGHT				4		//右
#define		CONTROL_AUTO				5		//自检操作
#define		CONTROL_ANGLE				6		//转至指定角度
#define		CONTROL_GO_TO_PRESET		7		//转至预置点
#define		CONTROL_SET_PRESET			8		//设置预置点
#define		CONTROL_CLEAR_PRESET		9		//删除预置点

void ConsoleRS485ProtocolInit(void);















#endif








