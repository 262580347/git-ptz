#include "main.h"
#include "App.h"



static void TaskForAppRun(void)
{
	static u8 s_RunStatus = 1, s_RunCount = 0;
	static u32 s_LastTime = 0;
	unsigned char Ver = 0;
	
	switch(s_RunStatus)
	{
		case 1:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 300)
			{
				s_LastTime = GetSystem10msCount();
				s_RunStatus = 2;
			}
		break;
		
		case 2:
			u1_printf(" Auto\r\n");
			MotorAutoRun();
			s_RunStatus = 3;
		break;
		
		case 3:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 3000)
			{
				s_LastTime = GetSystem10msCount();
				s_RunStatus = 4;
			}
		break;
			
		case 4:
			Ver = (s_RunCount*10%60);
			u1_printf(" Go to Mid 120,%d\r\n", Ver);
			Motor_GotoSetAngle(120, Ver);
			s_RunStatus = 5;
		break;
		
		case 5:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 3000)
			{
				s_RunCount++;
				s_LastTime = GetSystem10msCount();
				s_RunStatus = 6;
			}
		break;
		
		case 6:
			Ver = (s_RunCount*10%60);
			u1_printf(" Go to Right 210,%d\r\n", Ver);
			Motor_GotoSetAngle(210, Ver);
			s_RunStatus = 7;
		break;
		
		case 7:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 3000)
			{
				s_RunCount++;
				s_LastTime = GetSystem10msCount();
				s_RunStatus = 8;
			}
		break;
			
		case 8:
			Ver = (s_RunCount*10%60);
			u1_printf(" Go to Left 30,%d\r\n", Ver);
			Motor_GotoSetAngle(30, Ver);
			s_RunStatus = 9;
		break;
		
		case 9:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 3000)
			{
				s_LastTime = GetSystem10msCount();
				s_RunStatus = 4;
				s_RunCount++;
				if(s_RunCount >= 150)
				{
					s_RunCount = 0;
					s_RunStatus = 1;
				}
			}
		break;
	}
}

void App_Run(void)
{
	Task_Create(TaskForAppRun, 1);
}










