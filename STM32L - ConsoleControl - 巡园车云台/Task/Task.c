#include "main.h"
#include "Task.h"

//任务信息结构
typedef struct
{
  HANDLE handle;  
	unsigned int   time;  
	unsigned int   count; 
}TASK_INFO;

//任务队列
static TASK_INFO m_tasks[TASK_SIZE] = {NULL};
//任务运行指示器
static unsigned int m_Pinter = 0;

//创建一个新的任务
unsigned char Task_Create(TASKFUN htack,unsigned int time)
{
	unsigned int i;
	//检索当前任务是否已被创建
	for(i=0;i<m_Pinter;i++)	
	{
		if((m_tasks[i].handle!=NULL)\
		&&((unsigned int32)m_tasks[i].handle==(unsigned int32)htack))
		{
			return TRUE;
		}
	}
	//如果当前满足条件
	if((m_Pinter+1)<TASK_SIZE)
	{
		i = m_Pinter;
		m_tasks[i].handle = htack;
		m_tasks[i].time   = time;
		m_tasks[i].count  = 0;
		m_Pinter++;
		return TRUE;
	}
	return FALSE;
}

//设置任务的运行周期
void Task_SetTime(TASKFUN htack,unsigned int time)
{
	unsigned int i;
	//检索当前任务是否已被创建
	for(i=0;i<m_Pinter;i++)	
	{
		if((m_tasks[i].handle!=NULL)\
		&&((unsigned int32)m_tasks[i].handle==(unsigned int32)htack))
		{
			m_tasks[i].time  = time;
		    m_tasks[i].count = 0;
			return;
		}
	}
}

//销毁一个任务
void Task_Kill(TASKFUN htack)
{
	unsigned int i,j;
	//检索当前任务是否已被创建
	for(i=0;i<m_Pinter;i++)	
	{
		if((m_tasks[i].handle!=NULL)\
		&&((unsigned int32)m_tasks[i].handle==(unsigned int32)htack))
		{
			if(m_Pinter<(TASK_SIZE-1))
			{
				for(j=i;j<m_Pinter;j++)
				{
					m_tasks[j].handle = m_tasks[j+1].handle;
					m_tasks[j].time   = m_tasks[j+1].time;
					m_tasks[j].count  = m_tasks[j+1].count;
				}	
			}
			m_Pinter--;
			if(m_Pinter>(TASK_SIZE-1))
			{
				m_Pinter = TASK_SIZE-1; 	
			}
			return;
		}
	}
}

//注销所有任务
void Task_KillAll(void)
{
	unsigned int i;
	m_Pinter = 0;
	for(i=0;i<TASK_SIZE;i++)
	{
		m_tasks[i].handle = NULL;
		m_tasks[i].time   = 0;	
	    m_tasks[i].count  = 0;	
	}		
}

//任务运行
void Task_Execute(void)
{
	unsigned int i;
	HANDLE htask = NULL;
	for(i=0;i<m_Pinter;i++)
	{
		if(m_tasks[i].count++>=m_tasks[i].time)
		{
			m_tasks[i].count = 0;
			htask = m_tasks[i].handle;
			if(htask!=NULL)
			{
				((TASKFUN)htask)();
			}					
		}			
	}	
}
