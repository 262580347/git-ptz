#ifndef __TIMER_H
#define __TIMER_H
/*********************************
功能：硬件定时器群
类型：独立定时器 = 4个
类型：共享定时器 >=1个
当独立定时器资源被全部占用时，
将自动启用共享定时器资源
**********************************/
//共享类定时器定时基准时间,单位:ms
#define BASE_SHARE_TIME 100
//共享类定时器最大可支持的虚拟定时器总数
#define MAX_SHARE_TIMER 4

typedef void (*BASEOBSER)(void);

void Timer2Enable(unsigned char bIsOn,unsigned int umS);

unsigned int GetSystem10msCount(void);

unsigned int CalculateTime( unsigned int nNew, unsigned int nOld);

void TimermsInit(void);

//申请一个独立的ms级的定时器
unsigned int SetTimer(unsigned int msTime,BASEOBSER hObser);
//注销一个独立的ms级的定时器
void KillTimer(unsigned int TimerID);

//申请一个共享的定时器,返回定时器ID>0
unsigned int AllocShareTimer(unsigned int StepTime,BASEOBSER hObser);
//注销一个共享的定时器
void DeAllocShareTimer(BASEOBSER hObser);
#endif
