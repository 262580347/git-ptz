#ifndef __LED_H
#define __LED_H	 

#define		HALL1_GPIO_PIN			GPIO_Pin_0
#define		HALL1_GPIO_TYPE			GPIOB

#define		READ_HALL1_SENSOR()		GPIO_ReadInputDataBit(HALL1_GPIO_TYPE, HALL1_GPIO_PIN)


#define		HALL2_GPIO_PIN			GPIO_Pin_1
#define		HALL2_GPIO_TYPE			GPIOB

#define		READ_HALL2_SENSOR()		GPIO_ReadInputDataBit(HALL2_GPIO_TYPE, HALL2_GPIO_PIN)


void LEDNoWork(void);

void STM32_GPIO_Init(void);

void RunLED(void);

void WakeUp_GPIO_Init(void);

void Stop_GPIO_Init(void);

void TaskForLEDBlinkRun(void);

void TaskForLEDBlinkEnd(void);

#endif
