#ifndef		_IIC_H_
#define		_IIC_H_

#define ACK 	1
#define NOACK	0

void IIC_Init(void);

void IIC_Start(void);

void IIC_Stop(void);

unsigned char IIC_WaitACK(void);

void IIC_WriteByte(unsigned char data);

unsigned char IIC_ReadByte(unsigned char IsAck);

#endif
