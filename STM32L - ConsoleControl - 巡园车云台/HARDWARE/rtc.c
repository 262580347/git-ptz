/**********************************
说明:初始化内部或外部RTC，通过宏定义选择
	  初始化闹钟唤醒
作者:关宇晟
版本:V2018.02.01
***********************************/
#include "main.h"
#include "rtc.h"

#define RTC_CLOCK_SOURCE_LSE
//#define RTC_CLOCK_SOURCE_LSI

#define MAX_ALARM_SECOND	23

typedef __packed struct
{
	unsigned char 			Magic; 	//0x55
	unsigned short 			Length;	//存储内容长度	
	unsigned char 			Chksum;	//校验和
	unsigned char			Hour;
	unsigned char			Minute;
	unsigned char			Sencond;
	unsigned int			ResetCount;
}RTC_SAVECONFIG;


RTC_SAVECONFIG		RTC_SaveConfig;
RTC_SAVECONFIG* 	p_RTC;

static u8 s_RTCBuf[sizeof(RTC_SAVECONFIG)];
static u8 s_SynchronizationTimeFlag = FALSE;


RTC_TimeTypeDef RTC_TimeStructure;
RTC_InitTypeDef RTC_InitStructure;
RTC_AlarmTypeDef  RTC_AlarmStructure;

unsigned char GetSynchronizationTimeFlag(void)
{
	return s_SynchronizationTimeFlag;
}

void SetSynchronizationTimeFlag(unsigned char isTrue)
{
	s_SynchronizationTimeFlag = isTrue;
}

void RTC_Config(void);

void SysSetRTCTime(unsigned char Year, unsigned char Month, unsigned char Date, unsigned char Hours, unsigned char Minutes, unsigned char Seconds)
{
	int ret1, ret2;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
	

	RTC_DateStruct.RTC_Date  	= Date;
	RTC_DateStruct.RTC_Month  	= Month;
	RTC_DateStruct.RTC_Year  	= Year;	
	RTC_DateStruct.RTC_WeekDay  = 1;
	ret2 = RTC_SetDate(RTC_Format_BIN, &RTC_DateStruct);

	RTC_TimeStructure.RTC_Seconds	 = Seconds;
	RTC_TimeStructure.RTC_Minutes 	 = Minutes; 
	RTC_TimeStructure.RTC_Hours	 	 = Hours;
	ret1 = RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure);	
	
	u1_printf(" ---Lock-in Time---Ret:%d %d\r\n", ret1, ret2);
	
	RTC_WaitForSynchro();//等待RTC寄存器同步     
	delay_ms(100);	
	RTC_TimeShow();
}
void RTC_TimeShow(void)
{
	RTC_DateTypeDef RTC_DateStruct;
	
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
	u1_printf("\r\n Now Time:20%02d-%02d-%02d %02d:%02d:%02d\r\n", RTC_DateStruct.RTC_Year, RTC_DateStruct.RTC_Month, RTC_DateStruct.RTC_Date, RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
}

void RTC_EasyShowTime(void)
{
	RTC_DateTypeDef RTC_DateStruct;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	u1_printf("\r\n [%02d:%02d:%02d]", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
}
void RTC_AlarmShow(void)
{
	/* Get the current Alarm */
	RTC_GetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_AlarmStructure);
	
	u1_printf("\r\n The current alarm is :  %0.2d:%0.2d:%0.2d \r\n", RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours, RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes, RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds);
}

unsigned int GetRTCSecond(void)
{
	unsigned int SumOfSecond = 0;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	SumOfSecond = RTC_TimeStructure.RTC_Seconds + RTC_TimeStructure.RTC_Minutes*60 +RTC_TimeStructure.RTC_Hours*3600;
	
	return SumOfSecond;
}

unsigned int DifferenceOfRTCTime(unsigned int NewTime, unsigned int OldTime)
{
	unsigned int nTemp = 0;
	
	if (NewTime >= OldTime)
	{
		nTemp = NewTime - OldTime;
	}
	else
	{
		nTemp = 86400 - OldTime + NewTime;
	}
	return (nTemp);	
}



void RTC_Config_Init(void)
{
	p_RTC = &RTC_SaveConfig;
	
	if (RTC_ReadBackupRegister(RTC_BKP_DR0) != 0x32F2)
	{  
		/* RTC configuration  */
		RTC_Config();

		/* Configure the RTC data register and RTC prescaler */
		RTC_InitStructure.RTC_AsynchPrediv = 0x7f;
		RTC_InitStructure.RTC_SynchPrediv  = 0xff;
		RTC_InitStructure.RTC_HourFormat   = RTC_HourFormat_24;

		/* Check on RTC init */
		if (RTC_Init(&RTC_InitStructure) == ERROR)
		{
			u1_printf("\r\n RTC Prescaler Config failed\\ \r\n");
		}

		/* Configure the time register */
		RTC_TimeStructure.RTC_H12     = RTC_H12_AM;
		RTC_TimeStructure.RTC_Hours   = 0;
		RTC_TimeStructure.RTC_Minutes = 0;
		RTC_TimeStructure.RTC_Seconds = 0;
		
		delay_ms(200);
		while(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure) == ERROR)
		{
			u1_printf("\r\n>> !! RTC Set Time failed. Reset!! <<\r\n");
			delay_ms(200);
		} 
			u1_printf("\r\n>> !! RTC Set Time success. !! <<\r\n");
	
			RTC_SaveConfig.Magic 		= 0x55;
			RTC_SaveConfig.Length 		= sizeof(RTC_SAVECONFIG) - 4;
			RTC_SaveConfig.Chksum 		= 0;
			RTC_SaveConfig.Hour 		= 0;
			RTC_SaveConfig.Minute 		= 0;
			RTC_SaveConfig.Sencond		= 0;
			RTC_SaveConfig.ResetCount	= 0;

			RTC_SaveConfig.Chksum = Calc_Checksum((unsigned char *)&RTC_SaveConfig.Hour, sizeof(RTC_SaveConfig) - 4);
			EEPROM_WriteBytes(RESET_COUNT_ADDR, (unsigned char *)&RTC_SaveConfig, sizeof(RTC_SaveConfig));

			RTC_TimeShow();
		
			delay_ms(1000);//等待第一次上电RTC稳定
			/* Indicator for the RTC configuration */
			RTC_WriteBackupRegister(RTC_BKP_DR0, 0x32F2);
		
	}
	else
	{
		
		if (Check_Area_Valid(RESET_COUNT_ADDR))
		{
			EEPROM_ReadBytes(RESET_COUNT_ADDR, s_RTCBuf, sizeof(RTC_SAVECONFIG));
			
			p_RTC = (RTC_SAVECONFIG *)s_RTCBuf;
			
			RTC_SaveConfig.Hour 		= p_RTC->Hour;
			RTC_SaveConfig.Minute 		= p_RTC->Minute;
			RTC_SaveConfig.Sencond		= p_RTC->Sencond;
			RTC_SaveConfig.ResetCount	= p_RTC->ResetCount;
			
			RTC_SaveConfig.ResetCount++;
			
			u1_printf("\r\n CPU Reset Count:%d \r\n", RTC_SaveConfig.ResetCount);
			
			RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
			RTC_SaveConfig.Magic 		= 0x55;
			RTC_SaveConfig.Length 		= sizeof(RTC_SAVECONFIG) - 4;
			RTC_SaveConfig.Chksum 		= 0;
			RTC_SaveConfig.Hour 		= RTC_TimeStructure.RTC_Hours;
			RTC_SaveConfig.Minute 		= RTC_TimeStructure.RTC_Minutes;
			RTC_SaveConfig.Sencond		= RTC_TimeStructure.RTC_Seconds;

			RTC_SaveConfig.Chksum = Calc_Checksum((unsigned char *)&RTC_SaveConfig.Hour, sizeof(RTC_SaveConfig) - 4);
			EEPROM_WriteBytes(RESET_COUNT_ADDR, (unsigned char *)&RTC_SaveConfig, sizeof(RTC_SaveConfig));
		}

		
		

		/* Check if the Power On Reset flag is set */
		if (RCC_GetFlagStatus(RCC_FLAG_PORRST) != RESET)
		{
			u1_printf("\r\n Power On Reset occurred....\r\n");
		}
		/* Check if the Pin Reset flag is set */
		else if (RCC_GetFlagStatus(RCC_FLAG_PINRST) != RESET)
		{
			u1_printf("\r\n External Reset occurred....\r\n");
		}

		u1_printf("\r\n No need to configure RTC....\r\n");

		/* Enable the PWR clock */
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

		/* Allow access to RTC */
		PWR_RTCAccessCmd(ENABLE);

		/* Wait for RTC APB registers synchronisation */
		RTC_WaitForSynchro();

		/* Clear the RTC Alarm Flag */
		RTC_ClearFlag(RTC_FLAG_ALRAF);

		/* Clear the EXTI Line 17 Pending bit (Connected internally to RTC Alarm) */
		EXTI_ClearITPendingBit(EXTI_Line17);

		/* Display the RTC Time and Alarm */
		RTC_TimeShow();
	}	
	
//	RTC_CoarseCalibConfig(RTC_CalibSign_Negative, 6); //
//	
//	RTC_CoarseCalibCmd(ENABLE);
	
//RTC_SmoothCalibConfig(RTC_SmoothCalibPeriod_8sec, RTC_SmoothCalibPlusPulses_Set, 63);


}

void RTC_Config()
{
	/* Enable the PWR clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

	/* Allow access to RTC */
	PWR_RTCAccessCmd(ENABLE);

	#if defined (RTC_CLOCK_SOURCE_LSI)  /* LSI used as RTC source clock*/
	/* The RTC Clock may varies due to LSI frequency dispersion. */   
	/* Enable the LSI OSC */ 
	RCC_LSICmd(ENABLE);

	/* Wait till LSI is ready */  
	while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
	{
		
	}

	/* Select the RTC Clock Source */
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);

	#elif defined (RTC_CLOCK_SOURCE_LSE) /* LSE used as RTC source clock */
	/* Enable the LSE OSC */
	RCC_LSEConfig(RCC_LSE_ON);

	delay_ms(10);
	/* Wait till LSE is ready */  
	while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
	{
		delay_ms(10);
		RCC_LSEConfig(RCC_LSE_ON);
	}

	/* Select the RTC Clock Source */
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

	#else
	#error Please select the RTC Clock source inside the main.c file
	#endif /* RTC_CLOCK_SOURCE_LSI */

	/* Enable the RTC Clock */
	RCC_RTCCLKCmd(ENABLE);

	/* Wait for RTC APB registers synchronisation */
	RTC_WaitForSynchro();
}

void RTC_SetAlarmA(u8 Second)
{
	EXTI_InitTypeDef EXTI_InitStructure;
	RTC_AlarmTypeDef RTC_AlarmStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	if(Second == 0)
	{
		return;
	}
	/* EXTI configuration */
	EXTI_ClearITPendingBit(EXTI_Line17);
	EXTI_InitStructure.EXTI_Line = EXTI_Line17;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Enable the RTC Alarm Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = RTC_Alarm_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
		
	if(Second > MAX_ALARM_SECOND)
	{
		Second = MAX_ALARM_SECOND;
	}
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds = RTC_TimeStructure.RTC_Seconds + Second;	
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = RTC_TimeStructure.RTC_Minutes;	
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours = RTC_TimeStructure.RTC_Hours;	
	if(RTC_TimeStructure.RTC_Seconds + Second >= 60)
	{
		RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = RTC_TimeStructure.RTC_Minutes + 1;
		RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds = Second + RTC_TimeStructure.RTC_Seconds - 60;
	}
	

	if(RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes == 60)
	{
		RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = 0;
		RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours = RTC_TimeStructure.RTC_Hours + 1;
	}
	
	if(RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours == 24)
	{
		RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours = 0;
	}
	
	RTC_AlarmStructure.RTC_AlarmDateWeekDay = 0x31;
	RTC_AlarmStructure.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;
	RTC_AlarmStructure.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay;

	/* Configure the RTC Alarm A register */
	RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_AlarmStructure);	
//	RTC_AlarmShow();
	
	RTC_ITConfig(RTC_IT_ALRA, ENABLE);
	RTC_AlarmCmd(RTC_Alarm_A, ENABLE);
}

void RTC_AlarmConfig(void)
{
	EXTI_InitTypeDef EXTI_InitStructure;
	RTC_AlarmTypeDef RTC_AlarmStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* EXTI configuration */
	EXTI_ClearITPendingBit(EXTI_Line17);
	EXTI_InitStructure.EXTI_Line = EXTI_Line17;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Enable the RTC Alarm Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = RTC_Alarm_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
	
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours = 0x00;
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = 0x00;
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds = 0x00;
	
	RTC_AlarmStructure.RTC_AlarmDateWeekDay = 0x31;
	RTC_AlarmStructure.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;
	RTC_AlarmStructure.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay;

	/* Configure the RTC Alarm A register */
	RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_AlarmStructure);
//	RTC_AlarmShow();
	/* Enable AlarmA interrupt */
	RTC_ITConfig(RTC_IT_ALRA, ENABLE);

	/* Enable the alarmA */
	RTC_AlarmCmd(RTC_Alarm_A, ENABLE);

}

//#define PPM_PER_STEP  0.9536743 //10^6/2^20.
//#define PPM_PER_SEC   0.3858025 //10^6/(30d*24h*3600s).

//u16 FastSecPer30days = 117; //?????117??????

//void RTC_Calibration(void)
//{
//	float Deviation = 0.0;
//	u8 CalibStep = 0;

//	Deviation = FastSecPer30days * PPM_PER_SEC; //??ppm??
//	Deviation /= PPM_PER_STEP; //?????????
//	CalibStep = (u8)Deviation; // ?????????
//	if(Deviation >= (CalibStep + 0.5))
//	CalibStep += 1; //????
//	if(CalibStep > 127) 
//	CalibStep = 127; // ?????0�127??

//	BKP_SetRTCCalibrationValue(CalibStep); //?????
//    
//}
/************************************** EOF *********************************/
