/**********************************
说明：定时器配置
作者：关宇晟
版本：V2019.05.07
***********************************/
#include "Timer.h"
#include "main.h"

//=============== 独立定时器资源 ======================
//最大支持的硬件定时器总数[3、4、6、7]
#define TIMER_ID_SIZE  4
//硬件定时器初始化函数原型
typedef void (*TIMER_INIT)(BOOL bIsOn,unsigned int umS);
//定时器队列
static BASEOBSER 	m_hTimerObers[TIMER_ID_SIZE]  = {NULL};
static TIMER_INIT   m_hTimerFun[TIMER_ID_SIZE] = {NULL};
//=====================================================
//=========== 共享定时器资源[使用Timer2] ==============
//Timer2启用标识
static BOOL m_bShareTimer = FALSE;
//定时器中断观察者组
static BASEOBSER m_hShareTimers[MAX_SHARE_TIMER] = {NULL};
//定时器中断观察者预初值组
static unsigned int m_ShareValues[MAX_SHARE_TIMER] = {0};
//定时器中断观察者计时器组
static unsigned int m_ShareCounts[MAX_SHARE_TIMER] = {0};

static unsigned int s_TimerCount = 0;

//定时器2初始化
void Timer2Enable(unsigned char bIsOn,unsigned int umS)
{	
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	static BOOL bInit = FALSE;
	
	if(bIsOn)
	{
		if(!bInit)
		{
			bInit = TRUE;
			RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE); //时钟使能//TIM4时钟使能    
			NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
			NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0 ;//抢占优先级3
			NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;		//子优先级3
			NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
			NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
		}

		TIM_TimeBaseStructure.TIM_Period = umS; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	
		TIM_TimeBaseStructure.TIM_Prescaler = SYS_CLOCK; //设置用来作为TIMx时钟频率除数的预分频值+ 16
		TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
		TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
		TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位
	 
		TIM_ClearFlag(TIM2,TIM_FLAG_Update);
		TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE); //使能指定的TIM4中断,允许更新中断	
		TIM_Cmd(TIM2, ENABLE);  //使能TIMx			
	}
	else
	{
		TIM_ITConfig(TIM2,TIM_IT_Update,DISABLE);
		TIM_Cmd(TIM2,DISABLE); //关闭时钟
		TIM_DeInit(TIM2);
	}
	
//	Task_Create(TimerCountShow, 1);
}
//定时器3初始化
static void Timer3Enable(unsigned char bIsOn,unsigned int umS)
{	
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	static BOOL bInit = FALSE;
	
	if(bIsOn)
	{
		if(!bInit)
		{
			bInit = TRUE;
			RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE); //时钟使能//TIM4时钟使能    
			NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
			NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0 ;//抢占优先级3
			NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;		//子优先级3
			NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
			NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
		}

		TIM_TimeBaseStructure.TIM_Period = umS; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	
		TIM_TimeBaseStructure.TIM_Prescaler = SYS_CLOCK; //设置用来作为TIMx时钟频率除数的预分频值+ 16
		TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
		TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
		TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位
	 
		TIM_ClearFlag(TIM3,TIM_FLAG_Update);
		TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE); //使能指定的TIM4中断,允许更新中断	
		TIM_Cmd(TIM3, ENABLE);  //使能TIMx			
	}
	else
	{
		TIM_ITConfig(TIM3,TIM_IT_Update,DISABLE);
		TIM_Cmd(TIM3,DISABLE); //关闭时钟
		TIM_DeInit(TIM3);
	}
}
//定时器4初始化
static void Timer4Enable(unsigned char bIsOn,unsigned int umS)
{	
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	static BOOL bInit = FALSE;
	
	if(bIsOn)
	{
		if(!bInit)
		{
			bInit = TRUE;
			RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE); //时钟使能//TIM4时钟使能    
			NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
			NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0 ;//抢占优先级3
			NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;		//子优先级3
			NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
			NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
		}

		TIM_TimeBaseStructure.TIM_Period = umS; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	
		TIM_TimeBaseStructure.TIM_Prescaler = SYS_CLOCK; //设置用来作为TIMx时钟频率除数的预分频值+ 16
		TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
		TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
		TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位
	 
		TIM_ClearFlag(TIM4,TIM_FLAG_Update);
		TIM_ITConfig(TIM4,TIM_IT_Update,ENABLE); //使能指定的TIM4中断,允许更新中断	
		TIM_Cmd(TIM4, ENABLE);  //使能TIMx			
	}
	else
	{
		TIM_ITConfig(TIM4,TIM_IT_Update,DISABLE);
		TIM_Cmd(TIM4,DISABLE); //关闭时钟
		TIM_DeInit(TIM4);
	}
}
	 
static void Timer6Enable(unsigned char bIsOn,unsigned int umS)
{	
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	static BOOL bInit = FALSE;
	
	if(bIsOn)
	{
		if(!bInit)
		{
			bInit = TRUE;
			RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE); //时钟使能//TIM6时钟使能    
			NVIC_InitStructure.NVIC_IRQChannel = TIM6_IRQn;
			NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0 ;//抢占优先级3
			NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;		//子优先级3
			NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
			NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
		}

		TIM_TimeBaseStructure.TIM_Period = umS; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	
		TIM_TimeBaseStructure.TIM_Prescaler = SYS_CLOCK; //设置用来作为TIMx时钟频率除数的预分频值+ 16
		TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
		TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
		TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位
	 
		TIM_ClearFlag(TIM6,TIM_FLAG_Update);
		TIM_ITConfig(TIM6,TIM_IT_Update,ENABLE); //使能指定的TIM6中断,允许更新中断	
		TIM_Cmd(TIM6, ENABLE);  //使能TIMx			
	}
	else
	{
		TIM_ITConfig(TIM6,TIM_IT_Update,DISABLE);
		TIM_Cmd(TIM6,DISABLE); //关闭时钟
		TIM_DeInit(TIM6);
	}
}

static void Timer7Enable(unsigned char bIsOn,unsigned int umS)
{	
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	static BOOL bInit = FALSE;
	
	if(bIsOn)
	{
		if(!bInit)
		{
			bInit = TRUE;
			RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE); //时钟使能//TIM7时钟使能    
			NVIC_InitStructure.NVIC_IRQChannel = TIM7_IRQn;
			NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0 ;//抢占优先级3
			NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;		//子优先级3
			NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
			NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
		}

		TIM_TimeBaseStructure.TIM_Period = umS; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	
		TIM_TimeBaseStructure.TIM_Prescaler = SYS_CLOCK; //设置用来作为TIMx时钟频率除数的预分频值+ 16
		TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
		TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
		TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位
	 
		TIM_ClearFlag(TIM7,TIM_FLAG_Update);
		TIM_ITConfig(TIM7,TIM_IT_Update,ENABLE); //使能指定的TIM7中断,允许更新中断	
		TIM_Cmd(TIM7, ENABLE);  //使能TIMx			
	}
	else
	{
		TIM_ITConfig(TIM7,TIM_IT_Update,DISABLE);
		TIM_Cmd(TIM7,DISABLE); //关闭时钟
		TIM_DeInit(TIM7);
	}
}

//定时器2中断服务
void TIM2_IRQHandler()
{
	u8 i;	 

	if(TIM_GetITStatus(TIM2,TIM_IT_Update))
	{
		TIM_ClearITPendingBit(TIM2,TIM_FLAG_Update);
		for(i=0;i<MAX_SHARE_TIMER;i++)
		{
			if(m_hShareTimers[i]!=NULL)
			{
				m_ShareValues[i]++;
				if(m_ShareValues[i]>=m_ShareCounts[i])
				{							
					m_ShareValues[i] = 0;
					((BASEOBSER)m_hShareTimers[i])();	
				}	
			}
		}	
	}	
}

////定时器3中断服务
//void TIM3_IRQHandler()
//{
//	if(TIM_GetITStatus(TIM3,TIM_IT_Update))
//	{
//		TIM_ClearITPendingBit(TIM3 ,TIM_FLAG_Update);
//		if(m_hTimerObers[0] != NULL)
//		{
//	    	((BASEOBSER)m_hTimerObers[0])();
//		} 
//	}
//}

//定时器4中断服务
//void TIM4_IRQHandler()
//{
//	if(TIM_GetITStatus(TIM4,TIM_IT_Update))
//	{
//		TIM_ClearITPendingBit(TIM4 ,TIM_FLAG_Update);
//		if(m_hTimerObers[1] != NULL)
//		{
//	    	((BASEOBSER)m_hTimerObers[1])();
//		} 
//	}
//}

//定时器6中断服务
void TIM6_IRQHandler()
{
	if(TIM_GetITStatus(TIM6,TIM_IT_Update))
	{
		TIM_ClearITPendingBit(TIM6 ,TIM_FLAG_Update);
		s_TimerCount++;
	}
}

//定时器7中断服务
void TIM7_IRQHandler()
{
	if(TIM_GetITStatus(TIM7,TIM_IT_Update))
	{
		TIM_ClearITPendingBit(TIM7 ,TIM_FLAG_Update);
		if(m_hTimerObers[3] != NULL)
		{
	    	((BASEOBSER)m_hTimerObers[3])();
		} 
	}
}

//申请一个独立的ms级的定时器
unsigned int SetTimer(unsigned int msTime,BASEOBSER hObser)
{
	unsigned int i;
    unsigned int StepTime = 1;
	unsigned int ID;

	if(m_hTimerFun[0]==NULL)
	{
		m_hTimerFun[0] = Timer3Enable; 
		m_hTimerFun[1] = Timer4Enable;  
		m_hTimerFun[2] = Timer6Enable; 
		m_hTimerFun[3] = Timer7Enable; 	
	}
	//判断当前定时器阵列中是否已注册过相同的观察者
	for(i=0;i<TIMER_ID_SIZE;i++)
	{
		//如果指定的定时器已经启用过，则允许重新设定时间
		if( (int)((void *)m_hTimerObers[i]) == (int)((void *)hObser) )
		{
			m_hTimerFun[i](TRUE,msTime);
			return i+1;	
		}
	}
	//如果未指定过定时器
	for(i=0;i<TIMER_ID_SIZE;i++)
	{	
		if(m_hTimerObers[i] == NULL)
		{
			m_hTimerObers[i] = hObser;
			m_hTimerFun[i](TRUE,msTime);
			return i+1;		
		}
	}
	//独立定时器资源溢出，启用共享定时器
	if(msTime>BASE_SHARE_TIME)
	{
		StepTime = msTime/BASE_SHARE_TIME;
	}
	
	ID = AllocShareTimer(StepTime,hObser);
	
	if(ID > 0)
	{	 
		u1_printf("[Timer]->Const Alloc ShareTimer:%d\r\n",ID-1);
		return TIMER_ID_SIZE+ID;		
	}
	u1_printf(" [Timer]->Timers Full!\r\n",ID-1);
	return 0;			
}

//注销一个独立的ms及的定时器
void KillTimer(unsigned int TimerID)
{
	if(m_hTimerFun[0]==NULL)
	{
		m_hTimerFun[0] = Timer3Enable; 
		m_hTimerFun[1] = Timer4Enable;  
		m_hTimerFun[2] = Timer6Enable; 
		m_hTimerFun[3] = Timer7Enable; 	
	}
	if((TimerID<=TIMER_ID_SIZE)&&(TimerID>0))
	{ 
		m_hTimerFun[TimerID-1](FALSE,NULL);
		m_hTimerObers[TimerID-1] = NULL;
		return;
	}
	if((TimerID>TIMER_ID_SIZE)&&(TimerID<=(TIMER_ID_SIZE+MAX_SHARE_TIMER)))
	{
		DeAllocShareTimer(m_hShareTimers[TimerID-TIMER_ID_SIZE-1]);
		u1_printf("[Timer]->Const Kill ShareTimer:%d\r\n",TimerID-TIMER_ID_SIZE-1);		
	}
}
//申请一个共享的定时器
unsigned int AllocShareTimer(unsigned int StepTime,BASEOBSER hObser)
{
	unsigned int i;

	for(i=0;i<MAX_SHARE_TIMER;i++)
	{
		if((unsigned int32)m_hShareTimers[i]==(unsigned int)hObser)
		{
			m_ShareValues[i] = 0;
			m_ShareCounts[i] = StepTime;
			if(!m_bShareTimer)
			{
				Timer2Enable(TRUE,BASE_SHARE_TIME);	
			}
			return i+1;		
		}
	}
	for(i=0;i<MAX_SHARE_TIMER;i++)
	{
		if(m_hShareTimers[i]==NULL)
		{
			m_ShareValues[i]  = 0;
			m_ShareCounts[i]  = StepTime;
			m_hShareTimers[i] = hObser;	
			if(!m_bShareTimer)
			{
				Timer2Enable(TRUE,BASE_SHARE_TIME);	
			}
			return i+1;
		}
	}
	return 0;
}

//注销一个共享的定时器
void DeAllocShareTimer(BASEOBSER hObser)
{
	unsigned int i;

	for(i=0;i<MAX_SHARE_TIMER;i++)
	{
		if((unsigned int32)m_hShareTimers[i]==(unsigned int32)hObser)
		{
			m_hShareTimers[i] = NULL;
			m_ShareValues[i]  = 0;
			m_ShareCounts[i]  = 0;		
			break;
		}
	}
	for(i=0;i<MAX_SHARE_TIMER;i++)
	{
		if(m_hShareTimers[i]!=NULL)
		{
			return;
		}
	}
	Timer2Enable(FALSE,0);		
}

unsigned int GetSystem10msCount(void)
{
	return s_TimerCount;
}

static void TaskForShowTime(void)
{

	static u32 s_LastTime = 0;
	static uint32_t IC1Value = 0, LastIC1Value = 0, s_Last_PWM_Count;
	
//	if(s_LastTime != s_TimerCount && s_TimerCount % 100 == 0)
//	{
//		s_LastTime = s_TimerCount;
//		u1_printf("%d\r\n", s_LastTime);
////		IC1Value = Frequency;
////		
////		if(IC1Value > LastIC1Value)
////		{
////			u1_printf(" %d\r\n", IC1Value - LastIC1Value);
////			u1_printf("PWM: %d\r\n", PWM_Count - s_Last_PWM_Count);
////		}
////		else
////		{
////			u1_printf(" %d\r\n", 0xFFFFFFFF - LastIC1Value + IC1Value + 1);
////			
////			u1_printf("PWM: %d\r\n",  0xFFFFFFFF - s_Last_PWM_Count + PWM_Count + 1);
////		}
////		s_Last_PWM_Count = PWM_Count;
////		LastIC1Value = IC1Value;
////		s_LastTime = s_TimerCount;
//	}
	
	if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 100)
	{
		s_LastTime = GetSystem10msCount();
		u1_printf("%d\r\n", s_LastTime);
	}

}

void TimermsInit(void)
{
	Timer6Enable(TRUE, 9);
//	Task_Create(TaskForShowTime, 1);
}

unsigned int CalculateTime( unsigned int nNew, unsigned int nOld)
{
	unsigned int nTemp = 0;
	if (nNew >= nOld)
	{
		nTemp = nNew - nOld;
	}
	else
	{
		nTemp = 0xFFFFFFFF - nOld + nNew + 1;
	}
	return (nTemp);
}
