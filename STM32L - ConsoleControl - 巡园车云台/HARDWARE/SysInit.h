#ifndef  _SYS_INIT_
#define	 _SYS_INIT_

#include "Lora.h"

#define		VAILD 0x55

#define 	PROTOCOL_CONTROL_ID			(500) 

#define 	SYSTEM_CFG_ADDR		(EEPROM_BASE_ADDR + 0*EEPROM_BLOCK_SIZE)  	//系统配置存储区
#define 	RESET_COUNT_ADDR	(EEPROM_BASE_ADDR + 1*EEPROM_BLOCK_SIZE)  	//重启次数存储区
#define 	RUNMODE_FLAG_ADDR	(EEPROM_BASE_ADDR + 2*EEPROM_BLOCK_SIZE)  	//运行模式存储区
#define 	ANGLE_STORE_ADDR	(EEPROM_BASE_ADDR + 3*EEPROM_BLOCK_SIZE)  	//当前位置存储
#define 	WIFI_USER_ADDR		(EEPROM_BASE_ADDR + 4*EEPROM_BLOCK_SIZE)  	//WLAN用户名存储区
#define 	WIFI_PASSWORD_ADDR	(EEPROM_BASE_ADDR + 5*EEPROM_BLOCK_SIZE)  	//WLAN密码存储区

#define 	HTTP_SERVER_ADDR	(EEPROM_BASE_ADDR + 6*EEPROM_BLOCK_SIZE)  	//HTTP服务器地址	*128

#define 	LOG_STORE_POINTER_ADDR	(EEPROM_BASE_ADDR + 9*EEPROM_BLOCK_SIZE)  	//日志区指针地址
#define		LOG_STORE_START_ADDR	(EEPROM_BASE_ADDR + 10*EEPROM_BLOCK_SIZE)  	//日志区起始地址

#define		WIFI_USER_BLOCK			4
#define		WIFI_PASSWORD_BLOCK		5
#define		HTTP_BLOCK1				6
#define		HTTP_BLOCK2				7
typedef __packed struct
{
	unsigned short int software_ver;  	//软件版本
	unsigned short int hardware_ver;	//硬件版本
	unsigned char pn[8];			//?? 	如:20150330
	unsigned char sn[8];			//?? 如:nostore
}SYSTEM_INFO;

//typedef __packed struct
//{
//	unsigned int		Device_ID;	//设备ID
//	unsigned short int	Gateway;	//网关号
//	unsigned char		Channel;	//通信频道	
//	LORA_CONFIG			LoraConfig;
//}SYSTEMCONFIG;
typedef __packed struct
{
	unsigned int 		Device_ID;		//设备ID
	unsigned char 		Type;			//网络连接类型
	unsigned short int		Net;		//zigbee网络号
	unsigned short int 		Node;		//zigbee节点号
	unsigned short int		Gateway;	//zigbee网关号
	unsigned char 		Channel;	//zigbee通信频道
	unsigned char 		LowpowerFlag;	//zigbee拓扑结构
	unsigned char 		Gprs_ServerIP[4];	//GPRS服务器IP地址
	unsigned short int 		Gprs_Port;		//GPRS服务器端口号
	unsigned char 		Retry_Times;		//重发次数
	unsigned short int 		Heart_interval;		//心跳间隔
	unsigned short int 		Data_interval;		//数据上报间隔
	unsigned short int 		State_interval;		//状态上报间隔
	unsigned char			AutoResetFlag;
	unsigned short			AutoResetTime;		
}SYSTEMCONFIG;

typedef __packed struct
{
	unsigned char 			magic; 	//0x55
	unsigned short 			length;	//存储内容长度	
	unsigned char 			chksum;	//校验和
	unsigned int 			Device_ID;		//设备ID
	unsigned char 			Type;			//网络连接类型
	unsigned short int		Net;		//zigbee网络号
	unsigned short int 		Node;		//zigbee节点号
	unsigned short int		Gateway;	//zigbee网关号
	unsigned char 			Channel;	//zigbee通信频道
	unsigned char 			LowpowerFlag;	//zigbee拓扑结构
	unsigned char 			Gprs_ServerIP[4];	//GPRS服务器IP地址
	unsigned short int 		Gprs_Port;		//GPRS服务器端口号
	unsigned char 			Retry_Times;		//重发次数
	unsigned short int 		Heart_interval;		//心跳间隔
	unsigned short int 		Data_interval;		//数据上报间隔
	unsigned short int 		State_interval;		//状态上报间隔
	unsigned char			AutoResetFlag;
	unsigned short			AutoResetTime;		
}SYSCFGSAVE;

//typedef __packed struct
//{
//	unsigned char magic; 	//0x55
//	unsigned short length;	//存储内容长度	
//	unsigned char chksum;	//校验和
//	unsigned int		Device_ID;	//设备ID
//	unsigned short int	Gateway;	//网关号
//	unsigned char		Channel;	//通信频道	
//	LORA_CONFIG			LoraConfig;
//}SYSCFGSAVE;

typedef struct
{
	unsigned int d_report_acc;	   //数据上报累计计数
	unsigned int s_report_acc;	  	//状态上报累计计数
	
	unsigned int h_report_acc;	   //心跳上报时间间隔累计
	unsigned char h_status;				//
	
	unsigned int s_retry_timeout;		//状态重发超时
	unsigned int s_retry_times;		//重发次数
	
	unsigned int keep_alive;		 //
	unsigned int p_save_acc;		//脉冲值保存周期
	
	unsigned int hs_report_acc;	//历史数据上传间隔周期
	unsigned int hs_interval;
	
	unsigned int detect_acc;		//检测在线状态的累加定时器(定期清零)
	unsigned char is_online;		//是否在线
	
	SYSTEMCONFIG *SysConfig;	//通信参数系统配置
	SYSTEM_INFO *SysInfo;	//嵌入式软件信息
}SYSTEM_CTRL;

void SysInit(void);

void Wake_SysInit(void);

void SetOnline( unsigned char ucData);
	
unsigned int Check_Area_Valid(unsigned int base_addr);

unsigned char Set_System_Config(SYSTEMCONFIG *SysConfig);

extern SYSTEM_CTRL 	g_SysCtrl;

SYSTEMCONFIG * GetSystemConfig(void);

SYSTEM_INFO 	*Get_SystemInfo(void);


	
#endif
