#include "main.h"
#include "SysInit.h"

SYSTEM_CTRL 	g_SysCtrl;
SYSTEMCONFIG	g_SystemConfig;
SYSTEM_INFO		g_SystemInfo;

static u8 s_SysData[50] ;

void InitSystemConfig(void);
	
const SYSTEMCONFIG DefaultSystemCofig = 
{
	19050000,	   		//设备ID
	NETTYPE_WLAN,	//网络连接类型
	9999,			//网络号
	9999,			//节点号
	9999,			//网关地址
	22,				//通信频道
	0,				//0为休眠  1为不休眠
	{47,106,119,252}, //IP
	6070,			//端口
	1,				//云台预置点数量
	600,			//心跳间隔时间间隔
	600,			//数据上报时间间隔
	600				//状态上报时间间隔
};

static const SYSTEM_INFO default_sys_info = 
{
	SOFTWARE_VERSION,					//软件版本
	HARDWARE_VERSION,					//硬件版本
	{'2','0','2','0','1','2','0','8'},//序号
	{'C','o','n','s','o','l','e',' '}
};

SYSTEM_INFO 	*Get_SystemInfo(void)
{
	return g_SysCtrl.SysInfo;
}

void SetOnline( unsigned char ucData)
{
	g_SysCtrl.is_online = ucData;
}
void NVIC_Config(void)
{
	NVIC_InitTypeDef   NVIC_InitStructure;	
	
	 NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);	
		/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
		/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}
//RCC_MSIRange_0:/*!< MSI = 65.536 KHz  */
//RCC_MSIRange_1:/*!< MSI = 131.072 KHz */
//RCC_MSIRange_2:/*!< MSI = 262.144 KHz */
//RCC_MSIRange_3: /*!< MSI = 524.288 KHz */
//RCC_MSIRange_4:/*!< MSI = 1.048 MHz   */
//RCC_MSIRange_5:/*!< MSI = 2.097 MHz   */
//RCC_MSIRange_6:/*!< MSI = 4.194 MHz   */

void SysInit(void)
{	
	RCC_ClocksTypeDef RCC_Clocks;
	
	RTC_TimeTypeDef RTC_TimeStructure;

//	SysClockForMSI(RCC_MSIRange_6);
//	SysClockForHSE();
	
	RCC_GetClocksFreq(&RCC_Clocks);

	Delay_Init(SYS_CLOCK);		//延时配置
	
	STM32_GPIO_Init();			//GPIO配置
	
	NVIC_Config();				//中断优先级配置
	
	USART1_Config(115200);		//串口1，用于debug
	
	u1_printf("\r\n SYSCLK_Frequency: %dHz\r\n",RCC_Clocks.SYSCLK_Frequency);
	
	u1_printf("\r\n Project Build Data: %s - %s\r\n",  __DATE__ , __TIME__); //编译日期
	
	delay_ms(1000);
	SetIWDG(5);				//看门狗设置
	
	TimermsInit();
	
//	InitSystemConfig();
		
	LogStorePointerInit();		//日志功能区
	
//	TIM4_PWM_Input();	
//	
	Timer3_PWMOutoutModeInit(TIME_CCR_VAL);
		
	MotorRunModeInit();
		
	ConsoleRS485ProtocolInit();
	
	u1_printf("\r\n Console Control\r\n");
	
	if(ReadMotorRunMode() < 2)
	{
		Console_Control_Init();
		
		App_Run();					//APP正式运行
	}
	else
	{
		u1_printf(" 手持拍照模式，不使用云台\r\n");
	}







}


//计算是否为合法地址
unsigned int Check_Area_Valid(unsigned int base_addr)
{
	SYS_TAG tag;
	unsigned char chksum;
	unsigned char data[4];
	
	EEPROM_ReadBytes(base_addr, data, 4);

	tag.magic = data[0];
	tag.length = (data[1]) + ((data[2]) << 8);
	tag.chksum = data[3] ;
	
	if (tag.magic != VAILD)
	{
//		u1_printf("VAILD\r\n");
		return 0;		
	}

	chksum = EEPROM_CheckSum((base_addr+sizeof(SYS_TAG)), tag.length);

	if (tag.chksum != chksum) 
	{
//		u1_printf("Check\r\n");
		return 0;
	}
	
	if(tag.length == 0)
	{
		return 1;
	}
	else
	{
		return tag.length;
	}
}

SYSTEMCONFIG * GetSystemConfig(void)
{
	return g_SysCtrl.SysConfig;
}
static void Init_SystemInfo(void)
{		
	g_SysCtrl.SysInfo = (SYSTEM_INFO *)&default_sys_info;
}
//设置系统参数
unsigned char Set_System_Config(SYSTEMCONFIG *SysConfig)
{
	SYSCFGSAVE SysCfgSave;
	
	SysCfgSave.magic = 0x55;
	SysCfgSave.length = sizeof(SYSTEMCONFIG);
	SysCfgSave.chksum = Calc_Checksum((unsigned char *)SysConfig, sizeof(SYSTEMCONFIG));

	if(SysConfig->Type > 10)
	{
		return FALSE;
	}
	if(SysConfig->Device_ID >= 0x10000000)
	{
		return FALSE;
	}	
	if(SysConfig->Channel > 32)
	{
		return FALSE;
	}

	if(SysConfig->Gateway != SysConfig->Net)
	{
		return FALSE;
	}

	if(SysConfig->Retry_Times > 8)	//云台预置点设为0~8之间
	{
		SysConfig->Retry_Times = 8;
	}
	
	memcpy((void *)&SysCfgSave.Device_ID, SysConfig, sizeof(SYSTEMCONFIG));

	if((sizeof(SYSCFGSAVE)%4) == 0)
	{
		EEPROM_WriteWords(SYSTEM_CFG_ADDR, (unsigned int *)&SysCfgSave, sizeof(SYSCFGSAVE)/4);
	}
	else
	{
		EEPROM_WriteWords(SYSTEM_CFG_ADDR, (unsigned int *)&SysCfgSave, sizeof(SYSCFGSAVE)/4+1);
	}
	
//	EEPROM_WriteBytes(SYSTEM_CFG_ADDR, (unsigned char *)&SysCfgSave, sizeof(SYSCFGSAVE));
	if (Check_Area_Valid(SYSTEM_CFG_ADDR))
	{
		EEPROM_ReadBytes(SYSTEM_CFG_ADDR, s_SysData, sizeof(SYSCFGSAVE));
		g_SysCtrl.SysConfig = (SYSTEMCONFIG *)(&s_SysData[4]);
	}
	else
	{
		//使用默认数据
		g_SysCtrl.SysConfig = (SYSTEMCONFIG *)&DefaultSystemCofig;
	}
	
	return TRUE;
}
static void ReadHttpServer(void)
{
	u8  Http_Length, HttpBuf[128];
	SYS_TAG tag;
	char DefualtBuf[50] = "https://yxb.tcloudit.cn/";
	
	Http_Length = Check_Area_Valid(HTTP_SERVER_ADDR);
	if (Http_Length >= 4 && Http_Length <= 120)
	{
		u1_printf("\r\n (Size:%d)", Http_Length);
		EEPROM_ReadBytes(HTTP_SERVER_ADDR, (u8 *)HttpBuf, sizeof(SYS_TAG) +  Http_Length);
		
		HttpBuf[Http_Length + sizeof(SYS_TAG)] = 0;
		u1_printf(" %s\r\n", &HttpBuf[sizeof(SYS_TAG)]);
	}	
	else if(Http_Length > 120)
	{
		u1_printf("\r\n HttpServer Over\r\n");
	}
	else
	{
		Http_Length = strlen(DefualtBuf);
		tag.magic = VAILD;
		tag.length = Http_Length;
		tag.chksum = Calc_Checksum((unsigned char *)DefualtBuf, Http_Length);
		
		memcpy(HttpBuf, &tag, sizeof(SYS_TAG));
		memcpy(&HttpBuf[sizeof(SYS_TAG)], DefualtBuf, Http_Length);
		
		EEPROM_EraseWords(HTTP_BLOCK1);
		EEPROM_EraseWords(HTTP_BLOCK2);
		EEPROM_WriteBytes(HTTP_SERVER_ADDR, (unsigned char *)HttpBuf, sizeof(SYS_TAG) + Http_Length);

//		HttpBuf[sizeof(SYS_TAG) + Http_Length] = 0;
//		u1_printf("(Size:%d)%s\r\n", Http_Length, &HttpBuf[sizeof(SYS_TAG)]);	
	}
}
//系统参数读取初始化
static void System_Option_Init(void)
{
	u8  i;
	memset(&g_SysCtrl, 0, sizeof(SYSTEM_CTRL));
	
	//参数配置信息===============================================================

	if (Check_Area_Valid(SYSTEM_CFG_ADDR))
	{
		EEPROM_ReadBytes(SYSTEM_CFG_ADDR, s_SysData, sizeof(SYSCFGSAVE));
		g_SysCtrl.SysConfig = (SYSTEMCONFIG *)(&s_SysData[4]);

		u1_printf("\r\n [System]Read Config Success!\r\n");
	}
	else
	{
		//使用默认数据
		g_SysCtrl.SysConfig = (SYSTEMCONFIG *)&DefaultSystemCofig;
		
	}
	
	//软件版本信息==================================================================
	g_SysCtrl.SysInfo = (SYSTEM_INFO *)&default_sys_info;
	u1_printf("\r\n [System]Soft:%d  Hard:%d  Time:", g_SysCtrl.SysInfo->software_ver, g_SysCtrl.SysInfo->hardware_ver);		
	for(i=0; i<sizeof(g_SysCtrl.SysInfo->pn); i++)
		u1_printf("%c", g_SysCtrl.SysInfo->pn[i]);
	u1_printf("  Versions:");
	for(i=0; i<sizeof(g_SysCtrl.SysInfo->sn); i++)
		u1_printf("%c", g_SysCtrl.SysInfo->sn[i]);
	u1_printf("\r\n");
}
//打印系统配置
void InitSystemConfig(void)
{
	u8 i;
	SYSTEMCONFIG *p_sys;
	
	System_Option_Init();
	Init_SystemInfo();
//	p_sys = GetSystemConfig();		
//	
//	u1_printf("\r\n [System]DeviceID:%d\r\n", p_sys->Device_ID);
////	u1_printf("\r\n [System]Node:%d\r\n", p_sys->Node);
//	u1_printf("\r\n [System]Type:%d   ", p_sys->Type);
//	if(p_sys->Type == NETTYPE_SIM800C)
//		u1_printf("SIM800C\r\n");
//	else if(p_sys->Type == NETTYPE_LORA)
//		u1_printf("LORA\r\n");
//	else if(p_sys->Type == NETTYPE_GPRS)
//		u1_printf("GPRS\r\n");
//	else if(p_sys->Type == NETTYPE_NB_IOT)
//		u1_printf("NB\r\n");
//	else if(p_sys->Type == NETTYPE_WLAN)
//		u1_printf("WLAN\r\n");
//	else if(p_sys->Type == NETTYPE_4G)
//		u1_printf("4G\r\n");
//	u1_printf("\r\n [System]Net:%d\r\n", p_sys->Net);
////	u1_printf("\r\n [System]Channel:%d\r\n", p_sys->Channel);
//	u1_printf("\r\n [System]IP:");
//	for(i=0; i<4; i++)
//		u1_printf("%d ", p_sys->Gprs_ServerIP[i]);
//	u1_printf("\r\n");
//	u1_printf("\r\n [System]Port:%d\r\n", p_sys->Gprs_Port);
//	u1_printf("\r\n [System]LowPowerFlag:%d\r\n", p_sys->LowpowerFlag);
//	u1_printf("\r\n [System]Console Num:%d\r\n", p_sys->Retry_Times);
//	u1_printf("\r\n [System]Photo Interval:%d\r\n", p_sys->Heart_interval);
//	u1_printf("\r\n [System]Data Interval:%d\r\n", p_sys->Data_interval);

}

