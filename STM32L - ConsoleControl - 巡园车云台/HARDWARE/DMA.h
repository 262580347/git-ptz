#ifndef		_DMA_H_
#define		_DMA_H_




void DMA_Config(void);

void DMA_Usart2_Config(void);

void DMA_Usart3_Config(void);

void u1_printf(char* fmt,...);

void u2_printf(char* fmt,...);

void u3_printf(char* fmt,...) ;

void DMAUsart3RxConfig(void);

void DMAUsart2RxConfig(void);

void DMAUsart1RxConfig(void);

void USART1_DMA_Send(unsigned char *buffer, unsigned short buffer_size);

void USART3_DMA_Send(unsigned char *buffer, unsigned short buffer_size);

void USART2_DMA_Send(unsigned char *buffer, unsigned short buffer_size);



#endif


