#include "ConsoleProtocol.h"
#include "main.h"

#define		RS485_BAND_RATE		9600

#define		CMD_CONTROL		0x06
#define		CMD_INQUIRE		0x03

static u8 s_SendCount = 0;		//发送计数

static void UartSendConsoleRunStatus(unsigned char Status)
{
	u8 RS485Buf[10];
	u32 NowHorAngle = 0, NowVerAngle = 0, CRCVal = 0;
	
	NowHorAngle = GetNowHorizontalPluse();
	NowVerAngle = GetNowVerticalPluse();
	
	NowHorAngle = NowHorAngle/HOR_ANGLETOPLUSE;
	NowVerAngle = NowVerAngle/VER_ANGLETOPLUSE;
	
	RS485Buf[0] = 0;
	RS485Buf[1] = CMD_INQUIRE;
	RS485Buf[2] = 5;
	RS485Buf[3] = Status;
	RS485Buf[4] = NowHorAngle >> 8;
	RS485Buf[5] = NowHorAngle&0xff;
	RS485Buf[6] = NowVerAngle >> 8;
	RS485Buf[7] = NowVerAngle&0xff;
	CRCVal = Calculate_CRC16(RS485Buf, 8);
	RS485Buf[8] = CRCVal&0xff;
	RS485Buf[9] = CRCVal >> 8;	
	USART2_DMA_Send(RS485Buf, 10);
	USART3_DMA_Send(RS485Buf, 10);
	u1_printf(" UartSendConsoleRunStatus\r\n");
}

static void UartSendConsoleControlAck(unsigned char Status)
{
	u8 RS485Buf[10];
	u16 NowHorAngle = 0, NowVerAngle = 0, CRCVal = 0;
	
	NowHorAngle = GetNowHorizontalPluse();
	NowVerAngle = GetNowVerticalPluse();
	
	RS485Buf[0] = 0;
	RS485Buf[1] = CMD_CONTROL;
	RS485Buf[2] = 5;
	RS485Buf[3] = Status;
	RS485Buf[4] = NowHorAngle >> 8;
	RS485Buf[5] = NowHorAngle&0xff;
	RS485Buf[6] = NowVerAngle >> 8;
	RS485Buf[7] = NowVerAngle&0xff;
	CRCVal = Calculate_CRC16(RS485Buf, 8);
	RS485Buf[8] = CRCVal&0xff;
	RS485Buf[9] = CRCVal >> 8;	
	USART2_DMA_Send(RS485Buf, 10);	
	USART3_DMA_Send(RS485Buf, 10);
}

static void TaskForSendStopAck(void)
{	
	s_SendCount++;
	UartSendConsoleRunStatus(MOTOR_STOP);
	
	if(s_SendCount >= 3)
	{
		s_SendCount = 0;
		Task_Kill(TaskForSendStopAck);
	}
}

static void ExecutionInstruction(unsigned char *RS485Data)
{
	u8 Cmd = 0;
	u8 NowMotorStaus = 0, RunMode;
	unsigned int HorAngle = 0, VerAngle = 0;
	
	Cmd = RS485Data[1];
	
	if(Cmd == CMD_CONTROL)
	{
		switch(RS485Data[3])
		{
			case MOTOR_RUN_UP://上
				u1_printf(" Up\r\n");
				UartSendConsoleControlAck(RS485Data[3]);
				MotorControl_Up(MAX_VER_PLUSE);
			break;
			
			case MOTOR_RUN_DOWN://下
				u1_printf(" Down\r\n");
				UartSendConsoleControlAck(RS485Data[3]);
				MotorControl_Down(MAX_VER_PLUSE);
			break;
			
			case MOTOR_RUN_LEFT://左
				u1_printf(" Left\r\n");
				UartSendConsoleControlAck(RS485Data[3]);
				MotorControl_Left(MAX_HOR_PLUSE);
			break;
			
			case MOTOR_RUN_RIGHT://右
				u1_printf(" Right\r\n");
				UartSendConsoleControlAck(RS485Data[3]);
				MotorControl_Right(MAX_HOR_PLUSE);
			break;
			
			case MOTOR_STOP://停止
				u1_printf(" Stop\r\n");
				UartSendConsoleControlAck(RS485Data[3]);
				MotorControl_Stop();
			break;
			
			case MOTOR_AUTO://自检操作
				u1_printf(" Auto\r\n");
				UartSendConsoleControlAck(RS485Data[3]);
				MotorAutoRun();
			break;		
			
			case MOTOR_ANGLE://转至指定角度
				
				HorAngle = (RS485Data[4] << 8) + (RS485Data[5]);
				VerAngle = (RS485Data[6] << 8) + (RS485Data[7]);;
											
				u1_printf(" Angle: %d %d\r\n", HorAngle, VerAngle);		
		
				HorAngle *= HOR_ANGLETOPLUSE;
			
				VerAngle *= VER_ANGLETOPLUSE;
			
				if(HorAngle > MAX_HOR_PLUSE)
				{
					HorAngle = MAX_HOR_PLUSE;
				}
				
				if(VerAngle > MAX_VER_PLUSE)
				{
					VerAngle = MAX_VER_PLUSE;
				}
			
				u1_printf(" Pluse: %d %d\r\n", HorAngle, VerAngle);	
				
				MotorControl_GotoAngle(HorAngle, VerAngle);
				UartSendConsoleControlAck(RS485Data[3]);
			break;		

			case MOTOR_STOP_ACK:
				
				Task_Kill(TaskForSendStopAck);
				s_SendCount = 0;
				u1_printf(" Rec STM32 Stop Ack\r\n");
			
			break;
			
			case MOTOR_RUN_MODE:
				RunMode = RS485Data[5];
				u1_printf(" RunMode: %d\r\n", RunMode);
				if(RunMode == 0 || RunMode == 1 || RunMode == 2)
				{
					WriteMotorRunMode(RunMode);
				}
			break;
			
			case MOTOR_INIT:
				u1_printf(" 返回初始位置\r\n");
				MotorAutoRun();
				UartSendConsoleControlAck(RS485Data[3]);
			break;
			
//			case CMD_CONTROL_GO_TO_PRESET://转至预置点
//				
//			break;
//			
//			case CMD_CONTROL_SET_PRESET://设置预置点
//				
//			break;
//			
//			case CMD_CONTROL_CLEAR_PRESET://删除预置点
//				
//			break;			
			default:
				
			break;
		}
	}
	else if(Cmd == CMD_INQUIRE)
	{
		NowMotorStaus = GetMotorStatus();
		UartSendConsoleRunStatus(NowMotorStaus);
	}
}

void TaskForRecRS485Pack(void)
{
	static u8 DataBuf[20], s_LastStaus = MOTOR_STOP;
	unsigned short CRCVal = 0;
	u8 i;
	
	if(g_Uart2RxFlag)
	{		
		memcpy(DataBuf, g_USART2_RX_BUF, 10);	//只处理一次中断到来的数据
			
		CRCVal = Calculate_CRC16(DataBuf, 8);
		if((CRCVal == (DataBuf[9] << 8) + DataBuf[8]) && (g_USART2_RX_CNT >= 10))
		{
			ExecutionInstruction(DataBuf);		//执行对应操作
			if(DataBuf[3] != MOTOR_STOP_ACK)
			{
				s_LastStaus = DataBuf[3];
			}
			Task_Kill(TaskForSendStopAck);
			s_SendCount = 0;
		}
		else
		{
			u1_printf("\r\n CRC error: ");

			for(i=0; i<g_USART2_RX_CNT; i++)
			{
				u1_printf(" %02X", g_USART2_RX_BUF[i]);
			}
			u1_printf("\r\n");			
		}											
		Clear_Uart2Buff();
	}
	
	if((MOTOR_STOP == GetMotorStatus()) && (s_LastStaus != MOTOR_STOP) && (GetMotorAutoRunFlag() == FALSE))
	{
		s_LastStaus = MOTOR_STOP;
		
		Task_Create(TaskForSendStopAck, 150000);
		
		UartSendConsoleRunStatus(MOTOR_STOP);
		
		u1_printf(" Stop Ack2\r\n");
	}
}

void TaskForUart3(void)
{
	static u8 DataBuf[20], s_LastStaus = MOTOR_STOP;
	unsigned short CRCVal = 0;
	u8 i;
	
	if(g_Uart3RxFlag)
	{		
		memcpy(DataBuf, g_USART3_RX_BUF, 10);	//只处理一次中断到来的数据
			
		CRCVal = Calculate_CRC16(DataBuf, 8);
		if((CRCVal == ((DataBuf[9] << 8) + DataBuf[8])) && (g_USART3_RX_CNT >= 10))
		{
			ExecutionInstruction(DataBuf);		//执行对应操作
			if(DataBuf[3] != MOTOR_STOP_ACK)
			{
				s_LastStaus = DataBuf[3];
			}
			Task_Kill(TaskForSendStopAck);
			s_SendCount = 0;
		}
		else
		{
			u1_printf("\r\n CRC3 error: ");

			for(i=0; i<g_USART3_RX_CNT; i++)
			{
				u1_printf(" %02X", g_USART3_RX_BUF[i]);
			}
			u1_printf("\r\n");			
		}											
		Clear_Uart3Buff();
	}
	
	if((MOTOR_STOP == GetMotorStatus()) && (s_LastStaus != MOTOR_STOP) && (GetMotorAutoRunFlag() == FALSE))
	{
		s_LastStaus = MOTOR_STOP;
		
		Task_Create(TaskForSendStopAck, 150000);
		
		UartSendConsoleRunStatus(MOTOR_STOP);
		
		u1_printf(" Stop Ack3\r\n");
	}
}

void ConsoleRS485ProtocolInit(void)
{
	USART2_Config(9600);
	
	Task_Create(TaskForRecRS485Pack, 1);
	
	Clear_Uart2Buff();
	
	USART3_Config(115200);
	
	Task_Create(TaskForUart3, 1);
	
	Clear_Uart3Buff();
}






