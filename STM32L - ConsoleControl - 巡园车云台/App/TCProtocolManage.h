#ifndef		_TCPROTOCOLMANAGE_H_
#define		_TCPROTOCOLMANAGE_H_

#include "stm32l1xx_usart.h"

#define 	TC_DATA_SIZE    256

typedef enum
{
	NotConnected = 1,
	GotIP = 2,
	TCPConnected = 3,
	LinkFault = 4,
	ModErr = 5,
}COM_CONNECTSTATUS;

typedef struct
{
	unsigned char AliveAckFlag;
	unsigned char DataAckFlag;
	unsigned char StatusAckFlag;
	unsigned char ResetFlag;
	unsigned int  LastLinkTime;
	COM_CONNECTSTATUS	LinkStatus;
	
}COM_RECSTATUS;

//Ӧ��֡�ṹ
typedef __packed struct
{
	unsigned short  Protocol;
	unsigned int  	DeviceID;
	unsigned char  	Dir;
	unsigned short 	Seq;
	unsigned short 	Length;	
	unsigned char  	OPType;
	unsigned char  	UserBuf[TC_DATA_SIZE];	
}TC_COMM_MSG; 

extern COM_RECSTATUS	g_TCRecStatus;

typedef void (*TCCOMMRXOBSER)(TC_COMM_MSG *pMsg);

void OnRecTCProtocol(USART_TypeDef* USARTx, TC_COMM_MSG *pMsg);

unsigned char GetUpdataTimeFlag(void);

void SetUpdataTimeFlag(unsigned char isTrue);

unsigned char GetServerConfigCmdFlag(void);

void SetServerConfigCmdFlag(unsigned char isTrue);

void TaskForTCRxObser(void);

void ClearLockinTimeFlag(void);

unsigned char GetLockinTimeFlag(void);

void SetTCProtocolRunFlag(unsigned char isTrue);

void TCProtocolInit(void);

void SetDataPackFlag(unsigned char isTrue);

unsigned char GetDataPackFlag(void);

void SetTCModuleReadyFlag(unsigned char isTrue);

unsigned char GetTCModuleReadyFlag(void);

void SetCmdCloseSocket(unsigned char isTrue);

unsigned char GetCmdCloseSocket(void);



#endif




















