#include "main.h"
#include "App.h"

#define		INIT_TIME		TIME_CCR_VAL/150
#define 	RANDOM(x) (rand()%x)

static unsigned char s_Motor1_Status = 0, s_Motor2_Status = 0;
void MotorRun1(void)
{
	if(s_Motor1_Status == 0)
	{
		delay_ms(200);
		MotorControl_Left(MAX_HOR_PLUSE/2);
		s_Motor1_Status = 10;
	}
	else if(s_Motor1_Status == 1)
	{
		delay_ms(200);
		MotorControl_Right(MAX_HOR_PLUSE);
		s_Motor1_Status = 11;
	}
	else if(s_Motor1_Status == 2)
	{
		u1_printf("水平自检完成\r\n");
		Task_Kill(MotorRun1);
		s_Motor1_Status = 0;
	}
	else if(s_Motor1_Status == 10)
	{
		if(GetMotorStatus() == MOTOR_STOP)
		{
			s_Motor1_Status = 1;
		}
	}
	else if(s_Motor1_Status == 11)
	{
		if(GetMotorStatus() == MOTOR_STOP)
		{
			s_Motor1_Status = 2;
		}
	}
}

void HallSensorDetect1(void)
{
	static unsigned int Time1 = 0, Time2 = 0;
	static unsigned char s_Status = 0;
	
	if(READ_HALL1_SENSOR() == Bit_SET)
	{
		Time1 = GetSystem10msCount();
		s_Status = 0;	
	}
	else if(READ_HALL1_SENSOR() == Bit_RESET)
	{
		if(s_Status == 0)
		{
			Time1 = GetSystem10msCount();
			s_Status = 1;
		}
		else if(s_Status == 1)
		{
			if(CalculateTime(GetSystem10msCount(), Time1) >= INIT_TIME)
			{
				MotorControl_Stop();
				s_Status = 2;
				Time2 = GetSystem10msCount();
				u1_printf(" 找到水平初始点 Time:%d\r\n", Time2 - Time1);		
				Task_Kill(MotorRun1);
				s_Motor1_Status = 0;
				SetMotorStatus(MOTOR_INIT);
			}
		}
		
	}
}

void MotorRun2(void)
{	
	if(s_Motor2_Status == 0)
	{
		delay_ms(200);
		MotorControl_Up(MAX_VER_PLUSE/4);
		s_Motor2_Status = 10;
	}
	else if(s_Motor2_Status == 1)
	{
		delay_ms(200);
		MotorControl_Down(MAX_VER_PLUSE);
		s_Motor2_Status = 11;
	}
	else if(s_Motor2_Status == 2)
	{
		u1_printf("垂直自检完成\r\n");
		Task_Kill(MotorRun2);
		s_Motor2_Status = 0;
	}
	else if(s_Motor2_Status == 10)
	{
		if(GetMotorStatus() == MOTOR_STOP)
		{
			s_Motor2_Status = 1;
		}
	}
	else if(s_Motor2_Status == 11)
	{
		if(GetMotorStatus() == MOTOR_STOP)
		{
			s_Motor2_Status = 2;
		}
	}
}

void HallSensorDetect2(void)
{
	static unsigned int Time1 = 0, Time2 = 0;
	static unsigned char s_Status = 0;
	
	if(READ_HALL2_SENSOR() == Bit_SET)
	{
		Time1 = GetSystem10msCount();
		s_Status = 0;	
	}
	else if(READ_HALL2_SENSOR() == Bit_RESET)
	{
		if(s_Status == 0)
		{
			Time1 = GetSystem10msCount();
			s_Status = 1;
		}
		else if(s_Status == 1)
		{
			if(CalculateTime(GetSystem10msCount(), Time1) >= INIT_TIME)
			{
				MotorControl_Stop();
				s_Status = 2;
				Time2 = GetSystem10msCount();
				u1_printf(" 找到垂直初始点 Time:%d\r\n", Time2 - Time1);		
				Task_Kill(MotorRun2);
				s_Motor2_Status = 0;
				SetMotorStatus(MOTOR_INIT);
			}
		}
		
	}
}

static void TaskForAppRun(void)
{
	static u8 s_RunStatus = 1, s_RunCount = 0;
	static u32 s_LastTime = 0;
	unsigned int Ver = 0, Hor = 0;
	
	switch(s_RunStatus)
	{
		case 1:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 100)
			{
				s_LastTime = GetSystem10msCount();
				s_RunStatus = 2;
			}
		break;
		
		case 2:
			u1_printf(" Auto test mode\r\n");
			Task_Create(MotorRun1, 1000);
	
			Task_Create(HallSensorDetect1, 10);
			s_RunStatus = 3;
		break;
		
		case 3:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 3000)
			{
				s_LastTime = GetSystem10msCount();
				s_RunStatus = 4;
				u1_printf(" 寻找水平初始点失败\r\n");
			}
			else if(GetMotorStatus() == MOTOR_INIT)
			{
				u1_printf(" 云台位于初始位置，开始测试模式\r\n");
				s_RunStatus = 4;
				delay_ms(1000);
			}
		break;
			
		case 4:
			Hor = RANDOM(360);
			Ver = RANDOM(110);
			u1_printf(" Go to %d %d\r\n", Hor, Ver);
			Motor_GotoSetAngle(Hor, Ver);
			s_RunStatus = 5;
			s_LastTime = GetSystem10msCount();
		break;
		
		case 5:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1000)
			{
				s_RunCount++;
				s_LastTime = GetSystem10msCount();
				s_RunStatus = 4;
				if(s_RunCount >= 20)
				{
					s_RunCount = 0;
					s_RunStatus = 1;
				}
			}
		break;
		
	}
}



void ReturnInitPort(void)
{
	static unsigned char s_Status = 0;
	static unsigned int s_LastTime = 0;
	
	switch(s_Status)
	{
		case 0:
			s_Motor1_Status = 0;
			s_Motor2_Status = 0;
			s_Status++;
		break;
		case 1:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 200)
			{
				s_LastTime = GetSystem10msCount();
				s_Status = 2;
			}
		break;
		
		case 2:
			if(READ_HALL1_SENSOR() == Bit_RESET)
			{
				u1_printf(" 云台已经位于水平初始位置\r\n");
				SetNowHorizontalPluse(MAX_HOR_PLUSE/2);
				s_Status = 4;
				s_LastTime = GetSystem10msCount();
			}
			else
			{
				u1_printf(" Auto1\r\n");
				Task_Create(MotorRun1, 1);
				Task_Create(HallSensorDetect1, 1);
				s_Status = 3;
			}
			
		break;
		
		case 3:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1200)
			{
				s_LastTime = GetSystem10msCount();
				s_Status = 4;
				u1_printf(" 寻找水平初始点失败\r\n");
				Task_Kill(HallSensorDetect1);
			}
			else if(GetMotorStatus() == MOTOR_INIT)
			{
				u1_printf(" 云台位于水平初始位置\r\n");
				SetNowHorizontalPluse(MAX_HOR_PLUSE/2);
				s_Status = 4;
				Task_Kill(HallSensorDetect1);
				delay_ms(100);
				s_LastTime = GetSystem10msCount();
			}
		break;
			
		case 4:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 50)
			{
				s_LastTime = GetSystem10msCount();
				s_Status = 5;
			}
		break;
		
		case 5:
			if(READ_HALL2_SENSOR() == Bit_RESET)
			{
				u1_printf(" 云台已经位于垂直初始位置\r\n");
				SetNowVerticalPluse(MAX_VER_PLUSE/4);
				s_Status = 7;
				s_LastTime = GetSystem10msCount();
			}
			else
			{
				u1_printf(" Auto2\r\n");
				Task_Create(MotorRun2, 1);
				Task_Create(HallSensorDetect2, 1);
				s_Status = 6;
				s_LastTime = GetSystem10msCount();
			}
		break;
		
		case 6:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 600)
			{
				s_LastTime = GetSystem10msCount();
				s_Status = 7;
				u1_printf(" 寻找垂直初始点失败\r\n");
				Task_Kill(HallSensorDetect2);
			}
			else if(GetMotorStatus() == MOTOR_INIT)
			{
				u1_printf(" 云台位于垂直初始位置\r\n");
				SetNowVerticalPluse(MAX_VER_PLUSE/4);
				s_Status = 7;
				Task_Kill(HallSensorDetect2);
				delay_ms(100);
			}
		break;
			
		case 7:
			Task_Kill(ReturnInitPort);
			s_Status = 1;
			if(ReadMotorRunMode() == 1)
			{
				Task_Create(TaskForAppRun, 1);
			}
		break;
		
	}
}

void MotorAutoRun(void)
{
	Task_Create(ReturnInitPort, 1);
}

void App_Run(void)
{
	Task_Create(ReturnInitPort, 1);

}






