/**********************************
说明:MAX44009底层驱动程序
	  需要进行系数补偿，默认补偿值为3.8
	  开启高亮寄存器以使用高亮度环境
作者:关宇晟
版本:V2018.4.3
***********************************/
#include "main.h"
#include "MAX44009.h"

		
#define CMD_MAX_READ		0x01
#define CMD_MAX_WRITE		0x00

#define MAX44009_ADDR 		0x94 //0x96 A0 to VCC , 0x94 A0 to GND
#define INT_STATUS 			0x00
#define INT_ENABLE 			0x01
#define CONFIG_REG 			0x02
#define HIGH_BYTE 			0x03
#define LOW_BYTE 			0x04
#define THRESH_HIGH 		0x05
#define THRESH_LOW 			0x06
#define THRESH_TIMER 		0x07

#define	SAMPLING_COUNT		5


static float s_fIlluminanceBuf[SAMPLING_COUNT];
	

MAX_STATUS MaxStatus;	//传感器状态

unsigned char s_MAXGetFlag = FALSE;


static float s_Ill_Val = 0;

float Get_Ill_Value(void)
{
	return s_Ill_Val;
}

void StartMAXSensor(void)
{
	if(MaxStatus != MAX_ERROR)
	{
		LEDNoWork();
		
		delay_ms(20);
		
		s_MAXGetFlag = TRUE;
		
		MaxStatus = MAX_WORKING;
	}
}

static u8 MAX44009_Read_Lux(u8 *HighData, u8 *LowData)
{
	u8  ack = 0;
	IIC_Start();  
	IIC_WriteByte(MAX44009_ADDR | CMD_MAX_WRITE); 
	ack = IIC_WaitACK(); 
	if(ack)
		return 1;
	IIC_WriteByte(HIGH_BYTE);
	ack = IIC_WaitACK(); 

	IIC_Start();  
	IIC_WriteByte(MAX44009_ADDR | CMD_MAX_READ); 
	ack = IIC_WaitACK();	

	*HighData = IIC_ReadByte(TRUE);

	IIC_Start();  
	IIC_WriteByte(LOW_BYTE);
	ack = IIC_WaitACK(); 

	*LowData = IIC_ReadByte(FALSE);
	
	IIC_Stop();
	return 0;
}

static u8 MAX44009_Write(u8 Reg, u8 data)
{
	u8 err = 1;
	IIC_Start();
	IIC_WriteByte(MAX44009_ADDR | CMD_MAX_WRITE);
	err = IIC_WaitACK();
	if(err)
		return 1;
	
	IIC_WriteByte(Reg);
	err = IIC_WaitACK();
	if(err)
		return 1;

	IIC_WriteByte(data);
	err = IIC_WaitACK();
	if(err)
		return 1;
	
	IIC_Stop();
	return 0;
}

/********************************************************************************/
/********************************************************************************/

static float MAX44009_GetValue(void)
{
	uint8 LowByte = 0, HighByte = 0, Exponent, Mantissa, err;
	float Result;
	
	err = MAX44009_Read_Lux(&HighByte, &LowByte);
	if(err)
	{
		return ERROR_VALUE;
	}
//	delay_ms(10);

	Exponent = (HighByte & 0xF0) >> 4;
	if(Exponent == 0xF0)
	{
		u1_printf("[MAX44009] Over Range\r\n");
		return ERROR_VALUE;
	}
	Mantissa = (HighByte & 0x0F) << 4;

	Mantissa += LowByte & 0x0F; 

	Result = Mantissa * (1 << Exponent) * 0.045;
	return Result;	
}

static void TaskForMAX(void)
{
	float f_Ill_Value;
	static u8  s_DetCount = 0, s_DetNum = 0;
	uint32 error_mark = 0xEEEEEEEE;
	
	if(s_MAXGetFlag == TRUE)
	{
		s_MAXGetFlag = FALSE;
		MaxStatus = MAX_WORKING;
		f_Ill_Value = MAX44009_GetValue();
				
		if(f_Ill_Value == ERROR_VALUE)
		{
			s_Ill_Val = ERROR_VALUE;	
			MaxStatus = MAX_ERROR;
			memcpy((uint8 *)&s_Ill_Val, (uint8 *)&error_mark, sizeof(uint32));
//			u1_printf("\r\n Error Ill Data.\r\n");
		}
		else
		{
			if(f_Ill_Value < 3000)
			{
				f_Ill_Value = f_Ill_Value*DEFAULT_MAX_RATIO;
			}
			else
			{
				f_Ill_Value = (f_Ill_Value)*(DEFAULT_MAX_RATIO + 0.2) + 866;
			}
			
			if(s_DetCount < 3)
			{
				s_fIlluminanceBuf[s_DetCount] = f_Ill_Value;
				s_Ill_Val = f_Ill_Value;
			}
			else
			{
				s_fIlluminanceBuf[s_DetNum] = f_Ill_Value;
				s_Ill_Val = Mid_Filter(s_fIlluminanceBuf, s_DetCount);
			}
			
			s_DetCount++;
			s_DetNum++;
			
			if(s_DetCount >= SAMPLING_COUNT)
			{
				s_DetCount = SAMPLING_COUNT;
			}
			
			if(s_DetNum >= SAMPLING_COUNT)
			{
				s_DetNum = 0;
			}
						
//			u1_printf("\r\n Ill:%.1fLux Avg:%.1fLux\r\n", f_Ill_Value, s_Ill_Val);
			
		}	
		MaxStatus = MAX_IDLE;
	}
}

unsigned char MAX44009_Init(void)
{
	u8 err = 0;
	uint32 error_mark = 0xEEEEEEEE;
	float  illuminance = 0.0;
	
	MaxStatus = MAX_ERROR;
	IIC_Init();
	
	delay_ms(50); 
	
    err = MAX44009_Write(CONFIG_REG, 0x88);
	
	if(err)
	{
		u1_printf("\r\n [MAX44009]Init Fail\r\n");
		memcpy((uint8 *)&s_Ill_Val, (uint8 *)&error_mark, sizeof(uint32));
		MaxStatus = MAX_ERROR;
		s_Ill_Val = -1;
	}
	else
	{		
		delay_ms(200);
		illuminance = MAX44009_GetValue();
		
		if(illuminance > 300000)
		{
			u1_printf("\r\n [MAX44009]Data Err,Init Fail\r\n");
			memcpy((uint8 *)&s_Ill_Val, (uint8 *)&error_mark, sizeof(uint32));
			MaxStatus = MAX_ERROR;
			s_Ill_Val = -1;
		}
		else
		{
			illuminance = illuminance*DEFAULT_MAX_RATIO;
			s_Ill_Val = illuminance;
			u1_printf("\r\n [MAX44009]illuminance:%f\r\n", illuminance);
			MaxStatus = MAX_IDLE;
			Task_Create(TaskForMAX, 40);
		}
	}

	return err;
}

