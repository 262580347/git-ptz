#ifndef	_MAX44009_H_
#define	_MAX44009_H_

#define		DEFAULT_MAX_RATIO		1.70
typedef enum
{
	MAX_WORKING 	= 0,//???
	MAX_ERROR  		= 1,//?????
	MAX_IDLE 	 	= 2,//?????
	MAX_OTHER    	= 3,//????
}MAX_STATUS;

float Get_Ill_Value(void);

unsigned char MAX44009_Init(void);

void StartMAXSensor(void);

extern MAX_STATUS MaxStatus;

#endif

