#ifndef		_TFT_LCD_H_
#define		_TFT_LCD_H_

#include "general_type.h"

//#define	u8 unsigned char
//#define	u32 unsigned int
//#define	u32 unsigned long
	
//#define		LCD_CLK_PIN		GPIO_Pin_10
//#define		LCD_CLK_TYPE	GPIOB

//#define		LCD_MOSI_PIN	GPIO_Pin_10
//#define		LCD_MOSI_TYPE	GPIOB

//#define		LCD_RES_PIN		GPIO_Pin_10
//#define		LCD_RES_TYPE	GPIOB

//#define		LCD_DC_PIN		GPIO_Pin_10
//#define		LCD_DC_TYPE		GPIOA

//#define		LCD_BLK_PIN		GPIO_Pin_10
//#define		LCD_BLK_TYPE	GPIOA

//#define		LCD_MISO_PIN	GPIO_Pin_10
//#define		LCD_MISO_TYPE	GPIOA

//#define		LCD_CS1_PIN		GPIO_Pin_10
//#define		LCD_CS1_TYPE	GPIOA

//#define		LCD_CS2_PIN		GPIO_Pin_10
//#define		LCD_CS2_TYPE	GPIOA

#define USE_HORIZONTAL 2  //���ú�������������ʾ 0��1Ϊ���� 2��3Ϊ����


#if USE_HORIZONTAL==0 || USE_HORIZONTAL==1
#define LCD_W 320
#define LCD_H 480

#else
#define LCD_W 480
#define LCD_H 320
#endif

   						  
//-----------------����LED�˿ڶ���---------------- 

#define LED_ON GPIO_ResetBits(GPIOB,GPIO_Pin_6)
#define LED_OFF GPIO_SetBits(GPIOB,GPIO_Pin_6)

//-----------------OLED�˿ڶ���---------------- 

#define OLED_SCLK_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_5)	//SCLK
#define OLED_SCLK_Set() GPIO_SetBits(GPIOB,GPIO_Pin_5)

#define OLED_SDIN_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_4)//DIN
#define OLED_SDIN_Set() GPIO_SetBits(GPIOB,GPIO_Pin_4)

#define OLED_RST_Clr() 	GPIO_ResetBits(GPIOB,GPIO_Pin_3)//RES
#define OLED_RST_Set() 	GPIO_SetBits(GPIOB,GPIO_Pin_3)

#define OLED_DC_Clr() 	GPIO_ResetBits(GPIOA,GPIO_Pin_15)//DC
#define OLED_DC_Set() 	GPIO_SetBits(GPIOA,GPIO_Pin_15)

#define OLED_BLK_Clr()  GPIO_ResetBits(GPIOA,GPIO_Pin_14)//BLK
#define OLED_BLK_Set()  GPIO_SetBits(GPIOA,GPIO_Pin_14)

#define ZK_OUT()    	GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_13)//MISO  ��ȡ�ֿ���������

#define OLED_CS_Clr()  	GPIO_ResetBits(GPIOA,GPIO_Pin_12)//CS1 SPIƬѡ
#define OLED_CS_Set()  	GPIO_SetBits(GPIOA,GPIO_Pin_12)

//----------------------------------------------------------------------------
#define ZK_CS_Clr()  	GPIO_ResetBits(GPIOA,GPIO_Pin_11)//CS2 �ֿ�Ƭѡ
#define ZK_CS_Set()  	GPIO_SetBits(GPIOA,GPIO_Pin_11)					
	

//--------------------------------------------------------------------------------- 

#define OLED_CMD  0	//д����
#define OLED_DATA 1	//д����

void LCD_TFT_Init(void);

extern  u32 BACK_COLOR;   //����ɫ

void LCD_Writ_Bus(u8 dat);
void LCD_WR_DATA8(u8 dat);
void LCD_WR_DATA(u32 dat);
void LCD_WR_REG(u8 dat);
void LCD_Address_Set(u32 x1,u32 y1,u32 x2,u32 y2);
void Lcd_Init(void); 
void LCD_Clear(u32 Color);
void LCD_ShowChinese32x32(u32 x,u32 y,u8 index,u8 size,u32 color);
void LCD_DrawPoint(u32 x,u32 y,u32 color);
void LCD_DrawPoint_big(u32 x,u32 y,u32 colory);
void LCD_Fill(u32 xsta,u32 ysta,u32 xend,u32 yend,u32 color);
void LCD_DrawLine(u32 x1,u32 y1,u32 x2,u32 y2,u32 color);
void LCD_DrawRectangle(u32 x1, u32 y1, u32 x2, u32 y2,u32 color);
void Draw_Circle(u32 x0,u32 y0,u8 r,u32 color);
void LCD_ShowChar(u32 x,u32 y,u8 num,u8 mode,u32 color);
void LCD_ShowString(u32 x,u32 y,const u8 *p,u32 color);
u32 mypow(u8 m,u8 n);
void LCD_ShowNum(u32 x,u32 y,u32 num,u8 len,u32 color);
void LCD_ShowNum1(u32 x,u32 y,float num,u8 len,u32 color);
void LCD_ShowPicture(u32 x1,u32 y1,u32 x2,u32 y2);

void CL_Mem(void);
void ZK_command(u8 dat);
u8  get_data_from_ROM(void);
void get_n_bytes_data_from_ROM(u8 AddrHigh,u8 AddrMid,u8 AddrLow,u8 *pBuff,u8 DataLen);
void Display_GB2312(u32 x,u32 y,u8 zk_num,u32 color);
void Display_GB2312_String(u32 x,u32 y,u8 zk_num,u8 text[],u32 color);
void Display_Asc(u32 x,u32 y,u8 zk_num,u32 color);
void Display_Asc_String(u32 x,u8 y,u32 zk_num,u8 text[],u32 color);


//������ɫ
#define WHITE         	 0xFCFCFC
#define BLACK            0X000000
#define RED           	 0xFC0000
#define GREEN            0x00FC00
#define BLUE             0x0000FC


#endif


