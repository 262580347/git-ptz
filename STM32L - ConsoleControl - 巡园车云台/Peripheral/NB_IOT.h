#ifndef		_NB_IOT_H_
#define		_NB_IOT_H_

#include "stm32l1xx_usart.h"


#define		NB_EN_PIN		GPIO_Pin_14
#define		NB_EN_TYPE		GPIOB

#define		NB_POWERKEY_PIN			GPIO_Pin_11
#define		NB_POWERKEY_TYPE		GPIOA

#define		NB_WAKEUP_PIN			GPIO_Pin_12
#define		NB_WAKEUP_TYPE			GPIOA

#define		NB_RESET_PIN			GPIO_Pin_13
#define		NB_RESET_TYPE			GPIOA

#define		NB_POWER_ON()			GPIO_SetBits(NB_EN_TYPE, NB_EN_PIN)
#define		NB_POWER_OFF()			GPIO_ResetBits(NB_EN_TYPE, NB_EN_PIN)

#define		NB_POWERKEY_HIGH()		GPIO_ResetBits(NB_POWERKEY_TYPE, NB_POWERKEY_PIN)
#define		NB_POWERKEY_LOW()		GPIO_SetBits(NB_POWERKEY_TYPE, NB_POWERKEY_PIN)

#define		NB_WAKEUP_HIGH()		GPIO_ResetBits(NB_WAKEUP_TYPE, NB_WAKEUP_PIN)
#define		NB_WAKEUP_LOW()			GPIO_SetBits(NB_WAKEUP_TYPE, NB_WAKEUP_PIN)

#define		NB_RESET_HIGH()			GPIO_ResetBits(NB_RESET_TYPE, NB_RESET_PIN)
#define		NB_RESET_LOW()			GPIO_SetBits(NB_RESET_TYPE, NB_RESET_PIN)


typedef enum
{
	NotConnected = 1,
	Connecting = 2,
	Connected = 3,
	Unattach = 4,
	Attach = 5,
}NB_CONNECTSTATUS;

typedef struct
{
	unsigned char SendAliveFlag;
	unsigned char AliveAckFlag;
	unsigned char DataAckFlag;
	unsigned char StatusAckFlag;
	unsigned char GetConfigFlag;
	unsigned char SetConfigFlag;
	unsigned char ServerConfigCmdFlag;
	unsigned char EnterSleepFlag;
	unsigned char STM32StopFlag;
	unsigned char NB_InitFlag;
}NB_RECSTATUS;

void NBIOT_Process(void);

void NBIOT_Process2(void);

void NB_Send_Alive(USART_TypeDef* USARTx);

void NB_Reset(void);

void NB_Wakeup(void);

void NB_PowerKey(void);

void NB_PortInit(void);

NB_RECSTATUS GetNB_RecStatus(void);

#endif










