#ifndef		_CONSOLECONTROL_H_
#define		_CONSOLECONTROL_H_

#define		TIME_CCR_VAL		2400		//PWM输出频率，控制电机转速，数值越大转动越慢

#define		MAX_HOR_PLUSE		26000	//26000	满量程 		390 Pluse/度	0~360 度
#define		MAX_VER_PLUSE		10000	//18285	满量程		305 Pluse/度	0~60  度

#define		HOR_ANGLETOPLUSE	72u
#define		VER_ANGLETOPLUSE	72u

#define		MAX_PLUSE 			6000
#define		HOR_PLUSE			6000
#define		VER_PLUSE			6000

#define		MOTOR_RUN_UP 		1
#define		MOTOR_RUN_DOWN  	2
#define		MOTOR_RUN_LEFT 	 	3
#define		MOTOR_RUN_RIGHT    	4
#define		MOTOR_STOP			5
#define		MOTOR_AUTO			6
#define		MOTOR_ANGLE			7
#define		MOTOR_STOP_ACK		8
#define		MOTOR_INIT			9		//云台位于初始点

#define		MOTOR_RUN_MODE		55

#define		HORIZONTAL_DIR_PIN			GPIO_Pin_6	//水平方向电机

#define		HORIZONTAL_EN_PIN			GPIO_Pin_7

#define		HORIZONTAL_RST_PIN			GPIO_Pin_8

#define		HORIZONTAL_SLEEP_PIN		GPIO_Pin_9

#define		VERTICAL_EN_PIN				GPIO_Pin_12

#define		VERTICAL_DIR_PIN			GPIO_Pin_13	//垂直方向电机

#define		VERTICAL_RST_PIN			GPIO_Pin_14

#define		VERTICAL_SLEEP_PIN			GPIO_Pin_15

#define		CONSOLE_CONTROL_TYPE		GPIOB

#define		HOR_MOTOR_ENABLE()			GPIO_ResetBits(CONSOLE_CONTROL_TYPE,HORIZONTAL_EN_PIN)
#define		HOR_MOTOR_DISABLE()			GPIO_SetBits(CONSOLE_CONTROL_TYPE,HORIZONTAL_EN_PIN)

#define		VER_MOTOR_ENABLE()			GPIO_ResetBits(CONSOLE_CONTROL_TYPE,VERTICAL_EN_PIN);
#define		VER_MOTOR_DISABLE()			GPIO_SetBits(CONSOLE_CONTROL_TYPE,VERTICAL_EN_PIN); 

#define		HOR_MOTOR_SLEEP()			GPIO_ResetBits(CONSOLE_CONTROL_TYPE,HORIZONTAL_SLEEP_PIN)
#define		HOR_MOTOR_NOSLEEP()			GPIO_SetBits(CONSOLE_CONTROL_TYPE,HORIZONTAL_SLEEP_PIN)

#define		VER_MOTOR_SLEEP()			GPIO_ResetBits(CONSOLE_CONTROL_TYPE,VERTICAL_SLEEP_PIN);
#define		VER_MOTOR_NOSLEEP()		GPIO_SetBits(CONSOLE_CONTROL_TYPE,VERTICAL_SLEEP_PIN); 

#define		HOR_DIR_LEFT()				GPIO_SetBits(CONSOLE_CONTROL_TYPE,HORIZONTAL_DIR_PIN);
#define		HOR_DIR_RIGHT()				GPIO_ResetBits(CONSOLE_CONTROL_TYPE,HORIZONTAL_DIR_PIN);

#define		VER_DIR_UP()				GPIO_SetBits(CONSOLE_CONTROL_TYPE,VERTICAL_DIR_PIN);
#define		VER_DIR_DOWN()				GPIO_ResetBits(CONSOLE_CONTROL_TYPE,VERTICAL_DIR_PIN);

#define		MOTOR_STEP_MS1_PIN			GPIO_Pin_4	//步进模式选择

#define		MOTOR_STEP_MS2_PIN			GPIO_Pin_5

#define		MOTOR_STEP_MS3_PIN			GPIO_Pin_6

#define		MOTOR_STEP_TYPE				GPIOA


void Timer3_PWMOutoutModeInit(unsigned short CCR_Value);

void Init_Motor_Port(void);

void SetPWMPluse(unsigned int Pluse);

unsigned char GetMotorRunFlag(void);
void SetMotorStatus(unsigned char Status);

void MotorControl_Stop(void);
void MotorControl_Up(unsigned int Pluse);
void MotorControl_Down(unsigned int Pluse);
void MotorControl_Left(unsigned int Pluse);
void MotorControl_Right(unsigned int Pluse);

unsigned int GetNowHorizontalPluse(void);
unsigned int GetNowVerticalPluse(void);
	
void Console_Control_Init(void);

void ReadStoreAngle(void);

//void MotorAutoRun(void);

void MotorControl_GotoAngle(unsigned int Hor_Angle, unsigned int Ver_Angle);

unsigned char GetMotorStatus(void);

void MotorRunModeInit(void);

void WriteMotorRunMode(unsigned char RunMode);

unsigned char ReadMotorRunMode(void);
	
void Motor_GotoSetAngle(unsigned int Hor, unsigned int Ver);	//实际角度;
		
unsigned char GetMotorAutoRunFlag(void);
		
void SetNowHorizontalPluse(unsigned int Pluse);

void SetNowVerticalPluse(unsigned int Pluse);

void Motor1_PWM_Control(void);
	
void Motor2_PWM_Control(void);
#endif

