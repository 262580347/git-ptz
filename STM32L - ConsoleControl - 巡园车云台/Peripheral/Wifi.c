#include "Wifi.h"
#include "main.h"

#define COM_DATA_SIZE	256

#define		WIFI_POWER_PIN		GPIO_Pin_12
#define		WIFI_POWER_TYPE		GPIOB

#define		WIFI_POWER_ON()		GPIO_SetBits(WIFI_POWER_TYPE, WIFI_POWER_PIN)
#define		WIFI_POWER_OFF()	GPIO_ResetBits(WIFI_POWER_TYPE, WIFI_POWER_PIN)

static TCCOMMRXOBSER s_TCRxObser = NULL; 

typedef __packed struct
{
	unsigned char 			magic; 	//0x55
	unsigned short 			length;	//存储内容长度	
	unsigned char 			chksum;	//校验和
	unsigned char 			data[100];		//设备ID
}WIFIPASSWORD;

static char s_WlanName[64];		//WLAN用户名

static char s_WlanPassword[64];	//WLAN密码

void WiFi_Port_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB , ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = WIFI_POWER_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(WIFI_POWER_TYPE, &GPIO_InitStructure);	
		
	WIFI_POWER_OFF();
	
	delay_ms(1000);
	
	WIFI_POWER_ON();
	
	delay_ms(500);
}

static void ReadWlanName(void)
{
	u8 Name_Length, i;
	
	Name_Length = Check_Area_Valid(WIFI_USER_ADDR);
	if (Name_Length)
	{
		EEPROM_ReadBytes(WIFI_USER_ADDR, (u8 *)s_WlanName, sizeof(SYS_TAG) +  Name_Length);
		u1_printf("\r\n Name:");
		for(i=sizeof(SYS_TAG); i<sizeof(SYS_TAG) + Name_Length; i++)
		{
			u1_printf("%c", s_WlanName[i]);
		}
		u1_printf("\r\n");
	}
}

static void ReadWlanPassword(void)
{
	u8 Password_Length, i;
	char VolBuf[300];
	
	Password_Length = Check_Area_Valid(WIFI_PASSWORD_ADDR);
	if (Password_Length)
	{
		EEPROM_ReadBytes(WIFI_PASSWORD_ADDR, (u8 *)s_WlanPassword, sizeof(SYS_TAG) +  Password_Length);
		u1_printf("\r\n Password:");
		for(i=sizeof(SYS_TAG); i<sizeof(SYS_TAG) + Password_Length; i++)
		{
			u1_printf("%c", s_WlanPassword[i]);
		}
		u1_printf("\r\n");
	}
}

static void TaskForWiFiCommunication(void)
{
	static u8 s_First = FALSE, WiFiStatus = 2, WiFiStep = 1, s_ErrCount = 0, s_RetryCount = 0, s_ErrAckCount = 0;
	static u8 s_CWLAPFirst = FALSE, s_WaitTime = 1;
	static unsigned int s_RTCSumSecond = 0, s_RTCReStart = 0;
	char strWlan[128], str[40];
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	p_sys = GetSystemConfig();
	
	if(!s_First)
	{
		if(strncmp((char *)g_USART3_RX_BUF, "compiled", g_USART3_RX_CNT) == 0)
		{
			u1_printf("%s\r\n", g_USART3_RX_BUF);
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			Clear_Uart3Buff();
		}		
		else 
		{		
			
		}
		LCD_ShowString(160,176,"Wifi Init...",BLUE);
		USART3_Config(115200);	
		s_First = TRUE;
		g_TCRecStatus.LinkStatus = NotConnected;
		s_RTCReStart = GetRTCSecond();
		s_RTCSumSecond = GetRTCSecond();
	}
	
	if(g_TCRecStatus.LinkStatus != TCPConnected)
	{
		if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCReStart) > 600)
		{
			if(GetLockinTimeFlag())
			{
				ClearLockinTimeFlag();
				s_RTCReStart = GetRTCSecond();
			}
			else
			{		
				u1_printf("\r\n 10min Unconnect,Reboot\r\n");
				SetLogErrCode(LOG_CODE_RESET);
				StoreOperationalData();
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				Sys_Soft_Reset();
			}
		}
	}
	else
	{
		s_RTCReStart = GetRTCSecond();
	}
	
	switch(WiFiStatus)
	{	
		case 0:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) > 10)
			{
				s_RTCSumSecond = GetRTCSecond();
				u1_printf(" Check ESP Connect...\r\n");
				LCD_ShowString(160,176,"Wifi connection failed",BLUE);
			}
		break;
		
		case 1:
			s_RetryCount++;
			if(s_RetryCount >= 3)
			{
				s_RetryCount = 0;
				WiFiStatus = 0;
				break;
			}
			SetTCProtocolRunFlag(FALSE);
			SetTCModuleReadyFlag(FALSE);
			WiFi_Init();	
			
			WiFiStatus++;
			WiFiStep = 1;
			s_RTCSumSecond = GetRTCSecond();
		break;
				
		case 2:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) > 1)
			{
				WiFiStatus++;
				WiFiStep = 1;
				s_RTCSumSecond = GetRTCSecond();
			}
		break;
		
		case 3:

				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			

				switch(WiFiStep)
				{
					case 1:
						s_WaitTime = 1;
						u3_printf("AT\r\n");
					break;
					case 2:
						if(!s_CWLAPFirst)
						{
							s_WaitTime = 2;
							s_CWLAPFirst = TRUE;
							u3_printf("AT+GMR\r\n");
							break;
						}
						else
						{
							s_WaitTime = 1;
							WiFiStep = 3;
						}

					case 3:
						s_WaitTime = 1;
						u3_printf("AT+CWMODE=1\r\n");
					break;
					
					case 4:
						u3_printf("AT+CWAUTOCONN=0\r\n");
					break;
					case 5:
						s_WaitTime = 8;
						LCD_ShowString(160,176,"Wifi Connecting...",BLUE);
						if(Check_Area_Valid(WIFI_USER_ADDR) && Check_Area_Valid(WIFI_PASSWORD_ADDR))
						{
							sprintf(strWlan, "AT+CWJAP=\"%s\",\"%s\"\r\n", &s_WlanName[4], &s_WlanPassword[4]);
							u3_printf("%s", strWlan);
						}
						else
						{
							u3_printf("AT+CWJAP=\"ESP8266WIFI\",\"12344321\"\r\n");
						}
					break;
						
					case 6:		//尝试连接默认热点
						s_WaitTime = 8;
						u3_printf("AT+CWJAP=\"ESP8266WIFI\",\"12344321\"\r\n");
					break;
					
					case 7:
						s_WaitTime = 1;
						u3_printf("AT+CIPMUX=0\r\n");//设置单链接

					break;
					
					case 8:
						u3_printf("AT+CIPMODE=1\r\n");

					break;
					
					case 9:
						u1_printf("[WIFI]Establishing TCP links\r\n");
						sprintf(str,"AT+CIPSTART=\"TCP\",\"%d.%d.%d.%d\",%d\r\n"
						,p_sys->Gprs_ServerIP[0]
						,p_sys->Gprs_ServerIP[1]
						,p_sys->Gprs_ServerIP[2]
						,p_sys->Gprs_ServerIP[3]
						,p_sys->Gprs_Port	);
					
//						u1_printf("%s", str);
						u3_printf("%s", str);
						s_WaitTime = 8;
					break;
					
					case 10:
						s_WaitTime = 2;
						u3_printf("AT+CIPSEND\r\n");

					break;		
					
					case 20:
						u3_printf("AT+CWQAP\r\n");

					break;
				}
				WiFiStatus++;
				s_RTCSumSecond = GetRTCSecond();
		break;
		
		case 4:
			
			if(s_ErrAckCount >= 5)
			{
				u1_printf(" Err Over\r\n");
				s_ErrAckCount = 0;
				WiFiStatus = 1;
				WiFiStep = 1;
				g_TCRecStatus.LinkStatus = NotConnected;
			}
			
			if(g_Uart3RxFlag == TRUE)	//收到一帧数据包
			{			
				if(g_USART3_RX_CNT >= 1050)
				{
					g_USART3_RX_BUF[1050] = 0;
					g_USART3_RX_CNT = 1050;							
				}
				
					u1_printf("%s\r\n", g_USART3_RX_BUF);
				
				switch(WiFiStep)
				{
					case 1:
					case 3:
					case 4:
						if((strstr((const char *)g_USART3_RX_BUF,"OK")) != NULL )
						{
							WiFiStep++;	
							WiFiStatus--;							
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"ERROR")) != NULL )
						{
							s_ErrAckCount++;
						}
					break;
					
					case 2:
						if((strstr((const char *)g_USART3_RX_BUF,"OK")) != NULL )
						{
							WiFiStep++;	
							WiFiStatus--;	
						}
						
					break;
					
					case 5:	//连接存储的地址
						if((strstr((const char *)g_USART3_RX_BUF,"WIFI CONNECTED")) != NULL )
						{
							s_RTCSumSecond = GetRTCSecond();
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"WIFI GOT IP")) != NULL )
						{
							s_RTCSumSecond = GetRTCSecond();
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"OK")) != NULL )
						{
							g_TCRecStatus.LinkStatus = GotIP;
							WiFiStep = 7;
							WiFiStatus--;	
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"WIFI DISCONNECT")) != NULL )
						{
							g_TCRecStatus.LinkStatus = NotConnected;
							u1_printf("\r\n WIFI Password Error\r\n");
							SetLogErrCode(LOG_CODE_WIFIERR);
							StoreOperationalData();
							WiFiStep = 6;
							WiFiStatus--;	
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"FAIL")) != NULL )
						{
							u1_printf(" Retry\r\n");
							WiFiStatus--;	
							s_ErrAckCount++;
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"ERROR")) != NULL )//收到ERROR代表用户或密码错误或不存在该热点，做其他处理
						{
							u1_printf(" No AP\r\n");
							WiFiStep = 6;
							WiFiStatus--;	
						}
						
					break;
					
					case 6:		//连接默认热点
						if((strstr((const char *)g_USART3_RX_BUF,"WIFI CONNECTED")) != NULL )
						{
							s_RTCSumSecond = GetRTCSecond();
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"WIFI GOT IP")) != NULL )
						{
							s_RTCSumSecond = GetRTCSecond();
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"OK")) != NULL )
						{
							g_TCRecStatus.LinkStatus = GotIP;
							WiFiStep = 7;
							WiFiStatus--;	
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"WIFI DISCONNECT")) != NULL )
						{
							g_TCRecStatus.LinkStatus = NotConnected;
							u1_printf("\r\n WIFI Password Error\r\n");
							WiFiStep = 5;
							s_ErrAckCount++;
							WiFiStatus--;	
							delay_ms(100);
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"FAIL")) != NULL )
						{
							u1_printf(" Retry\r\n");
							WiFiStep = 5;
							WiFiStatus--;	
							s_ErrAckCount++;
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"ERROR")) != NULL )//收到ERROR代表用户或密码错误或不存在该热点，做其他处理
						{
							u1_printf(" No AP\r\n");
							WiFiStep = 5;
							WiFiStep = 1;
							WiFiStatus = 1;	
						}
						
					break;
						
					case 7:
					case 8:	
						if((strstr((const char *)g_USART3_RX_BUF,"OK")) != NULL )
						{
							WiFiStep++;	
							WiFiStatus--;					
						}
					break;
						
					case 9:
						if((strstr((const char *)g_USART3_RX_BUF,"CONNECT")) != NULL )
						{
							WiFiStep++;	
							WiFiStatus--;							
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"ERROR")) != NULL )						
						{	
							WiFiStatus--;		
							delay_ms(500);
							s_ErrAckCount++;
						}
							
					break;
						
					case 10:
						if((strstr((const char *)g_USART3_RX_BUF,">")) != NULL )
						{
							WiFiStep = 1;	
							WiFiStatus++;								
						}
						else if((strstr((const char *)g_USART3_RX_BUF,"ERROR")) != NULL )						
						{	
							WiFiStatus--;		
							delay_ms(500);
							s_ErrAckCount++;
						}
					break;	
					
					if(s_ErrAckCount >= 10)
					{
						WiFiStatus = 1;
						WiFiStep = 1;
						s_ErrAckCount = 0;
					}
						
				}
				
				Clear_Uart3Buff();

			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= s_WaitTime)
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				WiFiStatus--;
				u1_printf(" AT No Ack\r\n");
				g_TCRecStatus.LinkStatus = NotConnected;
				s_ErrCount++;
				s_RTCSumSecond = GetRTCSecond();
				if(s_ErrCount >= 3)
				{
					s_ErrCount = 0;
					WiFiStatus = 1;
					WiFiStep = 1;
				}
			}
			
			
		break;
		
		case 5:
			u1_printf("\r\n Success to TCP Connect\r\n");
			SetTCProtocolRunFlag(TRUE);
			SetTCModuleReadyFlag(TRUE);
			WiFiStatus++;
			g_TCRecStatus.LinkStatus = TCPConnected;
			g_TCRecStatus.LastLinkTime = GetRTCSecond();
			LCD_ShowString(160,176,"Wifi Connected     ",BLUE);
		break;
		
		case 6:
			if(g_TCRecStatus.ResetFlag)
			{
				g_TCRecStatus.ResetFlag = FALSE;
				u1_printf(" \r\n WIFI Retry Connect\r\n");
				SetTCProtocolRunFlag(FALSE);
				SetTCModuleReadyFlag(FALSE);
				g_TCRecStatus.LinkStatus = NotConnected;
				s_ErrCount = 0;
				WiFiStatus = 1;
				WiFiStep = 1;
				s_RTCSumSecond = GetRTCSecond();
			}
			else if(GetUpdataTimeFlag() == FALSE)
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), g_TCRecStatus.LastLinkTime) > 120)
				{
					g_TCRecStatus.LastLinkTime = GetRTCSecond();
					g_TCRecStatus.ResetFlag = TRUE;
					u1_printf(" Link Timeout!\r\n");
				}
			}
			else if(GetUpdataTimeFlag())
			{
				SetUpdataTimeFlag(FALSE);
				g_TCRecStatus.LastLinkTime = GetRTCSecond();
			}
		break;
		
		default:
			
		break;
		
	}
	

}
static void DecodeAppMsg(uint8 *pBuf,uint16 uLen,TC_COMM_MSG *pMsg)
{
    u32 uPoint = 0;
	
    pMsg->Protocol = pBuf[uPoint++];
    pMsg->Protocol = (pMsg->Protocol<<8)|pBuf[uPoint++];
	
	pMsg->DeviceID = pBuf[uPoint++];
    pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	
	pMsg->Dir   = pBuf[uPoint++];
	
	pMsg->Seq = pBuf[uPoint++];
    pMsg->Seq = (pMsg->Seq<<8)|pBuf[uPoint++];
	
    pMsg->Length = pBuf[uPoint++];
    pMsg->Length = (pMsg->Length<<8)|pBuf[uPoint++];
	
    pMsg->OPType   = pBuf[uPoint++];
	
    if(pMsg->Length > TC_DATA_SIZE)
    {
	    pMsg->Length = TC_DATA_SIZE;
    }
	memcpy((void *)pMsg->UserBuf,(void *)&pBuf[uPoint],pMsg->Length);
}

static BOOL TCProtocol(TC_COMM_MSG *s_RxFrame)
{
	unsigned char PackBuff[100];
	uint16   PackLengthgth = 0, i;
	
	UnPackMsg(g_USART3_RX_BUF+1, g_USART3_RX_CNT-2, PackBuff, &PackLengthgth);	//解包

	if (PackBuff[(PackLengthgth)-1] == Calc_Checksum(PackBuff, (PackLengthgth)-1))
	{		
		DecodeAppMsg(PackBuff, PackLengthgth-1, s_RxFrame);
		return TRUE;
	}
	else
	{
		u1_printf("COM3:");
		for(i=0; i<PackLengthgth; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");
		u1_printf(" CRC Error :%02X\r\n", Calc_Checksum(PackBuff, (PackLengthgth)-1));
		return FALSE;
	}	
	
}
////协议栈0 应用报文接收观察者处理任务
//static void TaskForTCRxObser(void)
//{
//	static TC_COMM_MSG s_RxFrame;
//	u8 i;

//	if(g_Uart3RxFlag == TRUE)
//	{
//		if(g_USART3_RX_BUF[0] == BOF_VAL && g_USART3_RX_BUF[g_USART3_RX_CNT-1] == EOF_VAL && g_USART3_RX_CNT >= 15)
//		{
//			if(TCProtocol(&s_RxFrame))
//			{
//				if(s_TCRxObser!=NULL)
//				{
//					s_TCRxObser(&s_RxFrame);
//				}
//			}
//		}
//		else
//		{
//			for(i=0; i<g_USART3_RX_CNT; i++)
//			{
//				u1_printf("%02X ", g_USART3_RX_BUF[i]);
//			}
//			u1_printf("\r\n");
//		}

//		Clear_Uart3Buff();
//	}
//}

//void AddWiFiRxObser(TCCOMMRXOBSER hRxObser)
//{
//	if(hRxObser!=NULL)
//	{
//		s_TCRxObser = hRxObser;
//		Task_Create(TaskForTCRxObser,1);				
//	}
//}

void WiFi_Init(void)
{			
	ReadWlanName();
	
	ReadWlanPassword();			
	
	USART3_Config(115200);	
	
	WiFi_Port_Init();
	
	Task_Create(TaskForWiFiCommunication, 2000);
	
	return;
}

void RemoveWiFiCommunicationTask(void)
{
	Task_Kill(TaskForWiFiCommunication);
}
