#include "main.h"
#include "GPSDrive.h"


//NMEA数据包结构: $消息类型,data1,data2.....*CheckSum 回车+换行
#define		GPS_MSG_START		0x24	//$消息头
#define		GPS_DATA_END		0x2A	//*数据结束符
#define		GPS_MSG_END1		0x0D	//消息尾1
#define		GPS_MSG_END2		0x0A	//消息尾2

#define MAX_FAILED_TIMES		3		//最大无应答次数

#define		GET_GPS_COUNT		10		//获取10次经纬度数据

static float s_LatitudeBuf[GET_GPS_COUNT], s_LongitudeBuf[GET_GPS_COUNT], s_AltitudeBuf[GET_GPS_COUNT];

static float s_Latitude = 0, s_Longitude = 0, s_Altitude = 0;

static u8 s_GetGPSDataFlag = FALSE;	//是否获取到GPS数据

static u8 s_GPSPowerFlag = FALSE;	//GPS电源开启标记

unsigned char GetGPSDataFlag(void)
{
	return s_GetGPSDataFlag;
}

unsigned char GetGPSPowerFlag(void)
{
	return s_GPSPowerFlag;
}
//卫星信息
__packed typedef struct  
{										    
 	u8 Num;		//卫星编号
	u8 Eledeg;	//卫星仰角
	u16 Azideg;	//卫星方位角
	u8 SN;		//信噪比		   
}NMEA_SATELLITE_MSG; 
//北斗 NMEA-0183协议重要参数结构体定义 
//卫星信息
__packed typedef struct  
{	
 	u8 BD_Num;		//卫星编号
	u8 BD_Eledeg;	//卫星仰角
	u16 BD_Azideg;	//卫星方位角
	u8 BD_SN;		//信噪比		   
}NMEA_BD_SATELLITE_MSG; 

//UTC时间信息
__packed typedef struct  
{										    
 	u16 Year;	//年份
	u8 Month;	//月份
	u8 date;	//日期
	u8 Hour; 	//小时
	u8 Min; 	//分钟
	u8 Sec; 	//秒钟
}NMEA_UTC_TIME;   	   
//NMEA 0183 协议解析后数据存放结构体
__packed typedef struct  
{										    
 	u8 Satellite_Num;					//可见GPS卫星数
	u8 BD_Satellite_Num;					//可见GPS卫星数
	NMEA_SATELLITE_MSG 		Sl_Msg[12];		//最多12颗GPS卫星
	NMEA_BD_SATELLITE_MSG 	BD_Sl_Msg[12];		//暂且算最多12颗北斗卫星
	NMEA_UTC_TIME 			UTC_Time;			//UTC时间
	u32 Latitude;				//纬度 分扩大100000倍,实际要除以100000
	u8 N_S_Hemi;					//北纬/南纬,N:北纬;S:南纬				  
	u32 Longitude;			    //经度 分扩大100000倍,实际要除以100000
	u8 E_W_Hemi;					//东经/西经,E:东经;W:西经
	u8 GPS_Status;					//GPS状态:0,未定位;1,非差分定位;2,差分定位;6,正在估算.				  
 	u8 Position_Num;				//用于定位的GPS卫星数,0~12.
 	u8 Position_Satellite[12];				//用于定位的卫星编号
	u8 FixMode;					//定位类型:1,没有定位;2,2D定位;3,3D定位
	u16 Pdop;					//位置精度因子 0~500,对应实际值0~50.0
	u16 Hdop;					//水平精度因子 0~500,对应实际值0~50.0
	u16 Vdop;					//垂直精度因子 0~500,对应实际值0~50.0 

	int Altitude;			 	//海拔高度,放大了10倍,实际除以10.单位:0.1m	 
	u16 Speed;					//地面速率,放大了1000倍,实际除以10.单位:0.001公里/小时	 
}NMEA_MSG;

static NMEA_MSG s_GPS_Msg;		//GPS信息
static NMEA_MSG *p_GPS;

static BOOL  s_IsOverTime		= FALSE;	//主机响应超时标记
static BOOL	 s_IsRecGPSInfo     = FALSE;	//收到GPS参数信息
static BOOL  s_IsRecGPSAnt		= FALSE;	//GPS天线检测
static UCHAR s_FailedCounter	= 0;		//失败计数

static unsigned int TimerID 	= 0;		//定时器ID
static unsigned int OverTimerID = 0;		//超时定时器ID
NMEA_MSG * GetGPSData(void)
{
	return p_GPS;
}

float GetAltitude(void)
{
	return s_Altitude;
}

void GetPosition(float *Latitude, float *Longitude)
{
	*Latitude  = s_Latitude;
	*Longitude = s_Longitude;
}


void GPSSendMsg(unsigned char GPSCmd);


//从buf里面得到第cx个逗号所在的位置
//返回值:0~0XFE,代表逗号所在位置的偏移.
//       0XFF,代表不存在第cx个逗号							  
u8 NMEA_Comma_Pos(u8 *Buff,u8 cx)
{	 		    
	u8 *p=Buff;
	while(cx)
	{		 
		if(*Buff=='*'||*Buff<' '||*Buff>'z')return 0XFF;//遇到'*'或者非法字符,则不存在第cx个逗号
		if(*Buff==',')cx--;
		Buff++;
	}
	return Buff-p;	 
}
//m^n函数
//返回值:m^n次方.
u32 NMEA_Pow(u8 m,u8 n)
{
	u32 result=1;	 
	while(n--)result*=m;    
	return result;
}
//str转换为数字,以','或者'*'结束
//Buff:数字存储区
//dx:小数点位数,返回给调用函数
//返回值:转换后的数值
int NMEA_Str2num(u8 *Buff,u8*dx)
{
	u8 *p=Buff;
	u32 ires=0,fres=0;
	u8 ilen=0,flen=0,i;
	u8 mask=0;
	int res;
	while(1) //得到整数和小数的长度
	{
		if(*p=='-'){mask|=0X02;p++;}//是负数
		if(*p==','||(*p=='*'))break;//遇到结束了
		if(*p=='.'){mask|=0X01;p++;}//遇到小数点了
		else if(*p>'9'||(*p<'0'))	//有非法字符
		{	
			ilen=0;
			flen=0;
			break;
		}	
		if(mask&0X01)flen++;
		else ilen++;
		p++;
	}
	if(mask&0X02)Buff++;	//去掉负号
	for(i=0;i<ilen;i++)	//得到整数部分数据
	{  
		ires+=NMEA_Pow(10,ilen-1-i)*(Buff[i]-'0');
	}
	if(flen>5)flen=5;	//最多取5位小数
	*dx=flen;	 		//小数点位数
	for(i=0;i<flen;i++)	//得到小数部分数据
	{  
		fres+=NMEA_Pow(10,flen-1-i)*(Buff[ilen+1+i]-'0');
	} 
	res=ires*NMEA_Pow(10,flen)+fres;
	if(mask&0X02)res=-res;		   
	return res;
}	  							 
//分析GPGSV信息
//GPS_Msg:nmea信息结构体
//Buff:接收到的GPS数据缓冲区首地址
void NMEA_GPGSV_Analysis(NMEA_MSG *GPS_Msg,u8 *Buff)
{
	u8 *p,*p1,dx;
	u8 Length,i,j,slx=0;
	u8 Posx;   	 
	p=Buff;
	p1=(u8*)strstr((const char *)p,"$GPGSV");
	Length=p1[7]-'0';								//得到GPGSV的条数
	Posx=NMEA_Comma_Pos(p1,3); 					//得到可见卫星总数
	if(Posx!=0XFF)GPS_Msg->Satellite_Num=NMEA_Str2num(p1+Posx,&dx);
	for(i=0;i<Length;i++)
	{	 
		p1=(u8*)strstr((const char *)p,"$GPGSV");  
		for(j=0;j<4;j++)
		{	  
			Posx=NMEA_Comma_Pos(p1,4+j*4);
			if(Posx!=0XFF)GPS_Msg->Sl_Msg[slx].Num=NMEA_Str2num(p1+Posx,&dx);	//得到卫星编号
			else break; 
			Posx=NMEA_Comma_Pos(p1,5+j*4);
			if(Posx!=0XFF)GPS_Msg->Sl_Msg[slx].Eledeg=NMEA_Str2num(p1+Posx,&dx);//得到卫星仰角 
			else break;
			Posx=NMEA_Comma_Pos(p1,6+j*4);
			if(Posx!=0XFF)GPS_Msg->Sl_Msg[slx].Azideg=NMEA_Str2num(p1+Posx,&dx);//得到卫星方位角
			else break; 
			Posx=NMEA_Comma_Pos(p1,7+j*4);
			if(Posx!=0XFF)GPS_Msg->Sl_Msg[slx].SN=NMEA_Str2num(p1+Posx,&dx);	//得到卫星信噪比
			else break;
			slx++;	   
		}   
 		p=p1+1;//切换到下一个GPGSV信息
	}   
}
//分析BDGSV信息
//GPS_Msg:nmea信息结构体
//Buff:接收到的GPS数据缓冲区首地址
void NMEA_BDGSV_Analysis(NMEA_MSG *GPS_Msg,u8 *Buff)
{
	u8 *p,*p1,dx;
	u8 Length,i,j,slx=0;
	u8 Posx;   	 
	p=Buff;
	p1=(u8*)strstr((const char *)p,"$BDGSV");
	Length=p1[7]-'0';								//得到BDGSV的条数
	Posx=NMEA_Comma_Pos(p1,3); 					//得到可见北斗卫星总数
	if(Posx!=0XFF)GPS_Msg->BD_Satellite_Num=NMEA_Str2num(p1+Posx,&dx);
	for(i=0;i<Length;i++)
	{	 
		p1=(u8*)strstr((const char *)p,"$BDGSV");  
		for(j=0;j<4;j++)
		{	  
			Posx=NMEA_Comma_Pos(p1,4+j*4);
			if(Posx!=0XFF)GPS_Msg->BD_Sl_Msg[slx].BD_Num=NMEA_Str2num(p1+Posx,&dx);	//得到卫星编号
			else break; 
			Posx=NMEA_Comma_Pos(p1,5+j*4);
			if(Posx!=0XFF)GPS_Msg->BD_Sl_Msg[slx].BD_Eledeg=NMEA_Str2num(p1+Posx,&dx);//得到卫星仰角 
			else break;
			Posx=NMEA_Comma_Pos(p1,6+j*4);
			if(Posx!=0XFF)GPS_Msg->BD_Sl_Msg[slx].BD_Azideg=NMEA_Str2num(p1+Posx,&dx);//得到卫星方位角
			else break; 
			Posx=NMEA_Comma_Pos(p1,7+j*4);
			if(Posx!=0XFF)GPS_Msg->BD_Sl_Msg[slx].BD_SN=NMEA_Str2num(p1+Posx,&dx);	//得到卫星信噪比
			else break;
			slx++;	   
		}   
 		p=p1+1;//切换到下一个BDGSV信息
	}   
}
//分析GNGGA信息
//GPS_Msg:nmea信息结构体
//Buff:接收到的GPS数据缓冲区首地址
void NMEA_GNGGA_Analysis(NMEA_MSG *GPS_Msg,u8 *Buff)
{
	u8 *p1,dx;			 
	u8 Posx;    
	p1=(u8*)strstr((const char *)Buff,"$GNGGA");
	Posx=NMEA_Comma_Pos(p1,6);								//得到GPS状态
	if(Posx!=0XFF)GPS_Msg->GPS_Status=NMEA_Str2num(p1+Posx,&dx);	
	Posx=NMEA_Comma_Pos(p1,7);								//得到用于定位的卫星数
	if(Posx!=0XFF)GPS_Msg->Position_Num=NMEA_Str2num(p1+Posx,&dx); 
	Posx=NMEA_Comma_Pos(p1,9);								//得到海拔高度
	if(Posx!=0XFF)GPS_Msg->Altitude=NMEA_Str2num(p1+Posx,&dx);  
}
//分析GNGSA信息
//GPS_Msg:nmea信息结构体
//Buff:接收到的GPS数据缓冲区首地址
void NMEA_GNGSA_Analysis(NMEA_MSG *GPS_Msg,u8 *Buff)
{
	u8 *p1,dx;			 
	u8 Posx; 
	u8 i;   
	p1=(u8*)strstr((const char *)Buff,"$GNGSA");
	Posx=NMEA_Comma_Pos(p1,2);								//得到定位类型
	if(Posx!=0XFF)GPS_Msg->FixMode=NMEA_Str2num(p1+Posx,&dx);	
	for(i=0;i<12;i++)										//得到定位卫星编号
	{
		Posx=NMEA_Comma_Pos(p1,3+i);					 
		if(Posx!=0XFF)GPS_Msg->Position_Satellite[i]=NMEA_Str2num(p1+Posx,&dx);
		else break; 
	}				  
	Posx=NMEA_Comma_Pos(p1,15);								//得到PDOP位置精度因子
	if(Posx!=0XFF)GPS_Msg->Pdop=NMEA_Str2num(p1+Posx,&dx);  
	Posx=NMEA_Comma_Pos(p1,16);								//得到HDOP位置精度因子
	if(Posx!=0XFF)GPS_Msg->Hdop=NMEA_Str2num(p1+Posx,&dx);  
	Posx=NMEA_Comma_Pos(p1,17);								//得到VDOP位置精度因子
	if(Posx!=0XFF)GPS_Msg->Vdop=NMEA_Str2num(p1+Posx,&dx);  
}
//分析GNRMC信息
//GPS_Msg:nmea信息结构体
//Buff:接收到的GPS数据缓冲区首地址
void NMEA_GNRMC_Analysis(NMEA_MSG *GPS_Msg,u8 *Buff)
{
	u8 *p1,dx;			 
	u8 Posx;     
	u32 Temp;	   
	float rs;  
	p1=(u8*)strstr((const char *)Buff,"$GNRMC");//"$GNRMC",经常有&和GNRMC分开的情况,故只判断GPRMC.
	Posx=NMEA_Comma_Pos(p1,1);								//得到UTC时间
	if(Posx!=0XFF)
	{
		Temp=NMEA_Str2num(p1+Posx,&dx)/NMEA_Pow(10,dx);	 	//得到UTC时间,去掉ms
		GPS_Msg->UTC_Time.Hour=Temp/10000;
		GPS_Msg->UTC_Time.Min=(Temp/100)%100;
		GPS_Msg->UTC_Time.Sec=Temp%100;	 	 
	}	
	Posx=NMEA_Comma_Pos(p1,3);								//得到纬度
	if(Posx!=0XFF)
	{
		Temp=NMEA_Str2num(p1+Posx,&dx);		 	 
		GPS_Msg->Latitude=Temp/NMEA_Pow(10,dx+2);	//得到°
		rs=Temp%NMEA_Pow(10,dx+2);				//得到'		 
		GPS_Msg->Latitude=GPS_Msg->Latitude*NMEA_Pow(10,5)+(rs*NMEA_Pow(10,5-dx))/60;//转换为° 
	}
	Posx=NMEA_Comma_Pos(p1,4);								//南纬还是北纬 
	if(Posx!=0XFF)GPS_Msg->N_S_Hemi=*(p1+Posx);					 
 	Posx=NMEA_Comma_Pos(p1,5);								//得到经度
	if(Posx!=0XFF)
	{												  
		Temp=NMEA_Str2num(p1+Posx,&dx);		 	 
		GPS_Msg->Longitude=Temp/NMEA_Pow(10,dx+2);	//得到°
		rs=Temp%NMEA_Pow(10,dx+2);				//得到'		 
		GPS_Msg->Longitude=GPS_Msg->Longitude*NMEA_Pow(10,5)+(rs*NMEA_Pow(10,5-dx))/60;//转换为° 
	}
	Posx=NMEA_Comma_Pos(p1,6);								//东经还是西经
	if(Posx!=0XFF)GPS_Msg->E_W_Hemi=*(p1+Posx);		 
	Posx=NMEA_Comma_Pos(p1,9);								//得到UTC日期
	if(Posx!=0XFF)
	{
		Temp=NMEA_Str2num(p1+Posx,&dx);		 				//得到UTC日期
		GPS_Msg->UTC_Time.date=Temp/10000;
		GPS_Msg->UTC_Time.Month=(Temp/100)%100;
		GPS_Msg->UTC_Time.Year=2000+Temp%100;	 	 
	} 
}
//分析GNVTG信息
//GPS_Msg:nmea信息结构体
//Buff:接收到的GPS数据缓冲区首地址
void NMEA_GNVTG_Analysis(NMEA_MSG *GPS_Msg,u8 *Buff)
{
	u8 *p1,dx;			 
	u8 Posx;    
	p1=(u8*)strstr((const char *)Buff,"$GNVTG");							 
	Posx=NMEA_Comma_Pos(p1,7);								//得到地面速率
	if(Posx!=0XFF)
	{
		GPS_Msg->Speed=NMEA_Str2num(p1+Posx,&dx);
		if(dx<3)GPS_Msg->Speed*=NMEA_Pow(10,3-dx);	 	 		//确保扩大1000倍
	}
}  
//提取NMEA-0183信息
//GPS_Msg:nmea信息结构体
//Buff:接收到的GPS数据缓冲区首地址
void GPS_Analysis(NMEA_MSG *GPS_Msg,u8 *Buff)
{
	NMEA_GPGSV_Analysis(GPS_Msg,Buff);	//GPGSV解析
	NMEA_BDGSV_Analysis(GPS_Msg,Buff);	//BDGSV解析
	NMEA_GNGGA_Analysis(GPS_Msg,Buff);	//GNGGA解析 	
	NMEA_GNGSA_Analysis(GPS_Msg,Buff);	//GPNSA解析
	NMEA_GNRMC_Analysis(GPS_Msg,Buff);	//GPNMC解析
	NMEA_GNVTG_Analysis(GPS_Msg,Buff);	//GPNTG解析
}


static void GPSPowerControl(unsigned char IsTrue)
{
	GPIO_InitTypeDef GPIO_InitStructure;
			
	GPIO_InitStructure.GPIO_Pin = GPS_POWER_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPS_POWER_TYPE, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPS_RESET_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPS_RESET_TYPE, &GPIO_InitStructure);
	
	if(IsTrue)
	{
		GPIO_SetBits(GPS_POWER_TYPE,GPS_POWER_PIN);
		GPIO_ResetBits(GPS_RESET_TYPE,GPS_RESET_PIN);
		
		delay_ms(300);
		
		GPIO_SetBits(GPS_RESET_TYPE,GPS_RESET_PIN);
		
		delay_ms(500);
	}
	else
	{
		GPIO_ResetBits(GPS_POWER_TYPE,GPS_POWER_PIN);
		
		GPIO_ResetBits(GPS_RESET_TYPE,GPS_RESET_PIN);
	}
}

static void GPSCOMProtocol(void)
{
	if((strstr((const char *)g_USART3_RX_BUF,"PDTINFO")) != NULL )	//系统信息
	{
		
		s_IsRecGPSInfo = TRUE;
		KillTimer(TimerID);
		TimerID = 0;
	}
	else if((strstr((const char *)g_USART3_RX_BUF,"ANTSTAT1")) != NULL )	//天线检测
	{
		s_IsRecGPSAnt = TRUE;
		KillTimer(TimerID);
		TimerID = 0;
	}

	GPS_Analysis(&s_GPS_Msg, g_USART3_RX_BUF);

}

static void ClearALLFlag(void)
{
//	s_GetGPSDataFlag = FALSE;
	s_IsOverTime	= FALSE;	//主机响应超时标记
	s_IsRecGPSInfo  = FALSE;
	s_IsRecGPSAnt 	= FALSE;
	TimerID 		= 0;		//定时器ID
	s_FailedCounter	= 0;		//失败计数
}
//接收GPS的串口信息进行解析
static void TaskForGPSCommunication(void)
{	
	static u8 s_DetCount = 0, s_DetNum;//检测计数
	static uint16 s_StartCount = 0;//开始检测计数
	
	if(g_Uart3RxFlag == TRUE)	//收到一帧数据包
	{
		g_Uart3RxFlag = FALSE;
				
		if(g_USART3_RX_CNT >= 4)//防止溢出
		{
//			u1_printf("%s\r\n", g_USART3_RX_BUF);
			
			GPSCOMProtocol();
			
			if(s_GPS_Msg.Longitude >= 7340000 && s_GPS_Msg.Longitude <= 13523000)
			{
				if(s_GPS_Msg.Latitude >= 352000 && s_GPS_Msg.Latitude <= 5333000)
				{
					//已获取到经纬度数据，结束GPS供电，节约电源 	s_LatitudeBuf[GET_GPS_COUNT]
					s_StartCount++;
					
					if(s_StartCount > 10 && s_GPS_Msg.Hdop < 250 )	//前10次数据丢弃 精度因子控制在2.5以内
					{
						if(s_DetCount < 3)
						{
							s_LatitudeBuf[s_DetCount] = s_GPS_Msg.Latitude;
							s_LongitudeBuf[s_DetCount] = s_GPS_Msg.Longitude;
							s_AltitudeBuf[s_DetCount] = s_GPS_Msg.Altitude;
														
							s_Latitude = s_GPS_Msg.Latitude/100000.0;
							s_Longitude = s_GPS_Msg.Longitude/100000.0;
							s_Altitude = s_GPS_Msg.Altitude/10.0;						
						}
						else
						{
							s_LatitudeBuf[s_DetNum] = s_GPS_Msg.Latitude;
							s_LongitudeBuf[s_DetNum] = s_GPS_Msg.Longitude;
							s_AltitudeBuf[s_DetNum] = s_GPS_Msg.Altitude;

							s_Latitude = Mid_Filter(s_LatitudeBuf, s_DetCount)/100000.0;;
							s_Longitude = Mid_Filter(s_LongitudeBuf, s_DetCount)/100000.0;;
							s_Altitude = Mid_Filter(s_AltitudeBuf, s_DetCount)/10.0;;
						}
						
						s_DetCount++;
						s_DetNum++;
						
						if(s_DetCount >= GET_GPS_COUNT)
						{
							s_DetCount = GET_GPS_COUNT;
						}
						
						if(s_DetNum >= GET_GPS_COUNT)
						{
							s_DetNum = 0;
							s_StartCount =0;
							s_DetCount = 0;
							
							GPSPowerOff();	
							
							u1_printf(" Get Location Data,Altitude:%.1f,Location:%.8f,%.8f\r\n", s_Altitude, s_Latitude,  s_Longitude);
							SetLogErrCode(LOG_CODE_GPS_OK);
							s_GetGPSDataFlag = TRUE;
						}
					}
		
					
				}
			}

			//确定是一个完整的数据包
//			if(g_USART3_RX_BUF[0] == GPS_MSG_START && g_USART3_RX_BUF[g_USART3_RX_CNT-2] == GPS_MSG_END1 && g_USART3_RX_BUF[g_USART3_RX_CNT-1] == GPS_MSG_END2)
//			{							
//				GPSCOMProtocol();				
//			}
//			else
//			{
////				u1_printf("%s\r\n", g_USART3_RX_BUF);
//			}
		}				
		Clear_Uart3Buff();
	}
}
static void OnOverTime_Obser(void)
{
	s_IsOverTime = TRUE;
	
	KillTimer(TimerID);
	
	TimerID = 0;
}


static void TaskForGPSSendMsg(void)
{
	static BOOL IsWaitingACK = FALSE;	//是否接收到应答
	static u8 s_GPS_Status = GPSPRODUCTINFORMATION;//状态
	
	switch(s_GPS_Status)
	{
		case GPSPRODUCTINFORMATION:	//检测GPS模块信息
			
			if(s_IsRecGPSInfo)
			{
				s_GPS_Status = GPSANTTEST;
				ClearALLFlag();
				IsWaitingACK = FALSE;	
			}
			else if(IsWaitingACK)
			{
				if(s_IsOverTime)
				{
					s_IsOverTime = FALSE;
					if(s_FailedCounter < MAX_FAILED_TIMES)
					{
						//超时次数未达到上限,再次询问
						IsWaitingACK = FALSE;	
						//失败次数累计变量自加
						s_FailedCounter++;
						u1_printf("[TaskForGPSSendMsg]->Failed Times=%d\r\n", s_FailedCounter);
					}		
					else
					{
						//超时次数达到最大限制, 关闭系统电源
						u1_printf("[TaskForGPSSendMsg]->GPS Power OFF\r\n");
						s_GetGPSDataFlag = TRUE;
						GPSPowerOff();
					}
				}
			}
			else
			{
				//发送查询指令
				GPSSendMsg(GPSPRODUCTINFORMATION);
				//开启超时定时器
				TimerID = SetTimer(1000, OnOverTime_Obser);
				//设置等待标志位
				IsWaitingACK = TRUE;
			}					
			
		break;
		
		case GPSANTTEST:			//检测天线信息
			
			if(s_IsRecGPSAnt)
			{
				s_GPS_Status = GPSGETLOCATION;
				ClearALLFlag();
				IsWaitingACK = FALSE;	
			}
			else if(IsWaitingACK)
			{
				if(s_IsOverTime)
				{
					s_IsOverTime = FALSE;
					if(s_FailedCounter < MAX_FAILED_TIMES)
					{
						//超时次数未达到上限,再次询问
						IsWaitingACK = FALSE;	
						//失败次数累计变量自加
						s_FailedCounter++;
						u1_printf("[TaskForGPSSendMsg]->Failed Times=%d\r\n", s_FailedCounter);
					}		
					else
					{
						//超时次数达到最大限制, 关闭系统电源
						u1_printf("[TaskForGPSSendMsg]->Master Power OFF\r\n");
						GPSPowerOff();
						s_GetGPSDataFlag = FALSE;
					}
				}
			}
			else
			{
				//发送查询指令
				GPSSendMsg(GPSANTTEST);
				//开启超时定时器
				TimerID = SetTimer(1000, OnOverTime_Obser);
				//设置等待标志位
				IsWaitingACK = TRUE;
			}	
		
		break;
		
		case GPSGETLOCATION:		//等待定位
			u1_printf(" Satellite Num:%d,Position Num:%d,GPS Status:%d,Altitude:%.1fm,P:%.2f,H:%.2f,V:%.2f,Location:%d,%d\r\n", \
			s_GPS_Msg.Satellite_Num, s_GPS_Msg.Position_Num, s_GPS_Msg.GPS_Status, (float)s_GPS_Msg.Altitude/100.0, (float)s_GPS_Msg.Pdop/100.0, (float)s_GPS_Msg.Hdop/100.0, (float)s_GPS_Msg.Vdop/100.0, s_GPS_Msg.Latitude, s_GPS_Msg.Longitude);						
		break;
	}
}

//static void TimerForPowerOff(void)
//{
//	GPSPowerOff();
//	
//	KillTimer(OverTimerID);
//	
//	OverTimerID = 0;
//}

void GPSPowerOn(void)
{
	p_GPS = &s_GPS_Msg;
	
	USART3_Config(9600);	//GPS波特率为9600
		
	Uart3ChannelSet(CHANNEL_GPS);	//Uart3与云台共用，使用模拟开关选择
	
	GPSPowerControl(TRUE);	//打开GPS电源
	
	ClearALLFlag();
	
	s_GPSPowerFlag = TRUE;
	
	s_GetGPSDataFlag = FALSE;
	
	Task_Create(TaskForGPSCommunication, 10);
	
	Task_Create(TaskForGPSSendMsg, 10000);
	
//	OverTimerID = SetTimer(40000, TimerForPowerOff);		//40s后关闭GPS
}

void GPSPowerOff(void)
{
	GPSPowerControl(FALSE);
	
	s_GPSPowerFlag = FALSE;
	
	Task_Kill(TaskForGPSCommunication);
	
	Task_Kill(TaskForGPSSendMsg);
	
	KillTimer(OverTimerID);
	
	OverTimerID = 0;
	
	ClearALLFlag();
}

void GPSSendMsg(unsigned char GPSCmd)
{
	switch(GPSCmd)
	{
		case GPSPRODUCTINFORMATION:
			u1_printf(" Get GPS information\r\n");
			u3_printf("$PDTINFO\r\n");
		break;
		
		case GPSANTTEST:
			u1_printf(" Detection antenna status\r\n");
			u3_printf("$ANTSTAT1\r\n");
		break;
				
		case GPSGETLOCATION:
			u3_printf("$CFGNMEA\r\n");
		break;
	}
}



















