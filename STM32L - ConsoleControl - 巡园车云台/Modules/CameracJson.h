#ifndef		_CAMERACJSON_H_
#define		_CAMERACJSON_H_

typedef enum 
{
	MeNoModule = 0, 
	MeConnecting = 1,
	MeConnected = 2,
} 
ME909STATUS;

void InitcJsonConfig(void);

void STM32ToHi3519DetCommunicationMsg(unsigned int Seq);//STM32主动发起检测核心板通信是否正常	0x00
	
void STM32AckHi3519SensorData(unsigned int Seq);//发送一帧主板传感器应答数据包	0x01

void STM32ToHi3519GetStatus(unsigned int Seq);//获取HI3519核心板状态信息	0x02

void STM32ToHi3519PowerOff(unsigned int Seq);//STM32向核心板发送关机指令		0x08

void STM32ToHi3519ExitProcess(unsigned int Seq);//STM32向核心板退出进程指令	0x09

void SendSTM32DetCommunicationbyRetryMode(void);//使用带重发的方式测试与核心板之间的通信

void SendSTM32GetStatusCmdbyRetryMode(void);//使用带重发的方式获取核心板状态

void SendSTM32PowerOffbyRetryMode(void);//使用带重发的方式关闭核心板通信

void SetHi3519LinkFlag(unsigned char isTrue);//设置核心板通信状态

unsigned char GetHi3519LinkFlag(void);//获取核心板通信状态

void SetTimeToPowerOffHi3519Flag(unsigned char isTrue);//设置核心板电源标志

unsigned char GetTimeToPowerOffHi3519Flag(void);//获取核心板电源标志
	
//STM32内部测试指令
void STM32ToSTM32TestCmd(unsigned int Seq, unsigned char CMD);
//STM32设置参数测试
void STM32ToSTM32SetConfig(unsigned int Seq);

unsigned char GetHi3519ErrCode(void);	//通信错误码;

void TaskForHi3519CommunicationEnd(void);

ME909STATUS GetMe909Status(void);

#endif
