#ifndef __LEDCONTROL_H
#define __LEDCONTROL_H

#include "mytype.h" 

#define LED_ON	1
#define LED_OFF	0


//LED显示对应的位======================

#define LED_1	0
#define LED_2	1
#define LED_3	2
#define LED_4	3
#define LED_5	4
#define LED_6	5
#define LED_7	6
#define LED_8	7

#define LED_9	8
#define LED_10	9
#define LED_11	10
#define LED_12	11
#define LED_13	12
#define LED_14	13
#define LED_15	14
#define LED_16	15
//========================================

//采集器LED的定义=========================
#define LED_RUN		LED_1
#define LED_NET		LED_2
#define LED_LAN		LED_3

#define LED_ADC0	LED_4
#define LED_ADC1	LED_5
#define LED_ADC2	LED_6
#define LED_ADC3	LED_7

#define LED_ADC4	LED_8
#define LED_ADC5	LED_9
#define LED_ADC6	LED_10
#define LED_ADC7	LED_11
#define LED_ADC8	LED_12
#define LED_ADC9	LED_13

#define LED_P1	LED_14
#define LED_P2	LED_15

#define LED_D1	LED_16



//extern UINT8 g_ucLedAdcPos[10];


void RunLedControl( UINT16 nMain100ms );

void NetLedControl( UINT16 nMain100ms );

void LanLedControl( UINT16 nMain100ms );



void LedControlProc( UINT16 nMain100ms );

void SetLedState( UINT8 ucPos,UINT8 ucState );
UINT16 GetLedState( void );

void SetLedFlash( UINT8 ucPos,UINT8 ucState );
UINT16 GetLedFlashState( void );

#endif
















