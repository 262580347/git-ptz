#ifndef		_ARITHMETIC_H_
#define		_ARITHMETIC_H_

#include "general_type.h"
#include "mytype.h"

unsigned short PackMsg(u8* srcbuf,u16 srclen,u8* destbuf,u16 destsize);

void Int32ToArray( unsigned char *pDst, unsigned int value );

unsigned char CheckHdrSum(CLOUD_HDR *pMsg, u8 *data, u8 lenth);

float encode_float(float value);

float Mid_Filter(float * filter_data, int16 len) ;

float GetAverageValue(float * data, unsigned char len) ;

unsigned int ArrayToInt32(unsigned char *pDst, unsigned char count);

int toupper(int c);

void HexStrToByte(const char* source, unsigned char* dest, int sourceLen);

void StrToHex(unsigned char *pbDest, unsigned char *pbSrc, int nLen);

unsigned short Calculate_CRC16(unsigned char *buf,unsigned short length);

unsigned char Calc_Checksum(unsigned char *buf, unsigned int len);

unsigned int swap_dword(unsigned int value);
	
unsigned short int swap_word(unsigned short int value);

unsigned short UnPackMsg(unsigned char *sbuf, unsigned short slen, unsigned char *dbuf, unsigned short *dlen);

unsigned short BBC_Checksum(char *Data, unsigned char Lenth);

int GetIPAddress(char *strIP, char *ArrayIP);

float Adc_Filter(float * filter_data, unsigned short len) ;



#endif



