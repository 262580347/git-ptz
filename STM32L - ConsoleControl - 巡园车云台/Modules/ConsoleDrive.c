#include "main.h"
#include "ConsoleDrive.h"

#define		PRESETGOTOTIME		16000	//预置点的旋转等待时间

#define		AUTOPOINTTIME		35000	//自检等待时间

static u8 s_ConsolePosition = 0;

static u8 s_ConsolePowerFlag = FALSE;	//云台电源开启标志

static unsigned int OverTimerID = 0;		//超时定时器ID

static u8 s_NeedToDetMyself = FALSE;

static void TaskForMotorCommunication(void);

unsigned char GetConsolePosition(void)
{
	return s_ConsolePosition;
}

void SetConsolePosition(unsigned char Point)
{
	s_ConsolePosition = Point;
}

unsigned char GetConsolePowerFlag(void)
{
	return s_ConsolePowerFlag;
}

typedef __packed struct
{
	unsigned char Command;
	unsigned char PelcoBuf[7];
}CONSOLE_CFG;

static CONSOLE_CFG ConsoleCMDList[] =
{
	{CONTROL_STOP,				{0xff,0x01,0x00,0x00,0x00,0x00,0x01}},
	{CONTROL_UP,				{0xff,0x01,0x00,0x08,0x00,0xff,0x08}},
	{CONTROL_DOWN,				{0xff,0x01,0x00,0x10,0x00,0xff,0x10}},
	{CONTROL_LEFT,				{0xff,0x01,0x00,0x04,0xff,0x00,0x04}},
	{CONTROL_RIGHT,				{0xff,0x01,0x00,0x02,0xff,0x00,0x02}},
	{CONTROL_ZOOM_LONG,			{0xff,0x01,0x00,0x20,0x00,0x00,0x21}},
	{CONTROL_ZOOM_SHORT,		{0xff,0x01,0x00,0x40,0x00,0x00,0x41}},
	{CONTROL_FOCUS_CLOSE,		{0xff,0x01,0x00,0x80,0x00,0x00,0x81}},
	{CONTROL_FOCUS_FAR,			{0xff,0x01,0x01,0x00,0x00,0x00,0x02}},
	{CONTROL_APERTUR_ZOOMOUT,	{0xff,0x01,0x02,0x00,0x00,0x00,0x03}},
	{CONTROL_APERTUR_ZOOMINT,	{0xff,0x01,0x04,0x00,0x00,0x00,0x05}},
	{CONTROL_LIGHT_ON,			{0xff,0x01,0x00,0x0b,0x00,0x01,0x0d}},
	{CONTROL_LIGHT_OFF,			{0xff,0x01,0x00,0x09,0x00,0x01,0x0b}},
	{CONTROL_GO_TO_PRESET,		{0xff,0x01,0x00,0x07,0x00,0x08,0x09}},
	{CONTROL_SET_PRESET,		{0xff,0x01,0x00,0x03,0x00,0x08,0x05}},
	{CONTROL_CLEAR_PRESET,		{0xff,0x01,0x00,0x05,0x00,0x08,0x07}},
};

void SendConsoleCMD(unsigned char ConsoleCMD)
{
	u8 i, Sum = 0;
	
	for(i=1; i<6; i++)
	{
		Sum += ConsoleCMDList[ConsoleCMD].PelcoBuf[i];
	}
	
	ConsoleCMDList[ConsoleCMD].PelcoBuf[i] = Sum;
	
	USART3_DMA_Send((u8 *)&ConsoleCMDList[ConsoleCMD].PelcoBuf, sizeof(ConsoleCMDList[0].PelcoBuf));
	
//	u1_printf(" Console:");
//	for(i=0; i<7; i++)
//	{
//		u1_printf("%02X ", ConsoleCMDList[ConsoleCMD].PelcoBuf[i]);
//	}
//	u1_printf("\r\n");
}

void ConsoleStop(void)
{
	SendConsoleCMD(CONTROL_STOP);
}

void ConsoleUp(void)
{
	SendConsoleCMD(CONTROL_UP);
}

void ConsoleDown(void)
{
	SendConsoleCMD(CONTROL_DOWN);
}

void ConsoleLeft(void)
{
	SendConsoleCMD(CONTROL_LEFT);
}

void ConsoleRight(void)
{
	SendConsoleCMD(CONTROL_RIGHT);
}

void SetPresetPoint(unsigned char Num)
{
	ConsoleCMDList[CONTROL_SET_PRESET].PelcoBuf[PRESET_INDX] = Num;
		
	SendConsoleCMD(CONTROL_SET_PRESET);
}

void GotoPresetPoint(unsigned char Num)
{
	ConsoleCMDList[CONTROL_GO_TO_PRESET].PelcoBuf[PRESET_INDX] = Num;
		
	SendConsoleCMD(CONTROL_GO_TO_PRESET);
}

void DelPresetPoint(unsigned char Num)
{
	ConsoleCMDList[CONTROL_CLEAR_PRESET].PelcoBuf[PRESET_INDX] = Num;
		
	SendConsoleCMD(CONTROL_CLEAR_PRESET);
}

void MotorPowerControl(unsigned char IsTrue)
{
	GPIO_InitTypeDef GPIO_InitStructure;
			
	GPIO_InitStructure.GPIO_Pin = MOTOR_POWER_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(MOTOR_POWER_TYPE, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = RS485_POWER_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(RS485_POWER_TYPE, &GPIO_InitStructure);
	
	if(IsTrue)
	{
		GPIO_SetBits(MOTOR_POWER_TYPE,MOTOR_POWER_PIN);
		GPIO_SetBits(RS485_POWER_TYPE,RS485_POWER_PIN);
	}
	else
	{
		GPIO_ResetBits(MOTOR_POWER_TYPE,MOTOR_POWER_PIN);
		GPIO_ResetBits(RS485_POWER_TYPE,RS485_POWER_PIN);
	}
}

static void TimerForPowerOff(void)
{	
	if(g_ComTestFlag == FALSE)	//调试模式不关闭电源
	{
		if(s_NeedToDetMyself)		//自检结束	转至第一个预置点 此分支不关闭定时器
		{
			s_NeedToDetMyself = FALSE;
			
			Task_Create(TaskForMotorCommunication, 15000);
			
			OverTimerID = SetTimer(PRESETGOTOTIME, TimerForPowerOff);	
			
			return;
		}
		else			
		{	
			u1_printf(" Motor Power Off\r\n");
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			MotorPowerOff();		
		}
	}
	
	KillTimer(OverTimerID);
	
	OverTimerID = 0;
}

static void TaskForMotorCommunication(void)
{
	static u8 s_First = FALSE;
	static u8 s_NextNeedToDet = FALSE;
	static u8 s_RunCount = 0;
	
	SYSTEMCONFIG *p_sys;
		
	if(s_NextNeedToDet)
	{
		delay_ms(200);	
		ConsoleStop();	
		delay_ms(100);	
		GotoPresetPoint(AUTOPOINT);			
		delay_ms(100);		
		GotoPresetPoint(AUTOPOINT);		
		u1_printf("\r\n Automatic cruise on\r\n");		
		delay_ms(100);
		
		s_NeedToDetMyself = TRUE;
		
		s_NextNeedToDet= FALSE;
			
		OverTimerID = SetTimer(AUTOPOINTTIME, TimerForPowerOff);
		
		Task_Kill(TaskForMotorCommunication);
		
		return;
	}
	
	if(!s_First)
	{
		s_First = TRUE;
		
		OverTimerID = SetTimer(AUTOPOINTTIME, TimerForPowerOff);		//40s后关闭云台
			
		delay_ms(200);
		ConsoleStop();
		delay_ms(200);	
		GotoPresetPoint(AUTOPOINT);		
		delay_ms(100);	
		GotoPresetPoint(AUTOPOINT);	
		
		s_NeedToDetMyself = TRUE; 	//为了自检完后转至第一个预置点
			
		u1_printf(" First power on, start automatic cruise\r\n");
	}
	else
	{		
		p_sys = GetSystemConfig();	
		
		s_ConsolePosition++;
		
		if((s_ConsolePosition > p_sys->Retry_Times))
		{
			s_ConsolePosition = 1;
			s_RunCount++;
			u1_printf(" Count:%d\r\n", s_RunCount);
		}
		
		if(s_RunCount >= 1)
		{			
			s_RunCount = 0;
			
			s_NextNeedToDet = TRUE;
		}
				
		u1_printf(" Go to %d Preset\r\n", s_ConsolePosition);		
		
		delay_ms(60);
		ConsoleStop();
		GotoPresetPoint(SPEED_FAST);
		delay_ms(60);
		ConsoleStop();
		
		delay_ms(60);
		
		GotoPresetPoint(s_ConsolePosition);
		
		OverTimerID = SetTimer(PRESETGOTOTIME, TimerForPowerOff);
	}	
	
	Task_Kill(TaskForMotorCommunication);
}

void MotorTestPowerOn(void)
{
	USART3_Config(2400);	//云台波特率为2400
		
	Uart3ChannelSet(CHANNEL_MOTOR);	//Uart3与GPS共用，使用模拟开关选择
	
	u1_printf(" Motor Power On\r\n");
	
	s_ConsolePowerFlag = TRUE;
	
	MotorPowerControl(TRUE);
	
	SetConsolePosition(1);
}

void MotorPowerOn(void)
{
	USART3_Config(2400);	//云台波特率为2400
		
	Uart3ChannelSet(CHANNEL_MOTOR);	//Uart3与GPS共用，使用模拟开关选择
	
	u1_printf(" Motor Power On\r\n");
	
	while (DMA_GetCurrDataCounter(DMA1_Channel4));
	
	s_ConsolePowerFlag = TRUE;
	
	MotorPowerControl(TRUE);
	
//	delay_ms(1000);
	
	Task_Create(TaskForMotorCommunication, 15000);
	
	OverTimerID = SetTimer(AUTOPOINTTIME, TimerForPowerOff);		//20s后关闭云台
}

void MotorPowerOff(void)
{
	MotorPowerControl(FALSE);
	
	s_ConsolePowerFlag = FALSE;
	
	Task_Kill(TaskForMotorCommunication);
}











