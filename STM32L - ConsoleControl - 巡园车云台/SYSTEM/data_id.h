
#ifndef __DATA_ID_H
#define __DATA_ID_H

#define KQW_ID				(0x0001)   //空气温度
#define KQS_ID      		(0x0002)   //空气湿度
#define TRW_ID      		(0x0003)   //土壤温度
#define TRS_ID      		(0x0004)   //土壤水分
#define GZD_ID      		(0x0005)   //光照度
#define CO2_ID      		(0x0006)   //二氧化碳
#define JYL_ID      		(0x0007)   //降雨量
#define FS_ID       		(0x0008)   //风速
#define LL_ID       		(0x0009)   //流量
#define FX_ID       		(0x000A)   //风向
#define SYL_ID      		(0x000B)   //水压力
#define QY_ID       		(0x000C)   //气压
#define YW_ID       		(0x000D)   //液位 		0-5m
#define PH_ID       		(0x000E)   //酸碱度

//add 2014.8.7
#define YMSD_ID				(0x0010)   //叶面湿度
#define NOISE_ID			(0x0011)   //噪声分贝DB
#define HWDS_ID				(0x0012)   //红外对射(光束遮断式)
#define HWSJ_ID				(0x0013)   //红外双鉴(微波+红外,空间探测)

#define H2S_ID      		(0x0014)   //硫化氢
#define NH3_ID				(0x0015)   //氨气浓度
#define AIR_PRESS_ID		(0x0016) 	//大气压力
#define PHOTOSYNTHETIC_ACTIVE_RADIATION_ID	(0x0017) 	//光合有效辐射
#define EVAPORATION_ID		(0x0018) 	//蒸发量
#define SOLAR_RADIATION_ID	(0x0019)	//太阳总辐射


#define O2_ID      			(0x001E)   //氧含量
#define CO2_PER_ID  		(0x001F)   //二氧化碳
#define CH4_ID       		(0x0020)   //甲烷

#define CO_ID				(0x0021)   //一氧化碳(CO)
#define NO2_ID				(0x0022)   //二氧化氮(N2O)
#define SO2_ID				(0x0023)   //二氧化硫(S2O)
#define SMOKE_ID			(0x0024)   //粉尘浓度
#define HCL_ID				(0x0025)   //氯化氢浓度（HCl，PPM）

#define DDL_ID				(0x002A)   //水质电导率
#define RJY_ID				(0x002B)   //溶解氧

#define COD_ID				(0x0034)   //COD指标
#define NH3_NH4_ID			(0x0035)   //氨氮
#define SOIL_M_ID			(0x0036)   //土壤张力0-93KPa

#define SOIL_EC_ID			(0x0038)  	//土壤EC值,0-20mS,精度0.1mS
#define SOIL_YF_ID			(0x0039)	//土壤盐分，0-8000mg/L，精度1mg
#define	SOIL_WATER_POTENTIAL (0x003A)	//土壤水势传感器  -9 ~ -100,000kPa

#define	H2O_NH3_ID			(0x003D)		//水质氨氮	ppm
#define	H2O_TURBIDITY_ID	(0x003E)		//水质浊度	NTU
#define H2O_CL2_ID			(0x003F)		//水质余氯	ppm

#define	ORP_POTENTIOMETER_ID (0x0040)	//氧化还原电位计
#define CHLOROPHYLL_ID		(0x0041)	//叶绿素传感器		

#define	LONGITUDE_ID		(0x0050)		//经度
#define	LATITUDE_ID			(0x0051)		//维度

#define C_LEVEL_ID  		(0x0064)   //电平控制器
#define C_RT_P_ID   		(0x0065)   //脉冲型正反转
#define C_RT_L_ID   		(0x0066)   //电平型正反转
#define C_PULSE_ID			(0x0068)   //脉冲开关控制
#define BAT_VOL     		(0x00FF)   //本地电源电压
//错误标识
#define 	ERROR_VALUE		0xEEEEEEEE

//数据标识符号
#define MARK_FLOAT			(0x00)
#define MARK_INT32			(0x01)
#define MARK_UINT32			(0x02)
#define MARK_INT16			(0x03)
#define MARK_UINT16			(0x04)
#define MARK_UINT8			(0x05)
#define MARK_STR			(0x06)
#endif
